package com.advertise.quickjob.app.domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserDetails {


    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("user_phone")
    @Expose
    private String userPhone;
    @SerializedName("user_email")
    @Expose
    private String userEmail;
    @SerializedName("user_address")
    @Expose
    private String userAddress;
    @SerializedName("user_dob")
    @Expose
    private String userDob;
    @SerializedName("user_Qualification")
    @Expose
    private String userQualification;
    @SerializedName("user_Qualification_name")
    @Expose
    private String userQualificationName;
    @SerializedName("user_male")
    @Expose
    private String userMale;

    @SerializedName("user_name")
    @Expose
    private String user_name;

    public UserDetails() {
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserAddress() {
        return userAddress;
    }

    public void setUserAddress(String userAddress) {
        this.userAddress = userAddress;
    }

    public String getUserDob() {
        return userDob;
    }

    public void setUserDob(String userDob) {
        this.userDob = userDob;
    }

    public String getUserQualification() {
        return userQualification;
    }

    public void setUserQualification(String userQualification) {
        this.userQualification = userQualification;
    }

    public String getUserQualificationName() {
        return userQualificationName;
    }

    public void setUserQualificationName(String userQualificationName) {
        this.userQualificationName = userQualificationName;
    }

    public String getUserMale() {
        return userMale;
    }

    public void setUserMale(String userMale) {
        this.userMale = userMale;
    }
}
