package com.advertise.quickjob.app.fragments


import android.os.Bundle
import android.util.DisplayMetrics
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.advertise.quickjob.app.R
import kotlinx.android.synthetic.main.fragment_ad.view.*

/**
 * A simple [Fragment] subclass.
 */
class AdFragment : Fragment() {

    var v:View?=null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        v= inflater.inflate(R.layout.fragment_ad, container, false)

        val bundle = arguments
        val pos = bundle!!.getString("pos")

        val displayMetrics = DisplayMetrics()
        activity!!.getWindowManager().defaultDisplay.getMetrics(displayMetrics)

        val height = displayMetrics.heightPixels
        val width = displayMetrics.widthPixels
        val w = width / 1.4


        if(pos.equals("0"))
        {
            v!!.img.setImageResource(R.drawable.slideronereduced)

        }
        if(pos.equals("1"))
        {

            v!!.img.setImageResource(R.drawable.slidertworeduced)
        }

        if(pos.equals("2"))
        {

            v!!.img.setImageResource(R.drawable.sliderthreereduced)
        }


        return v
    }


}
