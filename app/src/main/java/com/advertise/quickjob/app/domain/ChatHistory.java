package com.advertise.quickjob.app.domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChatHistory {
    @SerializedName("chatroom_id")
    @Expose
    private String chatroomId;
    @SerializedName("chatroom_secondperson")
    @Expose
    private String chatroomSecondperson;
    @SerializedName("chatuser")
    @Expose
    private Jobuser chatuser;

    public ChatHistory() {
    }

    public String getChatroomId() {
        return chatroomId;
    }

    public void setChatroomId(String chatroomId) {
        this.chatroomId = chatroomId;
    }

    public String getChatroomSecondperson() {
        return chatroomSecondperson;
    }

    public void setChatroomSecondperson(String chatroomSecondperson) {
        this.chatroomSecondperson = chatroomSecondperson;
    }

    public Jobuser getChatuser() {
        return chatuser;
    }

    public void setChatuser(Jobuser chatuser) {
        this.chatuser = chatuser;
    }
}
