package com.advertise.quickjob.app.views

import android.app.DatePickerDialog
import android.content.Intent
import android.graphics.Bitmap
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.DatePicker
import android.widget.NumberPicker
import android.widget.Toast
import com.advertise.quickjob.app.R
import com.advertise.quickjob.app.Utils.Constants
import com.advertise.quickjob.app.Utils.DownloadUtils
import com.advertise.quickjob.app.adapter.Qualificationadapter
import com.advertise.quickjob.app.domain.Jobuser
import com.advertise.quickjob.app.domain.Qualification
import com.advertise.quickjob.app.domain.Qualificationdata
import com.advertise.quickjob.app.domain.Tokendata
import com.advertise.quickjob.app.preferencehelper.PreferenceHelper
import com.advertise.quickjob.app.progress.ProgressFragment
import com.advertise.quickjob.app.webservicehelper.Retrofithelper
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import kotlinx.android.synthetic.main.activity_registration.*
import kotlinx.android.synthetic.main.activity_registration.view.*;
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList

class RegistrationActivity : AppCompatActivity() {


    var date:String?=null

    var qualification:String="0"

    var gender:String=""

    var date_data:String=""
    var month_data:String=""
    var year_data:String=""

    internal var pickerVals =
        arrayOf("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec")


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration)
        supportActionBar!!.hide()


        txtDate.setOnClickListener {

showdatepicker()
        }

        imgdate.setOnClickListener {

            showdatepicker()

        }


getQualifications()


        num1.maxValue = 31
        num1.minValue = 1


        num2.displayedValues = pickerVals

        num2.minValue = 0
        num2.maxValue = pickerVals.size - 1


        num3.maxValue = 2020
        num3.minValue = 1950



date_data="1"
        month_data="Jan"
year_data="1950"




        num1.setOnValueChangedListener(object :NumberPicker.OnValueChangeListener{

            override fun onValueChange(p0: NumberPicker?, p1: Int, p2: Int) {

           var a=     num1.value

                date_data=a.toString();

              //  Toast.makeText(this@RegistrationActivity,a.toString()+"",Toast.LENGTH_SHORT).show()

            }
        })


        num2.setOnValueChangedListener(object :NumberPicker.OnValueChangeListener{

            override fun onValueChange(p0: NumberPicker?, p1: Int, p2: Int) {

                var a=     num2.value

                var data=pickerVals[a]
                month_data=data

              //  Toast.makeText(this@RegistrationActivity,data.toString()+"",Toast.LENGTH_SHORT).show()


            }
        })


        num3.setOnValueChangedListener(object :NumberPicker.OnValueChangeListener{

            override fun onValueChange(p0: NumberPicker?, p1: Int, p2: Int) {

                var a=     num3.value

                year_data=a.toString()

             //   Toast.makeText(this@RegistrationActivity,a.toString()+"",Toast.LENGTH_SHORT).show()


            }
        })




        btn_signup.setOnClickListener {



            if(radiofemale.isChecked)
            {
                gender="female"
            }

            if(radiomale.isChecked)
            {
                gender="male"
            }

            if(radiothers.isChecked)
            {
                gender="Other"
            }




if(!input_name!!.text.toString().equals("")){


    if(!input_address!!.text.toString().equals("")){






            if(!input_mobile!!.text.toString().equals("")){


                if(input_mobile!!.text.toString().length==10){

                    date=date_data+"-"+month_data+"-"+year_data



                if(date!=null){

                    var w=spQualification!!.selectedItem as Qualification

                    qualification=w!!.id

                    if(!qualification.equals("0")){



                        if(!input_password!!.text.toString().equals("")){


                            if(!input_reEnterPassword!!.text.toString().equals("")){



checkMobileNumber()




                            }
                            else{


                                Toast.makeText(this,"Enter your password",Toast.LENGTH_SHORT).show()

                            }









                        }
                        else{


                            Toast.makeText(this,"Enter your password",Toast.LENGTH_SHORT).show()

                        }









                    }
                    else{


                        Toast.makeText(this,"Select your qualification",Toast.LENGTH_SHORT).show()

                    }







                }
                else{


                    Toast.makeText(this,"Select your date of birth",Toast.LENGTH_SHORT).show()

                }


                }
                else{


                    Toast.makeText(this,"Enter your valid mobile number",Toast.LENGTH_SHORT).show()

                }

            }
            else{


                Toast.makeText(this,"Enter your mobile number",Toast.LENGTH_SHORT).show()

            }









    }
    else{


        Toast.makeText(this,"Enter your address",Toast.LENGTH_SHORT).show()

    }





}
            else{


    Toast.makeText(this,"Enter your name",Toast.LENGTH_SHORT).show()

            }





        }
    }




    fun registerUser()

    {


        var p: ProgressFragment = ProgressFragment()
        p.show(supportFragmentManager,"dhjjd")
        val retrofithelper = Retrofithelper()

        var a: Call<Tokendata> = retrofithelper.getClient().userRegister(input_name!!.text.toString(),
            input_mobile!!.text.toString(),input_email!!.text.toString(),input_password!!.text.toString(),
            gender,date!!,input_address!!.text.toString(),qualification.toInt()
        )


        a.enqueue(object : Callback<Tokendata> {
            override fun onResponse(call: Call<Tokendata>, response: Response<Tokendata>) {

                p.dismiss()

                if(response.body()!=null)
                {

                    var p=response.body();

                    if(p!!.status==1)
                    {

                        PreferenceHelper(this@RegistrationActivity).putData(Constants.tokenkey,p!!.token)

                        startActivity(Intent(this@RegistrationActivity,MainActivity::class.java))

                        finish()



                    }
                    else{

                        Toast.makeText(this@RegistrationActivity,"Unknown error",Toast.LENGTH_SHORT).show()


                    }








                }






            }

            override fun onFailure(call: Call<Tokendata>, t: Throwable) {

                p.dismiss()

                if(!DownloadUtils().isNetworkConnected(this@RegistrationActivity))
                {
                    DownloadUtils().showNoConnectiondialog(this@RegistrationActivity);
                }

                t.printStackTrace()






            }
        })

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode==111 &&data!=null )
        {

            var msg=data.getStringExtra("message");

            if(msg!=null)
            {
                if(msg.equals("OTP verified"))
                {


                    registerUser()

                }



            }



        }


    }

















    fun checkMobileNumber()
    {

        var p: ProgressFragment = ProgressFragment()
        p.show(supportFragmentManager,"dhjjd")
        val retrofithelper = Retrofithelper()

        var a: Call<JsonObject> = retrofithelper.getClient().checkMobileNumber(input_mobile!!.text.toString())


        a.enqueue(object : Callback<JsonObject> {
            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {

                p.dismiss()

                if(response.body()!=null)
                {

                    var p=response.body();

                    var js=JSONObject(p.toString());



                    if(js.getInt("status")==1)
                    {


                        Toast.makeText(this@RegistrationActivity,js.getString("message"),Toast.LENGTH_SHORT).show()



                    }
                    else{


                       // registerUser()


                        val intent=Intent(this@RegistrationActivity,OTPActivity::class.java)

                        intent.putExtra("mob",input_mobile.text.toString().trim())

                        startActivityForResult(intent,111)




                    }








                }






            }

            override fun onFailure(call: Call<JsonObject>, t: Throwable) {

                p.dismiss()

                if(!DownloadUtils().isNetworkConnected(this@RegistrationActivity))
                {
                    DownloadUtils().showNoConnectiondialog(this@RegistrationActivity);
                }

                t.printStackTrace()






            }
        })



    }





    fun showdatepicker()
    {


        val cal=Calendar.getInstance();

        val dateSetListener = object : DatePickerDialog.OnDateSetListener {
            override fun onDateSet(view: DatePicker, year: Int, monthOfYear: Int,
                                   dayOfMonth: Int) {
//                cal.set(Calendar.YEAR, year)
//                cal.set(Calendar.MONTH, monthOfYear)
//                cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
//                updateDateInView()

                var m=monthOfYear+1;

                date=""+dayOfMonth+"-"+m+"-"+year

                txtDate.setText(date)



            }
        }



        DatePickerDialog(this,
            dateSetListener,
            // set DatePickerDialog to point to today's date when it loads up
            cal.get(Calendar.YEAR),
            cal.get(Calendar.MONTH),
            cal.get(Calendar.DAY_OF_MONTH)).show()
    }





    fun getQualifications()
    {

        val retrofithelper = Retrofithelper()

      var a: Call<Qualificationdata> = retrofithelper.getClient().getQualifications()

        a.enqueue(object : Callback<Qualificationdata> {
            override fun onResponse(call: Call<Qualificationdata>, response: Response<Qualificationdata>) {


                if(response.body()!=null)
                {

                    var q=response.body();

                    if(q!!.status==1)
                    {

                        var qlist=q!!.data as ArrayList<Qualification>

                        var q=Qualification();
                        q.id="1";
                        q.name="Select qualification"

                        qlist.add(0,q)

                        spQualification.adapter=Qualificationadapter(this@RegistrationActivity,qlist)





                    }
                    else{

                        Toast.makeText(this@RegistrationActivity,"Unknown erro found",Toast.LENGTH_SHORT).show()
                    }





                }






            }

            override fun onFailure(call: Call<Qualificationdata>, t: Throwable) {


                if(!DownloadUtils().isNetworkConnected(this@RegistrationActivity))
                {
                    DownloadUtils().showNoConnectiondialog(this@RegistrationActivity);
                }

                t.printStackTrace()






            }
        })




    }






}
