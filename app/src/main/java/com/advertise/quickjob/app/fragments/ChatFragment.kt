package com.advertise.quickjob.app.fragments


import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_chat.view.*;
import com.advertise.quickjob.app.R
import com.advertise.quickjob.app.Utils.Constants
import com.advertise.quickjob.app.adapter.ChatAdapter
import com.advertise.quickjob.app.backgroundservices.ChatHistoryService
import com.advertise.quickjob.app.backgroundservices.IncomingChatService
import com.advertise.quickjob.app.domain.ChatHistoryBundle
import com.advertise.quickjob.app.domain.ChatMessage
import com.advertise.quickjob.app.domain.ChatRoom
import com.advertise.quickjob.app.preferencehelper.PreferenceHelper
import com.advertise.quickjob.app.webservicehelper.Retrofithelper
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_chatroom.*
import kotlinx.android.synthetic.main.layout_chatholder.view.*
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.*
import java.lang.Exception
import java.text.FieldPosition
import java.util.*
import kotlin.collections.ArrayList

/**
 * A simple [Fragment] subclass.
 */
class ChatFragment : Fragment() {

    var v:View?=null

    var chatAdapter:ChatAdapter?=null

    var logs:ArrayList<ChatRoom>?=null

    var hourlyTask: TimerTask?=null

    var timer:Timer?=null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {



        v= inflater.inflate(R.layout.fragment_chat, container, false)






        return v!!
    }


    override fun onDestroy() {
        super.onDestroy()

        try {

            if (chatmsgreceiver != null) {
                activity!!.unregisterReceiver(chatmsgreceiver)
            }
        }catch (e:Exception)
        {
            e.printStackTrace()
        }
    }

    override fun onResume() {
        super.onResume()

        getChatHistory()

        var intentFilter =  IntentFilter();
        intentFilter.addAction("com.quickjob.advertise.job.incomingchat");
        activity!!.registerReceiver(chatmsgreceiver, intentFilter);
    }


    fun getChatHistory() {



        val retrofithelper = Retrofithelper()

        val jsonArrayCall = retrofithelper.getClient().getAllChatrooms(
            PreferenceHelper(activity).getData(
                Constants.tokenkey
            )
        )

        jsonArrayCall.enqueue(object : Callback<List<ChatRoom>> {
            override fun onResponse(
                call: Call<List<ChatRoom>>,
                response: Response<List<ChatRoom>>
            ) {

                logs=ArrayList<ChatRoom>()

                if (response.body() != null) {

                    var json = response.body()


                    try {



                        if (json!!.size>0) {

                            logs!!.addAll(json)


                            for (p in 0 until logs!!.size)
                            {

                                showMessageFromLocalFolder(logs!!.get(p),p)
                            }


                            checkIncomingmsgs()

                            v!!.recycler_view.layoutManager=LinearLayoutManager(activity)

                            chatAdapter=ChatAdapter(activity!!,logs!!)
                            v!!.recycler_view.adapter=chatAdapter


                        }
                        else{

                           // Toast.makeText(activity,"message sending failed", Toast.LENGTH_SHORT).show()
                        }


                    } catch (ex: Exception) {


                    }


                }
                else{

                   // Toast.makeText(activity,"message sending failed", Toast.LENGTH_SHORT).show()
                }



            }

            override fun onFailure(call: Call<List<ChatRoom>>, t: Throwable) {

            }
        })


    }





    fun checkdate(p:ArrayList<ChatMessage>):ArrayList<ChatMessage>
    {

        var date=""
        var plist=ArrayList<ChatMessage>()

        for (i in p)
        {

            if(date.equals(i.chat_senddate))
            {

                plist.add(i)

            }
            else{


                date=i.chat_senddate

                var c=ChatMessage()
                c.chatId="0"
                c.chat_senddate=i.chat_senddate



                plist.add(c)

                plist.add(i)




            }
        }


        return plist

    }




















    fun checkIncomingmsgs()
    {
        timer = Timer()
        hourlyTask = object : TimerTask() {
            override fun run() {


                // createChatFolder()

                for (p in 0 until logs!!.size)
                {

                    val intent= Intent(activity, IncomingChatService::class.java)
                    intent.putExtra("chatroom",logs!!.get(p).chatroomid)
                    intent.putExtra("position",p)



                    activity!!.startService(intent );
                }


            }
        }


        timer!!.schedule(hourlyTask, 4000, 6000);
    }


    private val chatmsgreceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {

            var data=intent.getSerializableExtra("DATAPASSED") as ChatHistoryBundle;

            var po=intent.getIntExtra("position",0)

            //  logs.clear()
            var unreadmsg = ArrayList<ChatMessage>()
            var unreadmsglogs = ArrayList<ChatMessage>()


            var p = data.chatHistories


            unreadmsglogs.addAll(p)

            var fjsondata = readjsonfile(logs!!.get(po))

            if (!fjsondata.equals("")) {

                var gs = GsonBuilder().create()

                val myType = object : TypeToken<ArrayList<ChatMessage>>() {}.type
                var  logsfile = gs.fromJson<ArrayList<ChatMessage>>(fjsondata, myType)




                for (j in p) {


                    if (!checkMessagesInFile(logsfile, j)) {

                        unreadmsg.add(j)

                       // logs!!.get(po).senderid

                        logs!!.get(po).message=j.chatMessage

                        logs!!.get(po).messagetype=j.chatMessageType
                        logs!!.get(po).receiverid=j.chatReceiverid
                        logs!!.get(po).senderid=j.chatSenderid

                    }


                }





                    // logs.addAll(unreadmsg)

                    val gson = Gson();





                    var listStringdata = gson.toJson(
                        unreadmsglogs
                    );


                    writeDatatoFile(listStringdata,logs!!.get(po).chatroomid)



            } else {


                val gson = Gson();

                var listString = gson.toJson(
                    unreadmsglogs
                );


                writeDatatoFile(listString,logs!!.get(po).chatroomid)


            }









                //  scrolledposition = logs.size

                if(chatAdapter!=null) {

                    chatAdapter!!.notifyDataSetChanged()
                }





        }
    }


    fun checkMessagesInFile(listfromfile: List<ChatMessage>, data: ChatMessage): Boolean {
        var a = false

        for (k in listfromfile) {

            if (data.chatId.equals(k.chatId)) {

                a = true
                break;

            }


        }





        return a
    }

    fun writeDatatoFile(data: String,chatroomid:Int) {
        try {
            var s = activity!!.externalCacheDir.toString() + "/" + chatroomid

            var f = File(s)
            var fpath = f.absolutePath + "/" + chatroomid + ".json"

            var fjson = File(fpath)


            var fileWriter = FileWriter(fjson);
            var bufferedWriter = BufferedWriter(fileWriter);
            bufferedWriter.write(data);
            bufferedWriter.close();

        } catch (ex: Exception) {


        }
    }

    fun showMessageFromLocalFolder(chatroom: ChatRoom,position: Int)
    {

        var s = activity!!.externalCacheDir.toString() + "/" + chatroom!!.chatroomid

        var f = File(s)
        var fpath = f.absolutePath + "/" + chatroom!!.chatroomid + ".json"

        var fjson = File(fpath)

        var fjsondata = readjsonfile(chatroom)

        if (!fjsondata.equals("")) {

            var gs = GsonBuilder().create()

            val myType = object : TypeToken<ArrayList<ChatMessage>>() {}.type
            var  log = gs.fromJson<ArrayList<ChatMessage>>(fjsondata, myType)

            if(log.size>0)
            {

                var chatMessage=log.get(log.size-1)


                logs!!.get(position).message=chatMessage.chatMessage

                logs!!.get(position).messagetype=chatMessage.chatMessageType
                logs!!.get(position).receiverid=chatMessage.chatReceiverid
                logs!!.get(position).senderid=chatMessage.chatSenderid




            }


        }



    }

    fun readjsonfile(chatroom: ChatRoom): String {
        var responce = ""

        try {
            var s = activity!!.externalCacheDir.toString() + "/" + chatroom!!.chatroomid

            var f = File(s)
            var fpath = f.absolutePath + "/" + chatroom!!.chatroomid + ".json"

            var fjson = File(fpath)


            var fileReader = FileReader(fjson);
            var bufferedReader = BufferedReader(fileReader);
            var stringBuilder = StringBuilder();
            var line = bufferedReader.readLine();
            while (line != null) {
                stringBuilder.append(line).append("\n");
                line = bufferedReader.readLine();
            }
            bufferedReader.close();// This responce will have Json Format String
            responce = stringBuilder.toString();


        } catch (ex: Exception) {

        }


        return responce;
    }


}
