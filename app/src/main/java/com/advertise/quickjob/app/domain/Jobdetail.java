package com.advertise.quickjob.app.domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Jobdetail {

    @SerializedName("job_id")
    @Expose
    private String jobId;
    @SerializedName("job_title")
    @Expose
    private String jobTitle;
    @SerializedName("job_desccription")
    @Expose
    private String jobDesccription;
    @SerializedName("job_salary_info")
    @Expose
    private String jobSalaryInfo;
    @SerializedName("job_user_id")
    @Expose
    private String jobUserId;
    @SerializedName("job_address")
    @Expose
    private String jobAddress;
    @SerializedName("job_latitude")
    @Expose
    private String jobLatitude;
    @SerializedName("job_longitude")
    @Expose
    private String jobLongitude;
    @SerializedName("job_deal_status")
    @Expose
    private String jobDealStatus;
    @SerializedName("job_phone_visible_status")
    @Expose
    private String jobPhoneVisibleStatus;
    @SerializedName("job_uploaded_date")
    @Expose
    private String jobUploadedDate;
    @SerializedName("job_edited_date")
    @Expose
    private String jobEditedDate;
    @SerializedName("job_validity")
    @Expose
    private String jobValidity;
    @SerializedName("job_salary_type")
    @Expose
    private String jobSalaryType;
    @SerializedName("job_qualification")
    @Expose
    private String jobQualification;
    @SerializedName("jobuser")
    @Expose
    private Jobuser jobuser;

    public Jobdetail() {
    }


    public Jobuser getJobuser() {
        return jobuser;
    }

    public void setJobuser(Jobuser jobuser) {
        this.jobuser = jobuser;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getJobDesccription() {
        return jobDesccription;
    }

    public void setJobDesccription(String jobDesccription) {
        this.jobDesccription = jobDesccription;
    }

    public String getJobSalaryInfo() {
        return jobSalaryInfo;
    }

    public void setJobSalaryInfo(String jobSalaryInfo) {
        this.jobSalaryInfo = jobSalaryInfo;
    }

    public String getJobUserId() {
        return jobUserId;
    }

    public void setJobUserId(String jobUserId) {
        this.jobUserId = jobUserId;
    }

    public String getJobAddress() {
        return jobAddress;
    }

    public void setJobAddress(String jobAddress) {
        this.jobAddress = jobAddress;
    }

    public String getJobLatitude() {
        return jobLatitude;
    }

    public void setJobLatitude(String jobLatitude) {
        this.jobLatitude = jobLatitude;
    }

    public String getJobLongitude() {
        return jobLongitude;
    }

    public void setJobLongitude(String jobLongitude) {
        this.jobLongitude = jobLongitude;
    }

    public String getJobDealStatus() {
        return jobDealStatus;
    }

    public void setJobDealStatus(String jobDealStatus) {
        this.jobDealStatus = jobDealStatus;
    }

    public String getJobPhoneVisibleStatus() {
        return jobPhoneVisibleStatus;
    }

    public void setJobPhoneVisibleStatus(String jobPhoneVisibleStatus) {
        this.jobPhoneVisibleStatus = jobPhoneVisibleStatus;
    }

    public String getJobUploadedDate() {
        return jobUploadedDate;
    }

    public void setJobUploadedDate(String jobUploadedDate) {
        this.jobUploadedDate = jobUploadedDate;
    }

    public String getJobEditedDate() {
        return jobEditedDate;
    }

    public void setJobEditedDate(String jobEditedDate) {
        this.jobEditedDate = jobEditedDate;
    }

    public String getJobValidity() {
        return jobValidity;
    }

    public void setJobValidity(String jobValidity) {
        this.jobValidity = jobValidity;
    }

    public String getJobSalaryType() {
        return jobSalaryType;
    }

    public void setJobSalaryType(String jobSalaryType) {
        this.jobSalaryType = jobSalaryType;
    }

    public String getJobQualification() {
        return jobQualification;
    }

    public void setJobQualification(String jobQualification) {
        this.jobQualification = jobQualification;
    }
}
