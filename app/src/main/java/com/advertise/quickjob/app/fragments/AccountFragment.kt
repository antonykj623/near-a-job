package com.advertise.quickjob.app.fragments


import android.Manifest
import android.app.Activity.RESULT_OK
import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView

import com.advertise.quickjob.app.R
import com.advertise.quickjob.app.preferencehelper.PreferenceHelper
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.net.Uri
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat.checkSelfPermission

import androidx.core.app.ActivityCompat.startActivityForResult
import android.provider.MediaStore

import java.util.Collections.rotate
import android.R.attr.bitmap
import android.graphics.BitmapFactory
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import android.R.attr.data
import android.app.Dialog
import android.graphics.Bitmap
import android.view.WindowManager
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.widget.AppCompatButton
import com.advertise.quickjob.app.Utils.Constants
import com.advertise.quickjob.app.Utils.DownloadUtils
import com.advertise.quickjob.app.domain.Tokendata
import com.advertise.quickjob.app.domain.UserDetails
import com.advertise.quickjob.app.progress.ProgressFragment
import com.advertise.quickjob.app.views.*
import com.advertise.quickjob.app.webservicehelper.Retrofithelper
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.gson.JsonObject
import kotlinx.android.synthetic.main.activity_registration.*

import kotlinx.android.synthetic.main.fragment_account.view.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.lang.Exception


/**
 * A simple [Fragment] subclass.
 */
class AccountFragment : Fragment(),View.OnClickListener {

    var v:View?=null



    val REQUEST_CODE=122

    val PICK_IMAGE=133


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        v= inflater.inflate(R.layout.fragment_account, container, false)


     v!!.imglogout!!.setOnClickListener(this)

        v!!.imgedit!!.setOnClickListener(this)
        v!!. txtlogout!!.setOnClickListener(this)

        v!!. txtdelete!!.setOnClickListener(this)
        v!!.  imgdelete!!.setOnClickListener(this)
        v!!.imgeditUser!!.setOnClickListener(this)
        v!!.edit!!.setOnClickListener(this)
        v!!.cardAppliedJob.setOnClickListener(this)

        v!!.cardWishlist.setOnClickListener(this)

        getProfileImage()

        showUserDetails()

        return v!!;
    }


    override fun onClick(p0: View?) {

        when(p0!!.id)
        {


            R.id.cardWishlist->{

                val intent = Intent(activity, FavouritesActivity::class.java)
                startActivity(intent)

            }




            R.id.cardAppliedJob->{


                val intent = Intent(activity, AppliedJobsActivity::class.java)
                            startActivity(intent)




            }




            R.id.txtlogout->{


                val builder = AlertDialog.Builder(context)
                builder.setTitle("Near a job")
                builder.setMessage("Do you want to logout now ?")
                builder.setNegativeButton(
                    "yes"
                ) {


                        dialog, which -> dialog.dismiss()

                    PreferenceHelper(activity).clearData()

                    val intent = Intent(activity, LoginActivity::class.java)
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(intent)
                }


                builder.setPositiveButton(
                    "no"
                ) {


                        dialog, which -> dialog.dismiss()


                }
                val alertDialog = builder.create()
                alertDialog.show()


            }


            R.id.imglogout->{


                val builder = AlertDialog.Builder(context)
                builder.setTitle("Near a job")
                builder.setMessage("Do you want to logout now ?")
                builder.setNegativeButton(
                    "yes"
                ) {


                        dialog, which -> dialog.dismiss()

                    PreferenceHelper(activity).clearData()

                    val intent = Intent(activity, LoginActivity::class.java)
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(intent)
                }


                builder.setPositiveButton(
                    "no"
                ) {


                        dialog, which -> dialog.dismiss()


                }
                val alertDialog = builder.create()
                alertDialog.show()


            }

            R.id.imgedit->{


                if (checkSelfPermission(context!!,android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {


                    ActivityCompat.requestPermissions(activity!!, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), REQUEST_CODE);

                }
                else{

                    val gallery =
                        Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI)
                    startActivityForResult(gallery, PICK_IMAGE)

                }


            }

            R.id.imgdelete->{

                showOpiniondialog()

            }


            R.id.txtdelete->{

                showOpiniondialog()

            }

            R.id.imgeditUser->{


                startActivity(Intent(activity!!,UserProfileEditActivity::class.java))

            }


            R.id.edit->{

                startActivity(Intent(activity!!,UserProfileEditActivity::class.java))


            }

        }

    }












    fun showOpiniondialog()
    {
        val d= Dialog(activity!!);
        d.setContentView(R.layout.layout_opinion)

        val window = d.getWindow();
        window!!.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);


        val input_name=d.findViewById<EditText>(R.id.input_name)

        val input_reason=d.findViewById<EditText>(R.id.input_reason)

        val btn_submit=d.findViewById<AppCompatButton>(R.id.btn_submit)


        btn_submit.setOnClickListener {


            if(!input_name!!.text.toString().equals("")) {


                if (!input_reason!!.text.toString().equals("")) {


d.dismiss()




                    var p: ProgressFragment = ProgressFragment()
                    p.show(activity!!.supportFragmentManager,"dhjjd")
                    val retrofithelper = Retrofithelper()

                    var a: Call<JsonObject> = retrofithelper.getClient().UserDeleteAccount(PreferenceHelper(activity!!).getData(Constants.tokenkey),input_name!!.text.toString(),input_reason!!.text.toString())


                    a.enqueue(object : Callback<JsonObject> {
                        override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {

                            p.dismiss()

                            if(response.body()!=null)
                            {

                                var p=response.body();

                                try{



                                    val json=JSONObject(p.toString())

                                    if(json.getInt("status")==1)
                                    {

                                        PreferenceHelper(activity!!).clearData()


                                        Toast.makeText(activity!!,json.getString("message"),Toast.LENGTH_SHORT).show()

                                        val intent=Intent(activity!!,LoginActivity::class.java)

                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        activity!!.startActivity(intent)
                                    }
                                    else{




                                        Toast.makeText(activity!!,json.getString("message"),Toast.LENGTH_SHORT).show()
                                    }







                                }catch (e:Exception)
                                {

                                }










                            }






                        }

                        override fun onFailure(call: Call<JsonObject>, t: Throwable) {

                            p.dismiss()

                            if(!DownloadUtils().isNetworkConnected(activity))
                            {
                                DownloadUtils().showNoConnectiondialog(activity);
                            }

                            t.printStackTrace()



                            //getProfileImage.php


                        }
                    })




























                }

                else{

                    Toast.makeText(activity!!,"Enter reason of deletion",Toast.LENGTH_SHORT).show()



                }

            }
            else{

                Toast.makeText(activity!!,"Enter name",Toast.LENGTH_SHORT).show()



            }




        }



        d.show();
    }











     override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {



        if (requestCode == PICK_IMAGE  && resultCode == RESULT_OK
            && null != data
        ) {

            val selectedImage = data.data

            val d= Dialog(activity!!);
            d.setContentView(R.layout.layout_cropdialog)

            val imgback=d.findViewById<ImageView>(R.id.imgback)

            val imgcrop=d.findViewById<ImageView>(R.id.imgcrop)

            val cropImageView=d.findViewById<CropImageView>(R.id.cropImageView)

            cropImageView.setImageUriAsync(selectedImage)


            imgback.setOnClickListener {

                d.dismiss()
            }

            imgcrop.setOnClickListener {

                d.dismiss()

                val cropped = cropImageView.getCroppedImage();



//var f=File(Constants().getTempstorageFilePath(activity!!))
//
//                if(!f.exists())
//                {
//
//                    f.createNewFile()
//                }
//var bytes =  ByteArrayOutputStream();
//    cropped.compress(Bitmap.CompressFormat.PNG, 60, bytes);
//
//                 var fo =  FileOutputStream(f);
//    fo.write(bytes.toByteArray());
//    fo.close();



uploadImageToServer(cropped)





            }


            val window = d.getWindow();
            window!!.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);

            d.show();



        }



    }




    fun uploadImageToServer(cropped:Bitmap)
    {




        var p: ProgressFragment = ProgressFragment()
        p.show(activity!!.supportFragmentManager,"dhjjd")





        var f=File(Constants.getTempstorageFilePath(activity!!))

        if(!f.exists())
        {

            f.createNewFile()
        }
        var bytes =  ByteArrayOutputStream();
        cropped.compress(Bitmap.CompressFormat.PNG, 60, bytes);

        var fo =  FileOutputStream(f);
        fo.write(bytes.toByteArray());
        fo.close();

        var requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), f);

        var body = MultipartBody.Part.createFormData("file", f.getName(), requestFile);


        var descBody = RequestBody.create(MediaType.parse("text/plain"), f.name);






        val retrofithelper = Retrofithelper()

        var a: Call<JsonObject> = retrofithelper.getClient().Uploadprofileimg(body,descBody,PreferenceHelper(activity!!).getData(Constants.tokenkey))



        a.enqueue(object : Callback<JsonObject> {
            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {

                p.dismiss()

                if(response.body()!=null)
                {

                    var p=response.body();

                    try{

                        val json=JSONObject(p.toString())

                        if(json.getInt("status")==1)
                        {

                            Toast.makeText(activity!!,json.getString("message"),Toast.LENGTH_SHORT).show()
                            getProfileImage()
                        }
                        else{




                            Toast.makeText(activity!!,json.getString("message"),Toast.LENGTH_SHORT).show()
                        }





                    }catch (e:Exception)
                    {

                    }










                }






            }

            override fun onFailure(call: Call<JsonObject>, t: Throwable) {

                p.dismiss()

                if(!DownloadUtils().isNetworkConnected(activity))
                {
                    DownloadUtils().showNoConnectiondialog(activity);
                }

                t.printStackTrace()



                //getProfileImage.php


            }
        })

    }




    fun getProfileImage()
    {

        var p: ProgressFragment = ProgressFragment()
        p.show(activity!!.supportFragmentManager,"dhjjd")
        val retrofithelper = Retrofithelper()

        var a: Call<JsonObject> = retrofithelper.getClient().Getprofileimg(PreferenceHelper(activity!!).getData(Constants.tokenkey))



        a.enqueue(object : Callback<JsonObject> {
            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {

                p.dismiss()

                if(response.body()!=null)
                {

                    var p=response.body();

                    try{

                        val json=JSONObject(p.toString())

                        if(json.getInt("status")==1)
                        {

                            var img=json.getString("image");



                            if(img!=null&&!img.equals("")) {


                                Glide.with(activity!!).load(Constants.profileimg + img).apply(
                                    RequestOptions.circleCropTransform()
                                ).into(v!!.profile)
                            }



                        }
                        else{






                                                  }





                    }catch (e:Exception)
                    {

                    }










                }






            }

            override fun onFailure(call: Call<JsonObject>, t: Throwable) {

                p.dismiss()

                if(!DownloadUtils().isNetworkConnected(activity))
                {
                    DownloadUtils().showNoConnectiondialog(activity);
                }

                t.printStackTrace()



                //getProfileImage.php


            }
        })



    }





    fun showUserDetails()
    {



        val retrofithelper = Retrofithelper()

        var a: Call<UserDetails> = retrofithelper.getClient().getUserDetails(PreferenceHelper(activity!!).getData(Constants.tokenkey))


        a.enqueue(object : Callback<UserDetails> {
            override fun onResponse(call: Call<UserDetails>, response: Response<UserDetails>) {



                if(response.body()!=null)
                {

                    var p=response.body();

                    try{



                        if(p!!.status==1)
                        {

                            v!!. username.setText(p!!.user_name)
                            v!!.  mobile.setText(p!!.userPhone)
                            v!!.email.setText(p!!.userEmail)

                            v!!. dob.setText(p!!.userDob)
                            v!!. txtQualification.setText(p!!.userQualificationName)
                            v!!.txtAddress.setText(p!!.userAddress)



                        }
                        else{






                        }





                    }catch (e:Exception)
                    {

                    }










                }






            }

            override fun onFailure(call: Call<UserDetails>, t: Throwable) {



                if(!DownloadUtils().isNetworkConnected(activity))
                {
                    DownloadUtils().showNoConnectiondialog(activity);
                }

                t.printStackTrace()



                //getProfileImage.php


            }
        })




    }

}
