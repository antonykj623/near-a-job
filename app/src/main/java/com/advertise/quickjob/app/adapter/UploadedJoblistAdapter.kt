package com.advertise.quickjob.app.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.advertise.quickjob.app.R
import com.advertise.quickjob.app.domain.JobDetails
import com.advertise.quickjob.app.views.JobdetailsActivity
import com.advertise.quickjob.app.views.MyJobDetailsActivity

class UploadedJoblistAdapter(context: Context,list: ArrayList<JobDetails>):RecyclerView.Adapter<UploadedJoblistAdapter.UploadedJoblistHolder>() {


    var con=context
    var li=list

    class UploadedJoblistHolder(v: View) : RecyclerView.ViewHolder(v)
    {

        var title=v.findViewById<TextView>(R.id.title)

        var txtwishlist=v.findViewById<TextView>(R.id.txtwishlist)

        var txtuploadDate=v.findViewById<TextView>(R.id.txtuploadDate)

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UploadedJoblistHolder {


        var v= LayoutInflater.from(parent.context).inflate(R.layout.layout_uploadedjobholder,parent,false)

return UploadedJoblistHolder(v)


    }

    override fun getItemCount(): Int {

return li.size

    }

    override fun onBindViewHolder(holder: UploadedJoblistHolder, position: Int) {

        holder.title.setText(li.get(position).jobTitle)

        holder.txtwishlist.setText(li.get(position).jobDesccription)

        holder.txtuploadDate.setText(li.get(position).jobUploadedDate)

        holder.itemView.setOnClickListener {

            var intent= Intent(con, MyJobDetailsActivity::class.java)
            intent.putExtra("jobdetails",li.get(position))

            con.startActivity(intent)

        }



    }
}