package com.advertise.quickjob.app.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ChatHistoryBundle implements Serializable {

    List<ChatMessage>chatHistories=new ArrayList<>();

    public List<ChatMessage> getChatHistories() {
        return chatHistories;
    }

    public void setChatHistories(List<ChatMessage> chatHistories) {
        this.chatHistories = chatHistories;
    }
}
