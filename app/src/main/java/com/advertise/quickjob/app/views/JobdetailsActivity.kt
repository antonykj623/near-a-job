package com.advertise.quickjob.app.views

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.advertise.quickjob.app.R
import com.advertise.quickjob.app.Utils.Constants
import com.advertise.quickjob.app.domain.JobDetails
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.activity_jobdetails.*
import kotlinx.android.synthetic.main.fragment_account.view.*

import com.google.android.gms.maps.SupportMapFragment

import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T

import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.LatLng

import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.view.View
import com.google.android.gms.maps.CameraUpdateFactory
import android.content.Intent

import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T

import android.net.Uri
import android.widget.Toast
import com.advertise.quickjob.app.Utils.DownloadUtils
import com.advertise.quickjob.app.domain.JobApplieds
import com.advertise.quickjob.app.preferencehelper.PreferenceHelper
import com.advertise.quickjob.app.progress.ProgressFragment
import com.advertise.quickjob.app.webservicehelper.Retrofithelper
import com.google.gson.JsonObject
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*


class JobdetailsActivity : AppCompatActivity(), OnMapReadyCallback {

    var jb :JobDetails?=null

    var cont:Context?=null


    var isfavourite=false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_jobdetails)
        supportActionBar!!.hide()

cont=this


        jb=intent.getSerializableExtra("jobdetails") as JobDetails

        imgback.setOnClickListener {

            onBackPressed()
        }




        imgfavourite.setOnClickListener {


            if(isfavourite)
            {
                removeFromFavourites(jb!!.job_id.toInt(),0)
            }
            else{

                addtoFavourites(jb!!.job_id.toInt(),0)
            }


        }



        txttitle.setText(jb!!.jobTitle)
        txtDescription.setText(jb!!.jobDesccription)


        var c=jb!!.jobSalaryInfo.split("to")

        var a1=c[0].trim()

        var a2=c[1].trim()

        var maximumsalary=a2.replace("Rs","")

        var a3=a1.replace("Between","")

        var minimumsalary=a3.replace("Rs","")


        txtSalaryinfo.setText(getString(R.string.rs)+""+minimumsalary+"-"+maximumsalary)
        txtSalarytype.setText("Salary type : "+jb!!.jobSalaryType)
        txtJobValidity.setText(" Work validity :  "+jb!!.jobValidity)

        var pro=Constants.profileimg + jb!!.jobUser.user_image;

        Glide.with(this).load(pro).apply(
            RequestOptions.circleCropTransform()
        ).into(profile)

        username.setText(jb!!.jobUser.userName)

        txtQualification.setText("Qualification : "+jb!!.qualificationName)

        txtAddress.setText(jb!!.jobAddress)


        if(jb!!.jobLatitude.toDouble()!=0.0&&jb!!.jobLongitude.toDouble()!=0.0) {
            val mapFragment = supportFragmentManager
                .findFragmentById(com.advertise.quickjob.app.R.id.map) as SupportMapFragment?
            mapFragment!!.getMapAsync(this)

            card_location.visibility= View.VISIBLE
        }
        else{

            card_location.visibility= View.GONE
        }

        if(jb!!.jobPhoneVisibleStatus.toInt()==1)
        {
            fabcall.visibility=View.GONE
        }
        else{
            fabcall.visibility=View.VISIBLE
        }

        fabcall.setOnClickListener {


            val intent = Intent(Intent.ACTION_DIAL)
            intent.data = Uri.parse("tel:"+jb!!.jobUser.userPhone)
            startActivity(intent)
        }

        fab_apply.setOnClickListener {


            val builder = AlertDialog.Builder(this)
            builder.setTitle("Near a job")
            builder.setMessage("Do you want to apply this job ?")
            builder.setNegativeButton(
                "no"
            ) { dialog, which -> dialog.dismiss() }


            builder.setPositiveButton(
                "yes"
            ) { dialog, which -> dialog.dismiss()

applyJob()





            }



            val alertDialog = builder.create()
            alertDialog.show()
        }


        checkfavourites()

        checkUser()





    }


    fun checkUser()
    {

        var progressFragment=ProgressFragment()
        progressFragment.show(supportFragmentManager,"")


        val retrofithelper = Retrofithelper()

        var a: Call<JsonObject> = retrofithelper.getClient().checkJobUser(
            PreferenceHelper(this).getData(
                Constants.tokenkey),
            jb!!.job_id)


        a.enqueue(object : Callback<JsonObject> {
            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {

                progressFragment.dismiss()

                if(response.body()!=null)
                {

                    var pa=response.body();

                    try{

                        if(pa!=null) {

                            try{

                                var json=JSONObject(pa.toString())

                                if(json.getInt("status")==1)
                                {



                                   // Toast.makeText(this@JobdetailsActivity,json.getString("message"),Toast.LENGTH_SHORT).show()

                                    fab_apply.visibility=View.GONE
                                    layout_normal.visibility=View.GONE




                                }


                                else{

                                    checkJobApplied()

                                }








                            }catch (ex:Exception)
                            {

                            }


                        }




                    }catch (e: Exception)
                    {

                    }










                }






            }

            override fun onFailure(call: Call<JsonObject>, t: Throwable) {

                progressFragment.dismiss()

                if(!DownloadUtils().isNetworkConnected(this@JobdetailsActivity))
                {
                    DownloadUtils().showNoConnectiondialog(this@JobdetailsActivity);
                }

                t.printStackTrace()






            }
        })
    }






    fun checkfavourites()
    {
        var progressFragment=ProgressFragment()
        progressFragment.show(supportFragmentManager,"")


        val retrofithelper = Retrofithelper()

        var a: Call<JsonObject> = retrofithelper.getClient().checkFavourite(
            PreferenceHelper(this).getData(
                Constants.tokenkey),
            jb!!.job_id)


        a.enqueue(object : Callback<JsonObject> {
            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {

                progressFragment.dismiss()

                if(response.body()!=null)
                {

                    var pa=response.body();

                    try{

                        if(pa!=null) {

                            try{

                                var json=JSONObject(pa.toString())

                                if(json.getInt("status")==1)
                                {




                                        isfavourite=true


                                      //  Toast.makeText(this@JobdetailsActivity,"Added to your favourites", Toast.LENGTH_SHORT).show()

                                        imgfavourite.setImageResource(R.drawable.ic_favorite)




                                }
                                else{

                                imgfavourite.setImageResource(R.drawable.ic_favorite_border_black)
                                isfavourite=false

                                }








                            }catch (ex:Exception)
                            {

                            }


                        }




                    }catch (e: Exception)
                    {

                    }










                }






            }

            override fun onFailure(call: Call<JsonObject>, t: Throwable) {

                progressFragment.dismiss()

                if(!DownloadUtils().isNetworkConnected(this@JobdetailsActivity))
                {
                    DownloadUtils().showNoConnectiondialog(this@JobdetailsActivity);
                }

                t.printStackTrace()






            }
        })
    }
























    fun addtoFavourites(jobid:Int,position: Int)
    {
        var p: ProgressFragment = ProgressFragment()
        p.show((cont  as JobdetailsActivity).supportFragmentManager,"dhjjd")
        val retrofithelper = Retrofithelper()

        var a: Call<JsonObject> = retrofithelper.getClient().addtoFavourites(
            PreferenceHelper(cont).getData(Constants.tokenkey),jobid.toString())


        a.enqueue(object : Callback<JsonObject> {
            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {

                p.dismiss()

                if(response.body()!=null)
                {

                    var p=response.body();

                    try{



                        val json= JSONObject(p.toString())

                        if(json.getInt("status")==1)
                        {


                            isfavourite=true
                            imgfavourite.setImageResource(R.drawable.ic_favorite)

                        }
                        else{




                            Toast.makeText(cont,json.getString("message"), Toast.LENGTH_SHORT).show()
                        }







                    }catch (e: Exception)
                    {

                    }










                }






            }

            override fun onFailure(call: Call<JsonObject>, t: Throwable) {

                p.dismiss()

                if(!DownloadUtils().isNetworkConnected(cont))
                {
                    DownloadUtils().showNoConnectiondialog(cont);
                }

                t.printStackTrace()



                //getProfileImage.php


            }
        })
    }





    fun removeFromFavourites(jobid:Int,position: Int)
    {
        var p: ProgressFragment = ProgressFragment()
        p.show((cont  as JobdetailsActivity).supportFragmentManager,"dhjjd")
        val retrofithelper = Retrofithelper()

        var a: Call<JsonObject> = retrofithelper.getClient().removeFromFavourites(
            PreferenceHelper(cont).getData(Constants.tokenkey),jobid.toString())


        a.enqueue(object : Callback<JsonObject> {
            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {

                p.dismiss()

                if(response.body()!=null)
                {

                    var p=response.body();

                    try{



                        val json= JSONObject(p.toString())

                        if(json.getInt("status")==1)
                        {


                            isfavourite=false

                            Toast.makeText(cont,json.getString("message"), Toast.LENGTH_SHORT).show()



                        }
                        else{




                            Toast.makeText(cont,json.getString("message"), Toast.LENGTH_SHORT).show()
                        }







                    }catch (e: Exception)
                    {

                    }










                }






            }

            override fun onFailure(call: Call<JsonObject>, t: Throwable) {

                p.dismiss()

                if(!DownloadUtils().isNetworkConnected(cont))
                {
                    DownloadUtils().showNoConnectiondialog(cont);
                }

                t.printStackTrace()



                //getProfileImage.php


            }
        })
    }



    fun checkJobApplied()
    {






        val retrofithelper = Retrofithelper()

        var a: Call<JsonObject> = retrofithelper.getClient().checkGivenAppliedJob(
            PreferenceHelper(this@JobdetailsActivity).getData(Constants.tokenkey),jb!!.job_id);

        a.enqueue(object : Callback<JsonObject> {
            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {


                if(response.body()!=null)
                {

                    var p=response.body();

                    try{



                        val json= JSONObject(p.toString())

                        if(json.getInt("status")==1)
                        {

                            fab_apply.visibility=View.GONE
                            layout_normal.visibility=View.GONE

                            Toast.makeText(this@JobdetailsActivity,json.getString("message"), Toast.LENGTH_SHORT).show()

                        }

                        else{

                            fab_apply.visibility=View.VISIBLE

                            layout_normal.visibility=View.VISIBLE

                        }







                    }catch (e: Exception)
                    {

                    }










                }






            }

            override fun onFailure(call: Call<JsonObject>, t: Throwable) {



                if(!DownloadUtils().isNetworkConnected(this@JobdetailsActivity))
                {
                    DownloadUtils().showNoConnectiondialog(this@JobdetailsActivity);
                }

                t.printStackTrace()



                //getProfileImage.php


            }
        })


    }




    fun applyJob()
    {


var dateFormat =  SimpleDateFormat("dd-MMM-yyyy");
            var today = Calendar.getInstance().getTime();

        var date_applied=dateFormat.format(today);

        var p: ProgressFragment = ProgressFragment()
        p.show(supportFragmentManager,"dhjjd")
        val retrofithelper = Retrofithelper()

        var a: Call<JsonObject> = retrofithelper.getClient().applyJob(
            PreferenceHelper(this@JobdetailsActivity).getData(Constants.tokenkey),jb!!.job_id,jb!!.jobUserId,"0",date_applied)


        a.enqueue(object : Callback<JsonObject> {
            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {

                p.dismiss()

                if(response.body()!=null)
                {

                    var p=response.body();

                    try{



                        val json= JSONObject(p.toString())

                        if(json.getInt("status")==1)
                        {

                            checkJobApplied()

                            Toast.makeText(this@JobdetailsActivity,json.getString("message"), Toast.LENGTH_SHORT).show()

                        }
                        else{




                            Toast.makeText(this@JobdetailsActivity,json.getString("message"), Toast.LENGTH_SHORT).show()
                        }







                    }catch (e: Exception)
                    {

                    }










                }






            }

            override fun onFailure(call: Call<JsonObject>, t: Throwable) {

                p.dismiss()

                if(!DownloadUtils().isNetworkConnected(this@JobdetailsActivity))
                {
                    DownloadUtils().showNoConnectiondialog(this@JobdetailsActivity);
                }

                t.printStackTrace()



                //getProfileImage.php


            }
        })
    }

    override fun onMapReady(p0: GoogleMap?) {

        val sydney = LatLng(jb!!.jobLatitude.toDouble(), jb!!.jobLongitude.toDouble())
        p0!!.addMarker(
            MarkerOptions()
                .position(sydney)
                .title(jb!!.jobAddress)


            );


        p0!!.animateCamera( CameraUpdateFactory.newLatLngZoom(
                LatLng(
                    jb!!.jobLatitude.toDouble(), jb!!.jobLongitude.toDouble()
                ), 16.0f))


    }
}
