package com.advertise.quickjob.app.views

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.graphics.Bitmap
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.advertise.quickjob.app.R
import com.advertise.quickjob.app.Utils.Constants
import com.advertise.quickjob.app.adapter.ChatroomAdapter
import com.advertise.quickjob.app.domain.ChatRoom
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.activity_chatroom.*
import kotlinx.android.synthetic.main.fragment_account.view.*
import androidx.core.app.ComponentActivity.ExtraData
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.provider.MediaStore
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.WindowManager
import android.widget.ImageView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.advertise.quickjob.app.Utils.DownloadUtils
import com.advertise.quickjob.app.adapter.ChatMessageAdapter
import com.advertise.quickjob.app.adapter.layoutmanager.ChatroomLayoutManager
import com.advertise.quickjob.app.backgroundservices.ChatHistoryService
import com.advertise.quickjob.app.domain.ChatHistoryBundle
import com.advertise.quickjob.app.domain.ChatMessage
import com.advertise.quickjob.app.preferencehelper.PreferenceHelper
import com.advertise.quickjob.app.webservicehelper.Retrofithelper
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import com.theartofdev.edmodo.cropper.CropImageView
import kotlinx.android.synthetic.main.fragment_home.view.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.*
import java.lang.Exception
import java.text.DateFormat
import java.util.*
import kotlin.collections.ArrayList


class ChatroomActivity : AppCompatActivity() {

    var chatroom: ChatRoom? = null
    var logs = ArrayList<ChatMessage>()

    var timer:Timer?=null

    val maprqcode=18999;

    val PICK_IMAGE=9878

    val REQUEST_CODE=11

    var istouched=false

    var scrolledposition=0

    var hourlyTask:TimerTask?=null

    var chatroomAdapter:ChatMessageAdapter?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chatroom)
        supportActionBar!!.hide()

        chatroom = intent.getSerializableExtra("chatroom") as ChatRoom






        imgback!!.setOnClickListener {

            onBackPressed()
        }

        fabSend!!.setOnClickListener {

            if (!edtMessage.text.toString().equals("")) {


                postMessages(edtMessage.text.toString(), 1)



            } else {


                Toast.makeText(this, "Enter the message", Toast.LENGTH_SHORT).show()
            }


        }

        imgpicklocation!!.setOnClickListener {


            startActivityForResult(Intent(this,ChooseCurrentlocationActivity::class.java),maprqcode)

        }

        imgpickgallery.setOnClickListener {



            loadGallery()

        }



        setupChatroom()


        createChatFolder()


         timer = Timer()
         hourlyTask = object : TimerTask() {
            override fun run() {


               // createChatFolder()


               val intent= Intent(this@ChatroomActivity, ChatHistoryService::class.java)
                intent.putExtra("chatroom",chatroom!!.chatroomid)



                startService(intent );

            }
        }


        timer!!.schedule(hourlyTask, 2000, 6000);








        var fjsondata = readjsonfile()

        if (!fjsondata.equals("")) {

            var gs = GsonBuilder().create()

            val myType = object : TypeToken<ArrayList<ChatMessage>>() {}.type
            logs = gs.fromJson<ArrayList<ChatMessage>>(fjsondata, myType)

            var chatroomLayoutManager= ChatroomLayoutManager(this@ChatroomActivity)

            chatroomLayoutManager.stackFromEnd=true
            recyclermsg.layoutManager = chatroomLayoutManager

      checkdate(logs)


            chatroomAdapter= ChatMessageAdapter(logs, this@ChatroomActivity)

            recyclermsg.adapter=chatroomAdapter
            recyclermsg.scrollToPosition(logs.size-1)

            scrolledposition=logs.size-1

        }
        else{

         checkdate(logs)

            var chatroomLayoutManager = ChatroomLayoutManager(this@ChatroomActivity)
            chatroomLayoutManager.stackFromEnd=true

            recyclermsg.layoutManager=chatroomLayoutManager

             chatroomAdapter = ChatMessageAdapter(logs, this@ChatroomActivity)

            recyclermsg.adapter = ChatMessageAdapter(logs, this@ChatroomActivity)
        }








    }





    fun checkdate(p:ArrayList<ChatMessage>):ArrayList<ChatMessage>
    {

        var date=""
        var plist=ArrayList<ChatMessage>()

        for (i in p)
        {

          if(date.equals(i.chat_senddate))
          {

              plist.add(i)

          }
            else{



              if(!i.chatId.equals("0")&&i.chatMessage!=null) {
                  date = i.chat_senddate

                  var c = ChatMessage()
                  c.chatId = "0"
                  c.chat_senddate = i.chat_senddate



                  plist.add(c)

                  plist.add(i)

              }


          }
        }

        logs.clear()

        logs.addAll(plist)


        return logs

    }
















    override fun onDestroy() {
        super.onDestroy()

        if(hourlyTask!=null)
        {
            hourlyTask!!.cancel()
        }

        if(chatmsgreceiver!=null)
        {

            unregisterReceiver(chatmsgreceiver)
        }
    }


    fun createChatFolder() {


        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) !== PackageManager.PERMISSION_GRANTED
        ) {


            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                119
            )

        } else {


            var s = this.externalCacheDir.toString() + "/" + chatroom!!.chatroomid

            var f = File(s)

            if (!f.exists()) {

                f.mkdir()
            }

            var fpath = f.absolutePath + "/" + chatroom!!.chatroomid + ".json"

            var fjson = File(fpath)
            if (!fjson.exists()) {

                fjson.createNewFile()
            }



          //  getAllMessages()

        }


    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)


        if(requestCode==119) {

            createChatFolder()
        }
        else{

            loadGallery()

        }
    }


    fun loadGallery()
    {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED) {


            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), REQUEST_CODE);

        }
        else{

            val gallery =
                Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI)
            startActivityForResult(gallery, PICK_IMAGE)

        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode==maprqcode&&resultCode==255&&data!=null)
        {


          var  lat=data.getDoubleExtra("lat",0.0)
          var  longi=data.getDoubleExtra("longitude",0.0)
            var location=data.getStringExtra("locationName")

            var message=lat.toString()+","+longi.toString()+","+location

            postMessages(message,3)



        }

        if (requestCode == PICK_IMAGE  && resultCode == Activity.RESULT_OK
            && null != data
        ) {


            val selectedImage = data.data

            val d= Dialog(this);
            d.setContentView(R.layout.layout_cropbeforesend)

            val imgback=d.findViewById<ImageView>(R.id.imgback)

            val imgcrop=d.findViewById<FloatingActionButton>(R.id.imgcrop)

            val cropImageView=d.findViewById<CropImageView>(R.id.cropImageView)

            cropImageView.setImageUriAsync(selectedImage)


            imgback.setOnClickListener {

                d.dismiss()
            }

            imgcrop.setOnClickListener {

                d.dismiss()

                val cropped = cropImageView.getCroppedImage();





                uploadImageToServer(cropped)





            }


            val window = d.getWindow();
            window!!.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);

            d.show();



        }




    }



    fun uploadImageToServer(cropped: Bitmap)
    {

        var f=File(Constants.getTempstorageFilePathForchat(this,chatroom!!.chatroomid.toString()))

        if(!f.exists())
        {

            f.createNewFile()
        }
        var bytes =  ByteArrayOutputStream();
        cropped.compress(Bitmap.CompressFormat.PNG, 60, bytes);

        var fo =  FileOutputStream(f);
        fo.write(bytes.toByteArray());
        fo.close();



        var requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), f);

        var body = MultipartBody.Part.createFormData("file", f.getName(), requestFile);


        var descBody = RequestBody.create(MediaType.parse("text/plain"), f.name);


        var chatroomid= RequestBody.create(MediaType.parse("text/plain"), chatroom!!.chatroomid.toString());


        var type= RequestBody.create(MediaType.parse("text/plain"), "2");

        var receiverid= RequestBody.create(MediaType.parse("text/plain"), chatroom!!.user.userId);



        //  , "", "2",

        var mydate = DateFormat.getDateInstance().format(Calendar.getInstance().getTime());

        var mytime= DateFormat.getTimeInstance().format(Calendar.getInstance().getTime());

        var requestbodytime= RequestBody.create(MediaType.parse("text/plain"), mydate);

        var requestbodytimedata= RequestBody.create(MediaType.parse("text/plain"), mytime);

        val retrofithelper = Retrofithelper()

        var a: Call<JsonObject> = retrofithelper.getClient().sendImagetoUser(body,descBody,PreferenceHelper(this).getData(Constants.tokenkey),chatroomid, descBody, type, receiverid,requestbodytime,requestbodytimedata)



        a.enqueue(object : Callback<JsonObject> {
            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {



                if(response.body()!=null)
                {

                    var p=response.body();

                    try{

                        val json=JSONObject(p.toString())

                        if(json.getInt("status")==1)
                        {

                            var msgid=json.getInt("messageid");

                            var chatMessage=ChatMessage()
                            chatMessage.chatMessageType="2"
                            chatMessage.chatMessage=f.name
                            chatMessage.chatRoomid=chatroom!!.chatroomid.toString()
                            chatMessage.chatReceiverid=chatroom!!.receiverid
                            chatMessage.chatSenderid=PreferenceHelper(this@ChatroomActivity).getData(Constants.userid)
                            chatMessage.chat_senddate=mydate
                            chatMessage.chat_sendtime=mytime
                            chatMessage.chatId=msgid.toString()



                            logs.add(chatMessage)

                            checkdate(logs)

                            val gson = Gson();

                            var listString = gson.toJson(
                                logs
                            );


                            writeDatatoFile(listString)

//                            if(chatroomAdapter!=null)
//                            {
                            chatroomAdapter!!.notifyDataSetChanged()

                            recyclermsg!!.smoothScrollToPosition(logs.size-1)
                            }

                       // }
                        else{




                            Toast.makeText(this@ChatroomActivity,json.getString("message"),Toast.LENGTH_SHORT).show()
                        }





                    }catch (e:Exception)
                    {

                    }










                }






            }

            override fun onFailure(call: Call<JsonObject>, t: Throwable) {

              //  p.dismiss()

                if(!DownloadUtils().isNetworkConnected(this@ChatroomActivity))
                {
                    DownloadUtils().showNoConnectiondialog(this@ChatroomActivity);
                }

                t.printStackTrace()



                //getProfileImage.php


            }
        })



    }

















    override fun onStart() {
        super.onStart()
          var intentFilter =  IntentFilter();
      intentFilter.addAction("com.quickjob.advertise.job");
      registerReceiver(chatmsgreceiver, intentFilter);
    }








    private val chatmsgreceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {

            var data=intent.getSerializableExtra("DATAPASSED") as ChatHistoryBundle;


            var unreadmsg = ArrayList<ChatMessage>()

            var p = data.chatHistories

           // var p= checkdate(logs)

            var fjsondata = readjsonfile()

            if (!fjsondata.equals("")) {

                var gs = GsonBuilder().create()

                val myType = object : TypeToken<ArrayList<ChatMessage>>() {}.type
              var  logsfile = gs.fromJson<ArrayList<ChatMessage>>(fjsondata, myType)




                for (j in p) {


                    if (!checkMessagesInFile(logsfile, j)) {

                        logs.add(j)

                        unreadmsg.add(j)

                    }


                }


                if (unreadmsg.size > 0) {


                    val gson = Gson();

                    var listStringdata = gson.toJson(
                        logs
                    );


                    writeDatatoFile(listStringdata)
                }


            } else {


                unreadmsg.addAll(p)
                logs.addAll(p)
                val gson = Gson();

                var listString = gson.toJson(
                    p
                );


                writeDatatoFile(listString)


            }







            if(unreadmsg.size>0) {


                checkdate(logs)

                chatroomAdapter!!.notifyDataSetChanged()





                recyclermsg.smoothScrollToPosition(logs.size-1)

            }




        }
    }















    fun postMessages(msg: String, type: Int) {

        val retrofithelper = Retrofithelper()

        edtMessage.setText("")



        var mydate = DateFormat.getDateInstance().format(Calendar.getInstance().getTime());

        var mytime=DateFormat.getTimeInstance().format(Calendar.getInstance().getTime());



        val jsonArrayCall = retrofithelper.getClient().postMessages(
            PreferenceHelper(this).getData(
                Constants.tokenkey
            ), chatroom!!.chatroomid.toString(), msg, type.toString(), chatroom!!.user.userId,mydate,mytime
        )

        jsonArrayCall.enqueue(object : Callback<JsonObject> {
            override fun onResponse(
                call: Call<JsonObject>,
                response: Response<JsonObject>
            ) {


                if (response.body() != null) {

                    var json = response.body()


                    try {

                        var jsonObject = JSONObject(json.toString())

                        if (jsonObject.getInt("status") == 1) {


                            //  receiveMessagebyId


                            var msgid=jsonObject.getInt("messageid");

                            var chatMessage=ChatMessage()
                            chatMessage.chatMessageType=type.toString()
                            chatMessage.chatMessage=msg
                            chatMessage.chatRoomid=chatroom!!.chatroomid.toString()
                            chatMessage.chatReceiverid=chatroom!!.receiverid
                            chatMessage.chatSenderid=PreferenceHelper(this@ChatroomActivity).getData(Constants.userid)
                            chatMessage.chat_senddate=mydate
                            chatMessage.chat_sendtime=mytime
                            chatMessage.chatId=msgid.toString()



                            logs.add(chatMessage)

                          checkdate(logs)

                            val gson = Gson();

                            var listString = gson.toJson(
                                logs
                            );



                            writeDatatoFile(listString)

//                            if(chatroomAdapter!=null)
//                            {
                               chatroomAdapter!!.notifyDataSetChanged()

                            recyclermsg!!.smoothScrollToPosition(logs.size-1)
                            }







                        //}
                        else{

                            Toast.makeText(this@ChatroomActivity,"message sending failed",Toast.LENGTH_SHORT).show()
                        }


                    } catch (ex: Exception) {


                    }


                }
                else{

                    Toast.makeText(this@ChatroomActivity,"message sending failed",Toast.LENGTH_SHORT).show()
                }



            }

            override fun onFailure(call: Call<JsonObject>, t: Throwable) {

            }
        })


    }









    fun setupChatroom() {

        if (!chatroom!!.user.user_image.equals("") && chatroom!!.user.user_image != null) {


            Glide.with(this).load(Constants.profileimg + chatroom!!.user.user_image).apply(
                RequestOptions.circleCropTransform()
            ).into(imgUserprofile)

        }

        txtName.setText(chatroom!!.user.userName)

    }


    fun readjsonfile(): String {
        var responce = ""

        try {
            var s = this.externalCacheDir.toString() + "/" + chatroom!!.chatroomid

            var f = File(s)
            var fpath = f.absolutePath + "/" + chatroom!!.chatroomid + ".json"

            var fjson = File(fpath)


            var fileReader = FileReader(fjson);
            var bufferedReader = BufferedReader(fileReader);
            var stringBuilder = StringBuilder();
            var line = bufferedReader.readLine();
            while (line != null) {
                stringBuilder.append(line).append("\n");
                line = bufferedReader.readLine();
            }
            bufferedReader.close();// This responce will have Json Format String
            responce = stringBuilder.toString();


        } catch (ex: Exception) {

        }


        return responce;
    }


    fun writeDatatoFile(data: String) {
        try {
            var s = this.externalCacheDir.toString() + "/" + chatroom!!.chatroomid

            var f = File(s)
            var fpath = f.absolutePath + "/" + chatroom!!.chatroomid + ".json"

            var fjson = File(fpath)


            var fileWriter = FileWriter(fjson);
            var bufferedWriter = BufferedWriter(fileWriter);
            bufferedWriter.write(data);
            bufferedWriter.close();

        } catch (ex: Exception) {


        }
    }


    fun checkMessagesInFile(listfromfile: List<ChatMessage>, data: ChatMessage): Boolean {
        var a = false

        for (k in listfromfile) {



                if (data.chatId.equals(k.chatId)) {

                    a = true
                    break;

                }




        }





        return a
    }


}
