package com.advertise.quickjob.app.adapter;

import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;


import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.advertise.quickjob.app.R;
import com.advertise.quickjob.app.domain.JobDetails;
import com.advertise.quickjob.app.domain.SearchSuggestion;

import java.util.List;

public class SearchSuggestionsAdapter extends RecyclerView.Adapter<SearchSuggestionsAdapter.SearchSuggestionHolder> {



    Context context;
    List<SearchSuggestion>items;

    public SearchSuggestionsAdapter(Context context, List<SearchSuggestion> items) {
        this.context = context;
        this.items = items;
    }

    public class SearchSuggestionHolder extends RecyclerView.ViewHolder{

        TextView txtOfferdetails;
        ImageButton btnremove;

        LinearLayout layoutbtn;

        public SearchSuggestionHolder(@NonNull View itemView) {
            super(itemView);
            txtOfferdetails=itemView.findViewById(R.id.txtOfferdetails);



            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   // ((SearchSuggestionActivity)context).showProductDetails(getAdapterPosition(),items);
                }
            });


        }
    }


    @NonNull
    @Override
    public SearchSuggestionHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_searchsuggestion,viewGroup,false);



        return new SearchSuggestionHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SearchSuggestionHolder searchSuggestionHolder, int i) {

        searchSuggestionHolder.txtOfferdetails.setText(items.get(i).getJobTitle());



    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}
