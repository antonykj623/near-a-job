package com.advertise.quickjob.app.views

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.advertise.quickjob.app.R
import kotlinx.android.synthetic.main.activity_video_fullscreen.*
import android.R.attr.start

import androidx.core.app.ComponentActivity.ExtraData
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.net.Uri
import android.widget.MediaController
import com.advertise.quickjob.app.Utils.Constants


class VideoFullscreenActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_video_fullscreen)
        supportActionBar!!.hide()

        var videostring=intent.getStringExtra("file")



        val uri = Uri.parse(Constants.adsurl+videostring)
        videoViewRelative.setVideoURI(uri)

        videoViewRelative.setMediaController(MediaController(this))
        videoViewRelative.start()
    }
}
