package com.advertise.quickjob.app.views

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.advertise.quickjob.app.R
import com.advertise.quickjob.app.Utils.Constants
import com.advertise.quickjob.app.Utils.DownloadUtils
import com.advertise.quickjob.app.adapter.FavouriteJobsAdapter
import com.advertise.quickjob.app.domain.FavouriteJob

import com.advertise.quickjob.app.preferencehelper.PreferenceHelper
import com.advertise.quickjob.app.progress.ProgressFragment
import com.advertise.quickjob.app.webservicehelper.Retrofithelper

import kotlinx.android.synthetic.main.activity_applied_jobs.imgback
import kotlinx.android.synthetic.main.activity_applied_jobs.recyclerview
import kotlinx.android.synthetic.main.activity_favourites.*

import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception

class FavouritesActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_favourites)

        supportActionBar!!.hide()

        imgback.setOnClickListener {

            onBackPressed()
        }

        getAllFavourites()

    }

    fun getAllFavourites()
    {
        var p: ProgressFragment = ProgressFragment()
        p.show(supportFragmentManager,"dhjjd")
        val retrofithelper = Retrofithelper()

        var a: Call<List<FavouriteJob>> = retrofithelper.getClient().getAllFavourites(
            PreferenceHelper(this).getData(Constants.tokenkey),1)

        a.enqueue(object : Callback<List<FavouriteJob>> {
            override fun onResponse(call: Call<List<FavouriteJob>>, response: Response<List<FavouriteJob>>) {

                p.dismiss()

                if(response.body()!=null)
                {

                    var p=response.body();

                    try{


                        if(p!!.size>0)
                        {

                            var a=p as ArrayList<FavouriteJob>

                            recyclerfav.layoutManager=LinearLayoutManager(this@FavouritesActivity)

                            recyclerfav.adapter=FavouriteJobsAdapter(a,this@FavouritesActivity)

                        }









                    }catch (e: Exception)
                    {

                    }










                }






            }

            override fun onFailure(call: Call<List<FavouriteJob>>, t: Throwable) {

                p.dismiss()

                if(!DownloadUtils().isNetworkConnected(this@FavouritesActivity))
                {
                    DownloadUtils().showNoConnectiondialog(this@FavouritesActivity);
                }

                t.printStackTrace()



                //getProfileImage.php


            }
        })
    }
}
