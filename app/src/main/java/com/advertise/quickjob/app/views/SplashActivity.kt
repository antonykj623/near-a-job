package com.advertise.quickjob.app.views

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import androidx.core.content.ContextCompat
import com.advertise.quickjob.app.R
import com.advertise.quickjob.app.Utils.Constants
import com.advertise.quickjob.app.preferencehelper.PreferenceHelper
import kotlinx.android.synthetic.main.activity_splash.*

class SplashActivity : AppCompatActivity() {

    var handler:Handler?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        supportActionBar!!.hide()

        handler=Handler()

        handler!!.postDelayed(Runnable {

            if(!PreferenceHelper(this).getData(Constants.tokenkey).equals("")) {


                startActivity(Intent(this, MainActivity::class.java))
                finish()
            }
            else{

                startActivity(Intent(this, LoginActivity::class.java))
                finish()

            }

        },3000)

       // imgsplash.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.splashbackground))

    }
}
