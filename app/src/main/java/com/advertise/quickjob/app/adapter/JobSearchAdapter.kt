package com.advertise.quickjob.app.adapter

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Filter
import com.advertise.quickjob.app.domain.JobDetails

class JobSearchAdapter(context: Context,  resourceId:Int,  items:ArrayList<JobDetails>) :ArrayAdapter<JobDetails>(context,resourceId,items) {


    var cont=context
    var res=resourceId
    var item=items


    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        return super.getView(position, convertView, parent)
    }

    override fun getItem(position: Int): JobDetails? {
        return super.getItem(position)
    }

    override fun getFilter(): Filter {
        return super.getFilter()
    }

    override fun getItemId(position: Int): Long {
        return super.getItemId(position)
    }

    override fun getCount(): Int {
        return super.getCount()
    }
}