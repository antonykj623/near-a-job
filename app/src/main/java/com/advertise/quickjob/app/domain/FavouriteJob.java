package com.advertise.quickjob.app.domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FavouriteJob {
    @SerializedName("favourite_job_id")
    @Expose
    private String favouriteJobId;
    @SerializedName("favourite_job_userid")
    @Expose
    private String favouriteJobUserid;
    @SerializedName("favourite_job_jobid")
    @Expose
    private String favouriteJobJobid;
    @SerializedName("job_details")
    @Expose
    private JobDetails jobDetails;
    @SerializedName("job_user")
    @Expose
    private Jobuser jobUser;
    @SerializedName("isend")
    @Expose
    private Boolean isend;

    public FavouriteJob() {
    }

    public String getFavouriteJobId() {
        return favouriteJobId;
    }

    public void setFavouriteJobId(String favouriteJobId) {
        this.favouriteJobId = favouriteJobId;
    }

    public String getFavouriteJobUserid() {
        return favouriteJobUserid;
    }

    public void setFavouriteJobUserid(String favouriteJobUserid) {
        this.favouriteJobUserid = favouriteJobUserid;
    }

    public String getFavouriteJobJobid() {
        return favouriteJobJobid;
    }

    public void setFavouriteJobJobid(String favouriteJobJobid) {
        this.favouriteJobJobid = favouriteJobJobid;
    }

    public JobDetails getJobDetails() {
        return jobDetails;
    }

    public void setJobDetails(JobDetails jobDetails) {
        this.jobDetails = jobDetails;
    }

    public Jobuser getJobUser() {
        return jobUser;
    }

    public void setJobUser(Jobuser jobUser) {
        this.jobUser = jobUser;
    }

    public Boolean getIsend() {
        return isend;
    }

    public void setIsend(Boolean isend) {
        this.isend = isend;
    }
}
