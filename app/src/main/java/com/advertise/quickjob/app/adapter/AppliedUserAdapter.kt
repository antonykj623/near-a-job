package com.advertise.quickjob.app.adapter

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.advertise.quickjob.app.R
import com.advertise.quickjob.app.Utils.Constants
import com.advertise.quickjob.app.Utils.DownloadUtils
import com.advertise.quickjob.app.domain.ChatRoom
import com.advertise.quickjob.app.domain.JobApplieds
import com.advertise.quickjob.app.preferencehelper.PreferenceHelper
import com.advertise.quickjob.app.progress.ProgressFragment
import com.advertise.quickjob.app.views.AppliedCandidatesActivity
import com.advertise.quickjob.app.views.ChatroomActivity
import com.advertise.quickjob.app.webservicehelper.Retrofithelper
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.gson.JsonObject
import kotlinx.android.synthetic.main.activity_applied_candidates.*
import kotlinx.android.synthetic.main.layout_applieduseradapter.view.*
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception

class AppliedUserAdapter(context: Context,p:ArrayList<JobApplieds>):RecyclerView.Adapter<AppliedUserAdapter.AppliedUserHolder>() {


    var con=context

    var plist=p;


    class AppliedUserHolder(v: View) : RecyclerView.ViewHolder(v)
    {



    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AppliedUserHolder {

        var v= LayoutInflater.from(parent.context).inflate(R.layout.layout_applieduseradapter,parent,false)

        return AppliedUserHolder(v)

    }

    override fun getItemCount(): Int {

        return plist.size
    }

    override fun onBindViewHolder(holder: AppliedUserHolder, position: Int) {



        Glide.with(con).load(Constants.profileimg + plist.get(position).jobUser.user_image).apply(
            RequestOptions.circleCropTransform()
        ).into(holder.itemView.profile)

        holder.itemView.username.setText(plist.get(position).jobUser.userName)
        holder.itemView.txtQualification.setText(plist.get(position).jobUser.userPhone)

        if(plist.get(position).jobApprovedStatus.equals("0"))
        {

            holder.itemView.cbStatus.isChecked=false

            holder.itemView.cbStatus.setText("Click here to approve")
        }
        else{

            holder.itemView.cbStatus.isChecked=true

            holder.itemView.cbStatus.setText("Approved")

        }


        holder.itemView.cbStatus.setOnClickListener {

            if(  holder.itemView.cbStatus.isChecked)
            {

                changeStatus(plist.get(position),1,position)
            }
            else{
                changeStatus(plist.get(position),0,position)

              //  Toast.makeText(con,"Not Checked",Toast.LENGTH_SHORT).show()

            }

        }



        holder.itemView.imgchat.setOnClickListener {

            createChatroom(plist.get(position).jobUser.userId)
        }


        holder.itemView.layout_chat.setOnClickListener {

            createChatroom(plist.get(position).jobUser.userId)
        }


        holder.itemView.txtchat.setOnClickListener {

            createChatroom(plist.get(position).jobUser.userId)
        }








        holder.itemView.imgcall.setOnClickListener {


            val intent = Intent(Intent.ACTION_DIAL)
            intent.data = Uri.parse("tel:"+plist.get(position).jobUser.userPhone)
            con.startActivity(intent)

        }

        holder.itemView.txtCall.setOnClickListener {

            val intent = Intent(Intent.ACTION_DIAL)
            intent.data = Uri.parse("tel:"+plist.get(position).jobUser.userPhone)
           con. startActivity(intent)
        }

        holder.itemView.layout_call.setOnClickListener {

            val intent = Intent(Intent.ACTION_DIAL)
            intent.data = Uri.parse("tel:"+plist.get(position).jobUser.userPhone)
            con. startActivity(intent)

        }


    }



    fun changeStatus(p:JobApplieds,status:Int,position:Int)
    {
        var progressFragment=ProgressFragment()
        progressFragment.show((con as AppliedCandidatesActivity).supportFragmentManager,"")


        val retrofithelper = Retrofithelper()

        var a: Call<JsonObject> = retrofithelper.getClient().changeApprovalStatus(
            PreferenceHelper(con).getData(
                Constants.tokenkey),p.appliedJobId,p.jobAppliedUserid,status.toString())


        a.enqueue(object : Callback<JsonObject> {
            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {

                progressFragment.dismiss()

                if(response.body()!=null)
                {

                    var pa=response.body();

                    try{

                        if(pa!=null) {

                            try{

                                var json=JSONObject(pa.toString())

                                if(json.getInt("status")==1)
                                {


                                    if(status==1)
                                    {

                                        plist.get(position).jobApprovedStatus="1"
                                        Toast.makeText(con,"You approved the candidate "+p.jobUser.userName, Toast.LENGTH_SHORT).show()



                                    }
                                    else{
                                        plist.get(position).jobApprovedStatus="0"

                                        Toast.makeText(con,"You cancelled the approval of candidate "+p.jobUser.userName, Toast.LENGTH_SHORT).show()

                                    }

                                    notifyDataSetChanged()
                                }
                                else{



                                }








                            }catch (ex:Exception)
                            {

                            }


                        }




                    }catch (e: Exception)
                    {

                    }










                }






            }

            override fun onFailure(call: Call<JsonObject>, t: Throwable) {

                progressFragment.dismiss()

                if(!DownloadUtils().isNetworkConnected(con))
                {
                    DownloadUtils().showNoConnectiondialog(con);
                }

                t.printStackTrace()






            }
        })
    }




    fun createChatroom(jobuser:String)
    {



        var progressFragment=ProgressFragment()
        progressFragment.show((con as AppliedCandidatesActivity).supportFragmentManager,"")


        val retrofithelper = Retrofithelper()

        var a: Call<ChatRoom> = retrofithelper.getClient().createChatroom(
            PreferenceHelper(con).getData(
                Constants.tokenkey),jobuser)


        a.enqueue(object : Callback<ChatRoom> {
            override fun onResponse(call: Call<ChatRoom>, response: Response<ChatRoom>) {

                progressFragment.dismiss()

                if(response.body()!=null)
                {

                    var pa=response.body();

                    try{

                        if(pa!=null) {

                            try{


                                if(pa.status==1)
                                {

                                   val intent=Intent(con,ChatroomActivity::class.java)
                                    intent.putExtra("chatroom",pa)
                                    con.startActivity(intent)




                                }
                                else{


                                    Toast.makeText(con,"Error occured",Toast.LENGTH_SHORT).show()
                                }









                            }catch (ex:Exception)
                            {

                            }


                        }




                    }catch (e: Exception)
                    {

                    }










                }






            }

            override fun onFailure(call: Call<ChatRoom>, t: Throwable) {

                progressFragment.dismiss()

                if(!DownloadUtils().isNetworkConnected(con))
                {
                    DownloadUtils().showNoConnectiondialog(con);
                }

                t.printStackTrace()






            }
        })


    }



}