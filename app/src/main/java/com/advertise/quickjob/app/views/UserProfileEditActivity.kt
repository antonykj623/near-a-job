package com.advertise.quickjob.app.views

import android.app.DatePickerDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.DatePicker
import android.widget.NumberPicker
import android.widget.Toast
import com.advertise.quickjob.app.R
import com.advertise.quickjob.app.Utils.Constants
import com.advertise.quickjob.app.Utils.DownloadUtils
import com.advertise.quickjob.app.adapter.Qualificationadapter
import com.advertise.quickjob.app.domain.Qualification
import com.advertise.quickjob.app.domain.Qualificationdata
import com.advertise.quickjob.app.domain.Tokendata
import com.advertise.quickjob.app.domain.UserDetails
import com.advertise.quickjob.app.preferencehelper.PreferenceHelper
import com.advertise.quickjob.app.progress.ProgressFragment
import com.advertise.quickjob.app.webservicehelper.Retrofithelper
import com.google.gson.JsonObject




import kotlinx.android.synthetic.main.activity_user_profile_edit.btn_signup
import kotlinx.android.synthetic.main.activity_user_profile_edit.imgback
import kotlinx.android.synthetic.main.activity_user_profile_edit.imgdate
import kotlinx.android.synthetic.main.activity_user_profile_edit.input_address
import kotlinx.android.synthetic.main.activity_user_profile_edit.input_email
import kotlinx.android.synthetic.main.activity_user_profile_edit.input_name
import kotlinx.android.synthetic.main.activity_user_profile_edit.num1
import kotlinx.android.synthetic.main.activity_user_profile_edit.num2
import kotlinx.android.synthetic.main.activity_user_profile_edit.num3
import kotlinx.android.synthetic.main.activity_user_profile_edit.radiofemale
import kotlinx.android.synthetic.main.activity_user_profile_edit.radiomale
import kotlinx.android.synthetic.main.activity_user_profile_edit.radiothers
import kotlinx.android.synthetic.main.activity_user_profile_edit.spQualification
import kotlinx.android.synthetic.main.activity_user_profile_edit.txtDate
import org.json.JSONObject


import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception
import java.util.*
import kotlin.collections.ArrayList

class UserProfileEditActivity : AppCompatActivity() {

    var date:String =""


    var quali:String=""

    var gender:String=""

    var date_data:String=""
    var month_data:String=""
    var year_data:String=""

    internal var pickerVals =
        arrayOf("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec")


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_profile_edit)

        supportActionBar!!.hide()

        imgback.setOnClickListener {


            onBackPressed()
        }

        txtDate.setOnClickListener {
            showdatepicker()


        }

        imgdate.setOnClickListener {
            showdatepicker()

        }

        btn_signup.setOnClickListener {



            updateUser()


        }
        num1.maxValue = 31
        num1.minValue = 1


        num2.displayedValues = pickerVals

        num2.minValue = 0
        num2.maxValue = pickerVals.size - 1


        num3.maxValue = 2020
        num3.minValue = 1950




        showUserDetails()


        num1.setOnValueChangedListener(object : NumberPicker.OnValueChangeListener{

            override fun onValueChange(p0: NumberPicker?, p1: Int, p2: Int) {

                var a=     num1.value

                date_data=a.toString();

                //  Toast.makeText(this@RegistrationActivity,a.toString()+"",Toast.LENGTH_SHORT).show()

            }
        })


        num2.setOnValueChangedListener(object : NumberPicker.OnValueChangeListener{

            override fun onValueChange(p0: NumberPicker?, p1: Int, p2: Int) {

                var a=     num2.value

                var data=pickerVals[a]
                month_data=data

                //  Toast.makeText(this@RegistrationActivity,data.toString()+"",Toast.LENGTH_SHORT).show()


            }
        })


        num3.setOnValueChangedListener(object : NumberPicker.OnValueChangeListener{

            override fun onValueChange(p0: NumberPicker?, p1: Int, p2: Int) {

                var a=     num3.value

                year_data=a.toString()

                //   Toast.makeText(this@RegistrationActivity,a.toString()+"",Toast.LENGTH_SHORT).show()


            }
        })


    }


    fun updateUser()

    {

        if(!input_name!!.text.toString().equals("")){


            if(!input_address!!.text.toString().equals("")){


             //   if(!input_email!!.text.toString().equals("")){



                    if(radiofemale.isChecked)
                    {
                        gender="female"
                    }

                    if(radiomale.isChecked)
                    {
                        gender="male"
                    }

                    if(radiothers.isChecked)
                    {
                        gender="Other"
                    }

                date=date_data+"-"+month_data+"-"+year_data



                if(date!=null){

                                var w=spQualification!!.selectedItem as Qualification

                                quali=w!!.id

                                if(!quali.equals("0")){







                                            var p: ProgressFragment = ProgressFragment()
                                            p.show(supportFragmentManager,"dhjjd")
                                            val retrofithelper = Retrofithelper()

                                            var a: Call<JsonObject> = retrofithelper.getClient().updateUserProfile(PreferenceHelper(this).getData(Constants.tokenkey),input_name!!.text.toString(),
                                                input_email!!.text.toString(),
                                                gender,date!!,input_address!!.text.toString(),quali.toInt()
                                            )


                                            a.enqueue(object : Callback<JsonObject> {
                                                override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {

                                                    p.dismiss()

                                                    if(response.body()!=null)
                                                    {

                                                        var p=response.body();

                                                        var js=JSONObject(p.toString())

                                                        if(js.getInt("status")==1)
                                                        {


                                                            Toast.makeText(this@UserProfileEditActivity,"Profile details updated successfully",Toast.LENGTH_SHORT).show()


                                                        }
                                                        else{

                                                            Toast.makeText(this@UserProfileEditActivity,"Unknown error",Toast.LENGTH_SHORT).show()


                                                        }








                                                    }






                                                }

                                                override fun onFailure(call: Call<JsonObject>, t: Throwable) {

                                                    p.dismiss()

                                                    if(!DownloadUtils().isNetworkConnected(this@UserProfileEditActivity))
                                                    {
                                                        DownloadUtils().showNoConnectiondialog(this@UserProfileEditActivity);
                                                    }

                                                    t.printStackTrace()






                                                }
                                            })





















                                }
                                else{


                                    Toast.makeText(this,"Select your qualification",Toast.LENGTH_SHORT).show()

                                }







                            }
                            else{


                                Toast.makeText(this,"Select your date of birth",Toast.LENGTH_SHORT).show()

                            }









//                }
//                else{
//
//
//                    Toast.makeText(this,"Enter your email",Toast.LENGTH_SHORT).show()
//
//                }



            }
            else{


                Toast.makeText(this,"Enter your address",Toast.LENGTH_SHORT).show()

            }





        }
        else{


            Toast.makeText(this,"Enter your name",Toast.LENGTH_SHORT).show()

        }
















    }











    fun showUserDetails()
    {


        var p: ProgressFragment = ProgressFragment()
        p.show(supportFragmentManager,"dhjjd")
        val retrofithelper = Retrofithelper()

        var a: Call<UserDetails> = retrofithelper.getClient().getUserDetails(
            PreferenceHelper(this).getData(
                Constants.tokenkey))


        a.enqueue(object : Callback<UserDetails> {
            override fun onResponse(call: Call<UserDetails>, response: Response<UserDetails>) {

                p.dismiss()

                if(response.body()!=null)
                {

                    var p=response.body();

                    try{



                        if(p!!.status==1)
                        {



                            input_name!!.setText(p!!.user_name);
                            input_address!!.setText(p!!.userAddress);
                            input_email!!.setText(p!!.userEmail)
                            txtDate!!.setText(p!!.userDob)

                            setQualification(p!!.userQualification)

                            date=p!!.userDob

                            var arr=date.split("-")

                            date_data=arr[0]
                            month_data=arr[1]
                            year_data=arr[2]

                            num1.value=date_data.toInt()
                            num3.value=year_data.toInt()

                            for (j in 0 until  pickerVals.size)
                            {
                                if(month_data.equals(pickerVals[j]))
                                {
                                    num2.value=j
                                }


                            }




                            if(p!!.userMale.equals("male"))
                            {
                                radiomale.isChecked=true
                            }

                            if(p!!.userMale.equals("female"))
                            {
                                radiofemale.isChecked=true
                            }

                            if(p!!.userMale.equals("Other"))
                            {
                                radiothers.isChecked=true
                            }

                        }
                        else{






                        }





                    }catch (e: Exception)
                    {

                    }










                }






            }

            override fun onFailure(call: Call<UserDetails>, t: Throwable) {

                p.dismiss()

                if(!DownloadUtils().isNetworkConnected(this@UserProfileEditActivity))
                {
                    DownloadUtils().showNoConnectiondialog(this@UserProfileEditActivity);
                }

                t.printStackTrace()






            }
        })




    }



    fun showdatepicker()
    {


        val cal= Calendar.getInstance();

        val dateSetListener = object : DatePickerDialog.OnDateSetListener {
            override fun onDateSet(view: DatePicker, year: Int, monthOfYear: Int,
                                   dayOfMonth: Int) {
//                cal.set(Calendar.YEAR, year)
//                cal.set(Calendar.MONTH, monthOfYear)
//                cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
//                updateDateInView()

                date=""+dayOfMonth+"-"+monthOfYear+"-"+year

                txtDate.setText(date)



            }
        }



        DatePickerDialog(this,
            dateSetListener,
            // set DatePickerDialog to point to today's date when it loads up
            cal.get(Calendar.YEAR),
            cal.get(Calendar.MONTH),
            cal.get(Calendar.DAY_OF_MONTH)).show()
    }










    fun setQualification( qualification: String)
    {

        val retrofithelper = Retrofithelper()

        var a: Call<Qualificationdata> = retrofithelper.getClient().getQualifications()

        a.enqueue(object : Callback<Qualificationdata> {
            override fun onResponse(call: Call<Qualificationdata>, response: Response<Qualificationdata>) {


                if(response.body()!=null)
                {

                    var q=response.body();

                    if(q!!.status==1)
                    {

                        var qlist=q!!.data as ArrayList<Qualification>

                        var q=Qualification();
                        q.id="0";
                        q.name="Select qualification"

                        qlist.add(0,q)

                        spQualification.adapter=
                            Qualificationadapter(this@UserProfileEditActivity,qlist)

                        var i=0

                        for (Q in qlist) {


                            if(Q.id.equals(qualification))
                            {
                                spQualification.setSelection(i)

                            }

                            i++
                        }



                    }
                    else{

                        Toast.makeText(this@UserProfileEditActivity,"Unknown erro found", Toast.LENGTH_SHORT).show()
                    }





                }






            }

            override fun onFailure(call: Call<Qualificationdata>, t: Throwable) {


                if(!DownloadUtils().isNetworkConnected(this@UserProfileEditActivity))
                {
                    DownloadUtils().showNoConnectiondialog(this@UserProfileEditActivity);
                }

                t.printStackTrace()






            }
        })

    }
}
