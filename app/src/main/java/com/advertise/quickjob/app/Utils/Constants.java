package com.advertise.quickjob.app.Utils;

import android.content.Context;
import android.os.Environment;
import android.util.Log;


import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Constants {


    public static String url="http://www.zoomoutdoormedia.in/Nearajob/api/";

    public static String profileimg="http://www.zoomoutdoormedia.in/Nearajob/profileimg/";
    public static String adsurl="http://www.zoomoutdoormedia.in/Nearajob/api/ads/";

    public static String tokenkey="tokenkey";

    public static String userid="userid";

    public static String chatfilepath="http://www.zoomoutdoormedia.in/Nearajob/api/chatfiles/";

    public static String msgapikey="239b6e56-11cc-11eb-9fa5-0200cd936042";



 public static String getTempstorageFilePath(Context context) {

     Long tsLong = System.currentTimeMillis()/1000;
     String ts = tsLong.toString();

     String tempstorage = context.getExternalCacheDir()+"/"+ts+".png";

     return tempstorage;

 }


    public  static String getTempstorageFilePathForchat(Context context,String chatroomid) {

        Long tsLong = System.currentTimeMillis()/1000;
        String ts = tsLong.toString();

        String t = context.getExternalCacheDir()+"/"+chatroomid+"/sentfiles";

        File fol=new File(t);

        if(!fol.exists())
        {
            fol.mkdirs();
        }

        String tempstorage = t+"/"+ts+".png";

        return tempstorage;

    }

}
