package com.advertise.quickjob.app.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.advertise.quickjob.app.R
import com.advertise.quickjob.app.Utils.Constants
import com.advertise.quickjob.app.domain.ChatMessage
import com.advertise.quickjob.app.domain.ChatRoom
import com.advertise.quickjob.app.preferencehelper.PreferenceHelper
import com.advertise.quickjob.app.views.ChatroomActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.layout_chatholder.view.*
import java.io.BufferedReader
import java.io.File
import java.io.FileReader
import java.lang.Exception


class ChatAdapter(context: Context,li:List<ChatRoom>) :RecyclerView.Adapter<ChatAdapter.ChatHolder>(){

    var con=context
    var list=li



    class ChatHolder(v:View):RecyclerView.ViewHolder(v)
    {


    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChatHolder {

      //  layout_chatholder

        var v= LayoutInflater.from(parent.context).inflate(R.layout.layout_chatholder,parent,false)


        return ChatHolder(v)

    }

    override fun getItemCount(): Int {

              return list.size
    }

    override fun onBindViewHolder(holder: ChatHolder, position: Int) {

        var chatroom=list.get(position)

        holder.itemView.setOnClickListener {

            val intent=Intent(con,ChatroomActivity::class.java)
            intent.putExtra("chatroom",chatroom)


            con.startActivity(intent)
        }

        if (!chatroom!!.user.user_image.equals("") && chatroom!!.user.user_image != null) {


            Glide.with(con).load(Constants.profileimg + chatroom!!.user.user_image).apply(
                RequestOptions.circleCropTransform()
            ).into(holder.itemView.imgUserprofile)

        }

        holder.itemView.txtUsername.setText(chatroom!!.user.userName)

        showMessageFromLocalFolder(chatroom,holder)
    }



















    fun showMessageFromLocalFolder(chatroom: ChatRoom,holder: ChatHolder)
    {





                var chatMessage:ChatRoom=chatroom

        if(chatMessage.senderid!=null&&chatMessage.receiverid!=null) {

            if (PreferenceHelper(con).getData(Constants.userid).equals(chatMessage.senderid)) {

                if (chatMessage.messagetype.equals("2")) {
                    holder.itemView.txtmsg.setText("You : sent image")


                } else if (chatMessage.messagetype.equals("3")) {
                    holder.itemView.txtmsg.setText("You : sent location")

                } else {

                    holder.itemView.txtmsg.setText("You : " + chatMessage.message)
                }


            } else {

                //   holder.itemView.txtmsg.setText(+" : "+chatMessage.chatMessage)


                if (chatMessage.messagetype.equals("2")) {
                    holder.itemView.txtmsg.setText(" Received image")


                } else if (chatMessage.messagetype.equals("3")) {
                    holder.itemView.txtmsg.setText(" Received location")

                } else {

                    holder.itemView.txtmsg.setText(" " + chatMessage.message)
                }

            }


        }




        }









}