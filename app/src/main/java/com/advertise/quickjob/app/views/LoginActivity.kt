package com.advertise.quickjob.app.views

import android.app.Dialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.advertise.quickjob.app.R
import com.advertise.quickjob.app.Utils.Constants
import com.advertise.quickjob.app.Utils.DownloadUtils
import com.advertise.quickjob.app.domain.Tokendata
import com.advertise.quickjob.app.preferencehelper.PreferenceHelper
import com.advertise.quickjob.app.progress.ProgressFragment
import com.advertise.quickjob.app.webservicehelper.Retrofithelper
import com.google.gson.JsonObject
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_login.view.*;
import kotlinx.android.synthetic.main.activity_registration.*
import kotlinx.android.synthetic.main.layout_registerednumber.*
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        supportActionBar!!.hide()




        btn_forgotpassword.setOnClickListener {


            var d=Dialog(this)
            d.setContentView(R.layout.layout_registerednumber)

            var inpu_mob=d.findViewById<EditText>(R.id.inpu_mob)

            var btn_submit=d.findViewById<Button>(R.id.btn_submit)


            btn_submit.setOnClickListener {

                if(!inpu_mob.text.toString().equals(""))
                {

                    d.dismiss()

                    checkMobileNumber(inpu_mob.text.toString())

                }

                else{


                    Toast.makeText(this,"Enter mobile number",Toast.LENGTH_SHORT).show()
                }


            }





            d.show()


        }











        btnlogin.setOnClickListener {


            if(!edtPhone!!.text.toString().equals("")) {





                if(!edtPassword!!.text.toString().equals("")) {


var p:ProgressFragment= ProgressFragment()
                    p.show(supportFragmentManager,"dhjjd")


                    val retrofithelper = Retrofithelper()

                    var a: Call<Tokendata> = retrofithelper.getClient().UserLogin(edtPhone!!.text.toString(),
                        edtPassword!!.text.toString() )


                    a.enqueue(object : Callback<Tokendata> {
                        override fun onResponse(call: Call<Tokendata>, response: Response<Tokendata>) {

                            p.dismiss()

                            if(response.body()!=null)
                            {

                                var p=response.body();

                                if(p!!.status==1)
                                {

                                    PreferenceHelper(this@LoginActivity).putData(Constants.tokenkey,p!!.token)

                                    startActivity(Intent(this@LoginActivity,MainActivity::class.java))

                                    finish()



                                }
                                else{

                                    Toast.makeText(this@LoginActivity,"Login failed",Toast.LENGTH_SHORT).show()


                                }








                            }






                        }

                        override fun onFailure(call: Call<Tokendata>, t: Throwable) {

                            p.dismiss()

                            if(!DownloadUtils().isNetworkConnected(this@LoginActivity))
                            {
                                DownloadUtils().showNoConnectiondialog(this@LoginActivity);
                            }

                            t.printStackTrace()






                        }
                    })















                }
                else{

                    Toast.makeText(this,"Enter password",Toast.LENGTH_SHORT).
                        show()
                }

            }
            else{

                Toast.makeText(this,"Enter phone number",Toast.LENGTH_SHORT).
                        show()
            }
        }

        btn_create.setOnClickListener {

            startActivity(Intent(this,RegistrationActivity::class.java))

        }

      //  imgbackground.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.loginbackground))
    }



    fun checkMobileNumber(phone:String)
    {
        var p: ProgressFragment = ProgressFragment()
        p.show(supportFragmentManager,"dhjjd")
        val retrofithelper = Retrofithelper()

        var a: Call<JsonObject> = retrofithelper.getClient().checkMobileNumber(phone)


        a.enqueue(object : Callback<JsonObject> {
            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {

                p.dismiss()

                if(response.body()!=null)
                {

                    var p=response.body();

                    var js= JSONObject(p.toString());



                    if(js.getInt("status")==1)
                    {


                        //

                        val intent=Intent(this@LoginActivity,OTPActivity::class.java)

                        intent.putExtra("mob",phone.trim())

                        startActivityForResult(intent,111)

                    }
                    else{


                        // registerUser()


                        Toast.makeText(this@LoginActivity,"Mobile number does not exists",Toast.LENGTH_SHORT).show()




                    }








                }






            }

            override fun onFailure(call: Call<JsonObject>, t: Throwable) {

                p.dismiss()

                if(!DownloadUtils().isNetworkConnected(this@LoginActivity))
                {
                    DownloadUtils().showNoConnectiondialog(this@LoginActivity);
                }

                t.printStackTrace()






            }
        })

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode==111 &&data!=null )
        {

            var msg=data.getStringExtra("message");

            var mobile=data.getStringExtra("mobile")

            if(msg!=null)
            {
                if(msg.equals("OTP verified"))
                {

                    val intent=Intent(this@LoginActivity,ForgotPasswordActivity::class.java)

                    intent.putExtra("mob",mobile)



                    startActivity(intent)
                    //registerUser()

                }



            }



        }
    }
}
