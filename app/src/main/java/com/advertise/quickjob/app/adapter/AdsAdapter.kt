package com.advertise.quickjob.app.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.advertise.quickjob.app.R
import com.advertise.quickjob.app.Utils.Constants
import com.advertise.quickjob.app.domain.Ads
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.layout_adholder.view.*
import androidx.core.content.ContextCompat.startActivity
import android.content.Intent

import android.net.Uri
import com.advertise.quickjob.app.views.VideoFullscreenActivity


class AdsAdapter(con:Context,list: List<Ads>):RecyclerView.Adapter<AdsAdapter.Adholder>() {

    var li=list
    var c=con


    class Adholder(v:View):RecyclerView.ViewHolder(v)
    {

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Adholder {

        var v= LayoutInflater.from(parent.context).inflate(R.layout.layout_adholder,parent,false)


        return Adholder(v)

    }

    override fun getItemCount(): Int {

        if(li.size>4)
        {
            return 4
        }
        else {

            return li.size
        }

    }

    override fun onBindViewHolder(holder: Adholder, position: Int) {

        Glide.with(c).load(Constants.adsurl + li.get(position).adsUrl).into(holder.itemView.imgad)


       if(li.get(position).ad_link.equals(""))
       {
           holder.itemView.txtview.visibility=View.GONE
       }
        else{

           holder.itemView.txtview.visibility=View.VISIBLE
       }


        if(li.get(position).adsType.equals("0"))
        {
            holder.itemView.imgplaybton.visibility=View.GONE
        }
        else{

            holder.itemView.imgplaybton.visibility=View.VISIBLE
        }

        holder.itemView.txtAdinfo.setText(li.get(position).ad_description)

        holder.itemView.txtview.setOnClickListener {

            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(li.get(position).ad_link))
            c.startActivity(browserIntent)

        }

        holder.itemView.imgad.setOnClickListener {

            if(li.get(position).adsType.equals("1"))
            {
                //holder.itemView.imgplaybton.visibility=View.GONE


                val intent=Intent(c,VideoFullscreenActivity::class.java)

                intent.putExtra("file",li.get(position).adsUrl)
                c.startActivity(intent)


            }
            else{

                //holder.itemView.imgplaybton.visibility=View.VISIBLE
            }


        }


    }
}