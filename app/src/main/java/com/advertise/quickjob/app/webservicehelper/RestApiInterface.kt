package com.advertise.quickjob.app.webservicehelper

import com.advertise.quickjob.app.domain.*
import com.google.gson.JsonObject
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

interface RestApiInterface {


    @Multipart
    @POST("{api_key}/ADDON_SERVICES/SEND/TSMS")
    abstract fun sendOTP(
        @Path(
            value = "api_key",
            encoded = false
        ) api_key: String, @Part("From") requestBody: RequestBody, @Part("To") requestBody_to: RequestBody, @Part(
            "TemplateName"
        ) TemplateName: RequestBody, @Part("VAR1") VAR1: RequestBody, @Part("VAR2") VAR2: RequestBody
    ): Call<JsonObject>


    @Headers("Accept: application/json")
    @GET("getQualifications")
     fun getQualifications(): Call<Qualificationdata>



    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST("UserRegister")
    fun userRegister(@Field("name")name:String,@Field("mobile")mobile:String,@Field("email")email:String,
                     @Field("password")password:String,@Field("gender")gender:String,
                     @Field("dob")dob:String,@Field("address")address:String
                     ,@Field("qualification")qualification:Int): Call<Tokendata>




    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST("updateUserProfile")
    fun updateUserProfile(@Header("Token")token:String,@Field("name")name:String,@Field("email")email:String
                     ,@Field("gender")gender:String,
                     @Field("dob")dob:String,@Field("address")address:String
                     ,@Field("qualification")qualification:Int): Call<JsonObject>


    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST("changePassword")
    fun changePassword(@Field("phone")mobile:String,
                  @Field("password")password:String
    ): Call<JsonObject>





    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST("cancelAppliedJob")
    fun cancelAppliedJob(@Header("Token")token:String,
                       @Field("job_id")job_id:String
    ): Call<JsonObject>



    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST("UserLogin")
    fun UserLogin(@Field("mobile")mobile:String,
                     @Field("password")password:String
                    ): Call<Tokendata>



    @Headers("Accept: application/json")
    @Multipart
    @POST("uploadProfilePhoto")
    fun Uploadprofileimg(@Part  file: MultipartBody.Part, @Part("name") name: RequestBody,
                         @Header("Token")token:String
    ): Call<JsonObject>









    @Headers("Accept: application/json")
    @Multipart
    @POST("sendImagetoUser")
    fun sendImagetoUser(@Part  file: MultipartBody.Part, @Part("name") name: RequestBody,
                        @Header("Token")token:String, @Part("chatroomid") chatroomid:RequestBody, @Part("message")message:RequestBody,
                        @Part("type")type:RequestBody, @Part("receiverid")receiverid:RequestBody,@Part("chat_senddate")chat_senddate:RequestBody,@Part("chat_sendtime")chat_sendtime:RequestBody
    ): Call<JsonObject>











    @Headers("Accept: application/json")
    @GET("getProfileImage")
    fun Getprofileimg(@Header("Token")token:String
    ): Call<JsonObject>



    @Headers("Accept: application/json")
    @GET("getUserDetails")
    fun getUserDetails(@Header("Token")token:String
    ): Call<UserDetails>


    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST("UserDeleteAccount")
    fun UserDeleteAccount(@Header("Token")token:String,@Field("name")name:String,@Field("reason")mobile:String
    ): Call<JsonObject>




    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST("checkMobileNumber")
    fun checkMobileNumber(@Field("mobile")mobile:String
    ): Call<JsonObject>



    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST("checkGivenAppliedJob")
    fun checkGivenAppliedJob(@Header("Token")token:String,@Field("job_id")job_id:String
    ): Call<JsonObject>



    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST("checkJobUser")
    fun checkJobUser(@Header("Token")token:String,@Field("job_id")job_id:String
    ): Call<JsonObject>



    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST("createChatroom")
    fun createChatroom(@Header("Token")token:String,@Field("jobuserid")job_id:String
    ): Call<ChatRoom>



    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST("uploadJob")
    fun uploadJob(@Header("Token")token:String,@Field("name")name:String,@Field("description")description:String,@Field("salary_type")salary_type:String,
                  @Field("work_validity") work_validity:String,  @Field("minsalary") minsalary:String,  @Field("qualification")qualification:String,
                  @Field("maxsalary")maxsalary:String,@Field("address")address:String,
                  @Field("lat")lat:String,@Field("lon")lon:String,@Field("phonevisiblestatus")phonevisiblestatus:String
    ): Call<JsonObject>



    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST("editUploadedjob")
    fun editUploadedjob(@Header("Token")token:String,@Field("name")name:String,@Field("description")description:String,@Field("salary_type")salary_type:String,
                  @Field("work_validity") work_validity:String,  @Field("minsalary") minsalary:String,  @Field("qualification")qualification:String,
                  @Field("maxsalary")maxsalary:String,@Field("address")address:String,
                  @Field("lat")lat:String,@Field("lon")lon:String,@Field("phonevisiblestatus")phonevisiblestatus:String,@Field("job_id")job_id:String
    ): Call<JsonObject>




    @Headers("Accept: application/json")
    @GET("getAllJobsByUser")
    fun getAllJobs(@Header("Token")token:String): Call<List<JobDetails>>


    @Headers("Accept: application/json")
    @GET("getAppliedJobs")
    fun getAppliedJobs(@Header("Token")token:String): Call<List<AppliedJob>>


    @Headers("Accept: application/json")
    @GET("getJobDetail")
    fun getJobDetail(@Header("Token")token:String,@Query("job_id")page:Int): Call<JobDetails>

    @Headers("Accept: application/json")
    @GET("getSearchSuggestions")
    fun getSearchSuggestions(@Header("Token")token:String,@Query("word")word:String): Call<List<SearchSuggestion>>


    @Headers("Accept: application/json")
    @GET("getAllJobs")
    fun getAllJobsFrontpage(@Header("Token")token:String,@Query("page")page:Int,@Query("word")word:String,@Query("latitude")latitude:String,@Query("longitude")longitude:String): Call<List<JobDetails>>




    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST("updateFirebasetoken")
    fun updateFirebasetoken(@Header("Token")token:String,@Field("firebaseid")firebaseid:String
    ): Call<JsonObject>




    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST("postMessages")
    fun postMessages(@Header("Token")token:String,@Field("chatroomid")chatroomid:String,@Field("message")message:String,
                             @Field("type")type:String,@Field("receiverid")receiverid:String,@Field("send_datetime")send_date:String,@Field("send_time")send_time:String

    ): Call<JsonObject>




    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST("receiveChatMessages")
    fun receiveChatMessages(@Header("Token")token:String,@Field("chatroomid")chatroomid:String
    ): Call<List<ChatMessage>>



    @Headers("Accept: application/json")
    @GET("getUserId")
    fun getUserId(@Header("Token")token:String): Call<JsonObject>


    @Headers("Accept: application/json")
    @GET("getAllChatrooms")
    fun getAllChatrooms(@Header("Token")token:String): Call<List<ChatRoom>>



    @Headers("Accept: application/json")
    @GET("getAllads")
    fun getAllads(@Header("Token")token:String): Call<List<Ads>>



    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST("changeApprovalStatus")
    fun changeApprovalStatus(@Header("Token")token:String,@Field("job_id")job_id:String,@Field("job_applieduser")job_applieduser:String,
                             @Field("status")status:String
    ): Call<JsonObject>



    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST("checkFavourite")
    fun checkFavourite(@Header("Token")token:String,@Field("job_id")job_id:String

    ): Call<JsonObject>




    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST("getAllAppliedCandidates")
    fun getAllAppliedCandidates(@Header("Token")token:String,@Field("job_id")job_id:String): Call<List<JobApplieds>>




    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST("applyJob")
    fun applyJob(@Header("Token")token:String,@Field("job_id")job_id:String,@Field("job_employer_id")job_employer_id:String,
                 @Field("job_applied_userid")job_applied_userid:String, @Field("job_applied_date")job_applied_date:String
    ): Call<JsonObject>



    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST("addtoFavourites")
    fun addtoFavourites(@Header("Token")token:String,@Field("job_id")job_id:String
    ): Call<JsonObject>


    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST("removeFromFavourites")
    fun removeFromFavourites(@Header("Token")token:String,@Field("job_id")job_id:String
    ): Call<JsonObject>


    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST("removeUploadedJob")
    fun removeUploadedJob(@Header("Token")token:String,@Field("job_id")job_id:String
    ): Call<JsonObject>


    @Headers("Accept: application/json")
    @GET("getAllFavourites")
    fun getAllFavourites(@Header("Token")token:String,@Query("page")page:Int
    ): Call<List<FavouriteJob>>
}