package com.advertise.quickjob.app.views

import android.Manifest
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.location.Geocoder
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.annotation.NonNull
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.advertise.quickjob.app.R
import com.advertise.quickjob.app.Utils.Constants
import com.advertise.quickjob.app.Utils.DownloadUtils
import com.advertise.quickjob.app.adapter.Qualificationadapter
import com.advertise.quickjob.app.domain.Qualification
import com.advertise.quickjob.app.domain.Qualificationdata
import com.advertise.quickjob.app.preferencehelper.PreferenceHelper
import com.advertise.quickjob.app.progress.ProgressFragment
import com.advertise.quickjob.app.webservicehelper.Retrofithelper
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.gson.JsonObject
import kotlinx.android.synthetic.main.activity_registration.*
import kotlinx.android.synthetic.main.activity_upload_job.*
import kotlinx.android.synthetic.main.activity_upload_job.imgback
import kotlinx.android.synthetic.main.activity_upload_job.input_address
import kotlinx.android.synthetic.main.activity_upload_job.input_name
import kotlinx.android.synthetic.main.activity_upload_job.spQualification
import org.florescu.android.rangeseekbar.RangeSeekBar
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class UploadJobActivity : AppCompatActivity(), LocationListener {

    var progressFragment:ProgressFragment?=null

    var locationManager:LocationManager?=null;

    var location:Location?=null

    var lat=0.0;
    var longi=0.0

    var qid="0"

    val maprqcode=111

    var result:String?="";

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_upload_job)
        supportActionBar!!.hide()
        imgback.setOnClickListener {

            onBackPressed()
        }

        imgpickloc.setOnClickListener {


            //checkPermission()

            startActivityForResult(Intent(this,ChooseLocationActivity::class.java),maprqcode)

        }

        txtLocation.setOnClickListener {
           // checkPermission()

            startActivityForResult(Intent(this,ChooseLocationActivity::class.java),maprqcode)


        }









        btn_upload.setOnClickListener {


            if(!input_name.text.toString().equals(""))
            {
                if(!input_description.text.toString().equals(""))
                {

                    var w=spQualification!!.selectedItem as Qualification

                    qid=w!!.id

                    if(!qid.equals("0"))
                    {

                        var v=spSalary!!.selectedItem as String

                        if(!v.equals("Select salary type"))
                        {

                           var wd= spWorkingdays!!.selectedItem as String
                            if(!wd.equals("Select job validity"))
                            {


                    if(!input_address.text.toString().equals(""))
                    {

                        var cd_phone_status=0;

                        if(cb_phone.isChecked)
                        {
                            cd_phone_status=1;
                        }
                        else{

                            cd_phone_status=0;
                        }

                        var p: ProgressFragment = ProgressFragment()
                        p.show(supportFragmentManager,"dhjjd")


                        val retrofithelper = Retrofithelper()

                        var a: Call<JsonObject> = retrofithelper.getClient().uploadJob(
                            PreferenceHelper(this).getData(Constants.tokenkey),input_name.text.toString(),
                            input_description.text.toString(),v,wd,input_minimumsalary.text.toString(),qid,
                            input_maximum.text.toString(),input_address.text.toString(),lat.toString(),longi.toString(),cd_phone_status.toString())



                        a.enqueue(object : Callback<JsonObject> {
                            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {


                                if(response.body()!=null)
                                {

                                    p.dismiss()

                                    var p=response.body();

                                    var js= JSONObject(p.toString());



                                    if(js.getInt("status")==1)
                                    {

                                        input_name.setText("");
                                        input_description.setText("");

                                        input_address.setText("");
                                   spWorkingdays.setSelection(0)
                                        spQualification.setSelection(0)

                                        spSalary.setSelection(0)
                                        Toast.makeText(this@UploadJobActivity,js.getString("message"),Toast.LENGTH_SHORT).show()



                                    }
                                    else{

                                        Toast.makeText(this@UploadJobActivity,"Failed",Toast.LENGTH_SHORT).show()


                                    }





                                }






                            }

                            override fun onFailure(call: Call<JsonObject>, t: Throwable) {

                                p.dismiss()
                                if(!DownloadUtils().isNetworkConnected(this@UploadJobActivity))
                                {
                                    DownloadUtils().showNoConnectiondialog(this@UploadJobActivity);
                                }

                                t.printStackTrace()






                            }
                        })





                    }
                    else{



                        Toast.makeText(this,"Enter your address",Toast.LENGTH_SHORT).show()
                    }


                            }
                            else{



                                Toast.makeText(this,"Select job validity",Toast.LENGTH_SHORT).show()
                            }

                        }
                        else{



                            Toast.makeText(this,"Select salary type",Toast.LENGTH_SHORT).show()
                        }


                    }
                    else{



                        Toast.makeText(this,"Select qualification",Toast.LENGTH_SHORT).show()
                    }

                }
                else{



                    Toast.makeText(this,"Enter job description",Toast.LENGTH_SHORT).show()
                }



            }
            else{



                Toast.makeText(this,"Enter job title",Toast.LENGTH_SHORT).show()
            }



        }


        cb_phone.setOnClickListener {


            if(cb_phone.isChecked)
            {


                val builder = AlertDialog.Builder(this)
                builder.setTitle("Near a job")
                builder.setMessage("Other user cannot contact to your phone number.")
                builder.setNegativeButton(
                    "ok"
                ) {


                        dialog, which -> dialog.dismiss()


                }



                val alertDialog = builder.create()
                alertDialog.show()

            }
            else{

                val builder = AlertDialog.Builder(this)
                builder.setTitle("Near a job")
                builder.setMessage("Other user can contact your phone number.")
                builder.setNegativeButton(
                    "ok"
                ) {


                        dialog, which -> dialog.dismiss()


                }



                val alertDialog = builder.create()
                alertDialog.show()

            }




        }

        getQualifications()
    }





    fun checkPermission() {

        //        if(CheckGpsStatus()) {


        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) !== PackageManager.PERMISSION_GRANTED
        ) {


            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                119
            )

        } else {


            if (!CheckGpsStatus()) {
                calllocation()
            } else {


                try {
                    progressFragment = ProgressFragment()
                    progressFragment!!.setCancelable(false)
                    progressFragment!!.show(supportFragmentManager, "dfgdf")
                    locationManager =
                        this@UploadJobActivity.getSystemService(Context.LOCATION_SERVICE) as LocationManager

                    location =
                        locationManager!!.getLastKnownLocation(LocationManager.NETWORK_PROVIDER)
                    locationManager!!.requestLocationUpdates(
                        LocationManager.NETWORK_PROVIDER,
                        12000,
                        7f,
                        this@UploadJobActivity
                    )
                } catch (e: SecurityException) {

                }


            }




        }



    }


    fun calllocation() {

        val mLocationRequest = LocationRequest.create()
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
            .setInterval(10 * 1000)
            .setFastestInterval(1 * 1000)


        val settingsBuilder = LocationSettingsRequest.Builder()
            .addLocationRequest(mLocationRequest)
        settingsBuilder.setAlwaysShow(true)


        val result = LocationServices.getSettingsClient(this)
            .checkLocationSettings(settingsBuilder.build())



        result.addOnCompleteListener(object : OnCompleteListener<LocationSettingsResponse> {

            override fun onComplete(@NonNull task: Task<LocationSettingsResponse>) {

                try {
                    val response = task.getResult(ApiException::class.java)
                } catch (ex: ApiException) {
                    when (ex.getStatusCode()) {
                        LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> try {
                            val resolvableApiException = ex as ResolvableApiException
                            resolvableApiException
                                .startResolutionForResult(
                                    this@UploadJobActivity,
                                    11
                                )
                        } catch (e: IntentSender.SendIntentException) {

                        }

                        LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                        }
                    }
                }

            }


        })


    }







    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == maprqcode && resultCode==255&&data!=null) {

            lat=data.getDoubleExtra("lat",0.0)
            longi=data.getDoubleExtra("longitude",0.0)
            var location=data.getStringExtra("locationName")

            result = location

            txtLocation.setText(location)



//            try {
//                progressFragment = ProgressFragment()
//                progressFragment!!.setCancelable(false)
//                progressFragment!!.show(supportFragmentManager, "dfgdf")
//                locationManager =
//                    this@UploadJobActivity.getSystemService(Context.LOCATION_SERVICE) as LocationManager
//
//                location = locationManager!!.getLastKnownLocation(LocationManager.NETWORK_PROVIDER)
//                locationManager!!.requestLocationUpdates(
//                    LocationManager.NETWORK_PROVIDER,
//                    12000,
//                    7f,
//                    this@UploadJobActivity
//                )
//            } catch (e: SecurityException) {
//
//            }

        }
    }


    override fun onLocationChanged(p0: Location?) {



//        if (location != null) {
//
//            lat = p0!!.getLatitude()
//            longi = p0!!.getLongitude()
//
//
//            val geocoder = Geocoder(this, Locale.getDefault())
//
//            try {
//                val addressList = geocoder.getFromLocation(
//                    lat, longi, 1
//                )
//                if (addressList != null && addressList.size > 0) {
//                    val address = addressList[0]
//
//
//                    if (address != null) {
//                        val sb = StringBuilder()
//                        for (i in 0 until address.maxAddressLineIndex) {
//
//                            if (address.getAddressLine(i) != null && address.getAddressLine(i) != "") {
//
//                                sb.append(address.getAddressLine(i)).append(",")
//                            }
//                        }
//
//                        if (address.locality != null) {
//                            sb.append(address.locality).append(",")
//                        }
//
//                        if (address.countryName != null) {
//                            sb.append(address.countryName).append(",")
//                        }
//                        //sb.append(address.getCountryName());
//                        result = sb.toString()
//
//                        txtLocation.setText(result)
//
//
//                    }
//                } else {
//
//
//                }
//            } catch (e: Exception) {
//
//            }
//
//        } else {
//
//            Toast.makeText(this, "Cannot fetch location", Toast.LENGTH_LONG)
//                .show()
//
//        }
//
//        if (progressFragment != null) {
//
//            if (progressFragment!!.isVisible()) {
//                progressFragment!!.dismiss()
//            }
//
//        }


    }

    override fun onStatusChanged(p0: String?, p1: Int, p2: Bundle?) {

    }

    override fun onProviderEnabled(p0: String?) {

    }

    override fun onProviderDisabled(p0: String?) {

    }

    fun CheckGpsStatus(): Boolean {

        locationManager =this.getSystemService(Context.LOCATION_SERVICE) as LocationManager

        return locationManager!!.isProviderEnabled(LocationManager.GPS_PROVIDER)

    }


    override fun onRequestPermissionsResult(requestCode: Int, @NonNull permissions: Array<String>, @NonNull grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) === PackageManager.PERMISSION_GRANTED
        ) {


            checkPermission()
        }

    }


    fun getQualifications()
    {

        val retrofithelper = Retrofithelper()

        var a: Call<Qualificationdata> = retrofithelper.getClient().getQualifications()

        a.enqueue(object : Callback<Qualificationdata> {
            override fun onResponse(call: Call<Qualificationdata>, response: Response<Qualificationdata>) {


                if(response.body()!=null)
                {

                    var q=response.body();

                    if(q!!.status==1)
                    {

                        var qlist=q!!.data as ArrayList<Qualification>

                        var q= Qualification();
                        q.id="0";
                        q.name="Select a required qualification"

                        qlist.add(0,q)

                        spQualification.adapter=
                            Qualificationadapter(this@UploadJobActivity,qlist)





                    }
                    else{

                        Toast.makeText(this@UploadJobActivity,"Unknown erro found",Toast.LENGTH_SHORT).show()
                    }





                }






            }

            override fun onFailure(call: Call<Qualificationdata>, t: Throwable) {


                if(!DownloadUtils().isNetworkConnected(this@UploadJobActivity))
                {
                    DownloadUtils().showNoConnectiondialog(this@UploadJobActivity);
                }

                t.printStackTrace()






            }
        })




    }


}
