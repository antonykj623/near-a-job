package com.advertise.quickjob.app.views

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.advertise.quickjob.app.R
import com.advertise.quickjob.app.Utils.Constants
import com.advertise.quickjob.app.Utils.DownloadUtils
import com.advertise.quickjob.app.adapter.AppliedJobsAdapter
import com.advertise.quickjob.app.adapter.AppliedUserAdapter
import com.advertise.quickjob.app.domain.AppliedJob
import com.advertise.quickjob.app.domain.JobApplieds
import com.advertise.quickjob.app.preferencehelper.PreferenceHelper
import com.advertise.quickjob.app.webservicehelper.Retrofithelper
import com.google.gson.JsonObject
import kotlinx.android.synthetic.main.activity_applied_candidates.*
import kotlinx.android.synthetic.main.activity_applied_jobs.*
import kotlinx.android.synthetic.main.activity_applied_jobs.imgback
import kotlinx.android.synthetic.main.activity_applied_jobs.recyclerview
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception

class AppliedJobsActivity : AppCompatActivity() {

    var appliedJobsAdapter:AppliedJobsAdapter?=null;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_applied_jobs)
        supportActionBar!!.hide()

        imgback.setOnClickListener {

            onBackPressed()
        }

        getAllApliedJobs()

    }

    fun getAllApliedJobs()
    {

        val retrofithelper = Retrofithelper()

        var a: Call<List<AppliedJob>> = retrofithelper.getClient().getAppliedJobs(
            PreferenceHelper(this).getData(
                Constants.tokenkey))


        a.enqueue(object : Callback<List<AppliedJob>> {
            override fun onResponse(call: Call<List<AppliedJob>>, response: Response<List<AppliedJob>>) {



                if(response.body()!=null)
                {

                    var p=response.body();

                    try{

                        if(p!=null) {

                            if(p.size>0) {


                                var a=p as ArrayList<AppliedJob>

                                appliedJobsAdapter= AppliedJobsAdapter(this@AppliedJobsActivity,a)

                                recyclerview.layoutManager= LinearLayoutManager(this@AppliedJobsActivity)
                                recyclerview.adapter=appliedJobsAdapter




                            }
                        }




                    }catch (e: Exception)
                    {

                    }










                }






            }

            override fun onFailure(call: Call<List<AppliedJob>>, t: Throwable) {



                if(!DownloadUtils().isNetworkConnected(this@AppliedJobsActivity))
                {
                    DownloadUtils().showNoConnectiondialog(this@AppliedJobsActivity);
                }

                t.printStackTrace()






            }
        })
    }



}
