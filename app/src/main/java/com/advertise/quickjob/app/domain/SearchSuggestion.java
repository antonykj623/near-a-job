package com.advertise.quickjob.app.domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SearchSuggestion {

    @SerializedName("job_id")
    @Expose
    private String jobId;
    @SerializedName("job_title")
    @Expose
    private String jobTitle;

    public SearchSuggestion() {
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }
}
