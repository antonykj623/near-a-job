package com.advertise.quickjob.app.adapter

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Handler
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatImageView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.advertise.quickjob.app.R
import com.advertise.quickjob.app.Utils.Constants
import com.advertise.quickjob.app.Utils.DownloadUtils
import com.advertise.quickjob.app.domain.JobDetails
import com.advertise.quickjob.app.preferencehelper.PreferenceHelper
import com.advertise.quickjob.app.progress.ProgressFragment
import com.advertise.quickjob.app.views.JobdetailsActivity
import com.advertise.quickjob.app.views.MainActivity
import com.advertise.quickjob.app.webservicehelper.Retrofithelper
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.material.tabs.TabLayout
import com.google.gson.JsonObject
import kotlinx.android.synthetic.main.fragment_account.view.*
import kotlinx.android.synthetic.main.layout_joblist.view.*
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception
import java.util.*
import kotlin.collections.ArrayList

class JobListAdapter(context: Context,list: ArrayList<JobDetails>) : RecyclerView.Adapter<JobListAdapter.JoblistHolder>() {


    var con=context;
    var li=list

    var currentPage:Int=0


    class JoblistHolder(v:View) : RecyclerView.ViewHolder(v)
    {
        var title=v.findViewById<TextView>(R.id.title)

        var txtwishlist=v.findViewById<TextView>(R.id.txtwishlist)

        var txtuploadDate=v.findViewById<TextView>(R.id.txtuploadDate)

        var txtSalaryinfo=v.findViewById<TextView>(R.id.txtSalaryinfo)

        var imgjobuser=v.findViewById<AppCompatImageView>(R.id.imgjobuser)

        var cardWishlist=v.findViewById<CardView>(R.id.cardWishlist)

        var viewpager=v.findViewById<ViewPager>(R.id.viewpager)
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): JoblistHolder {


        var v=LayoutInflater.from(parent.context).inflate(R.layout.layout_joblist,parent,false)

        return JoblistHolder(v)
    }

    override fun getItemCount(): Int {


        return li.size


    }

    override fun onBindViewHolder(holder: JoblistHolder, position: Int) {

//        if(position==0)
//        {
//            holder.itemView.cardvideo.visibility=View.VISIBLE
//
//            for (j in 1..3) {
//
//
//
//                holder.itemView.tab_layout.addTab( holder.itemView.tab_layout.newTab())
//                holder.viewpager.id=j
//            }
//
//            holder.viewpager.offscreenPageLimit=3
//
//            val displayMetrics = DisplayMetrics()
//            (con as Activity).windowManager.defaultDisplay.getMetrics(displayMetrics)
//            val height = displayMetrics.heightPixels
//            val width = displayMetrics.widthPixels
//            val w = width / 1.7
//
//            holder.viewpager.setLayoutParams(FrameLayout.LayoutParams(width, w.toInt()))
//
//            holder.viewpager.setAdapter(
//                AdpagerAdapter(
//                    (con as AppCompatActivity).getSupportFragmentManager()
//
//                )
//            )
//            holder.itemView.tab_layout.setupWithViewPager(holder.viewpager)
//
//          //  holder.itemView.viewpager.setPageTransformer(true, ZoomOutPageTransformer())
//
//            holder.itemView.tab_layout.addOnTabSelectedListener(object :
//                TabLayout.OnTabSelectedListener {
//              override  fun onTabSelected(tab: TabLayout.Tab) {
//                  holder.viewpager.setCurrentItem(tab.getPosition())
//                }
//
//                override  fun onTabUnselected(tab: TabLayout.Tab) {
//
//                }
//
//                override  fun onTabReselected(tab: TabLayout.Tab) {
//
//                }
//            })
//
//
//             var handler =  Handler();
//    var Update =  Runnable() {
//
//        if (currentPage == 3) {
//            currentPage = 0;
//        }
//        holder.viewpager.setCurrentItem(currentPage++, true);
//
//     };
//
//            val timer = Timer()
//            val hourlyTask = object : TimerTask() {
//                override  fun run() {
//
//
//                    //showLatestOrders()
//
//                    handler.post(Update)
//
//
//                }
//            }
//
//
//            timer.schedule (hourlyTask, 5000, 6000);
//
//
//        }
//        else{
//
//            holder.itemView.cardvideo.visibility=View.GONE
//
//        }
//
//
//
//        holder.cardWishlist.setOnClickListener {
//
//
//            var intent=Intent(con,JobdetailsActivity::class.java)
//            intent.putExtra("jobdetails",li.get(position))
//
//            con.startActivity(intent)
//
//
//
//
//        }
//
//
//
//
//        if(li.get(position).isFavourite)
//        {
//            holder.itemView.imgfavourite.setImageResource(R.drawable.ic_favorite)
//        }
//        else{
//
//            holder.itemView.imgfavourite.setImageResource(R.drawable.ic_favorite_border_black)
//        }
//
//
//
//
//        holder.itemView.imgfavourite.setOnClickListener {
//
//
//            if(li.get(position).isFavourite)
//            {
//                removeFromFavourites(li.get(position).job_id.toInt(), position)
//            }
//            else {
//
//                addtoFavourites(li.get(position).job_id.toInt(), position)
//            }
//
//        }
//
//
//
//
//
//        holder.title.setText(li.get(position).jobTitle)
//
//        holder.txtwishlist.setText(li.get(position).jobDesccription)
//
//        holder.txtuploadDate.setText(li.get(position).jobUploadedDate)
//
//        holder.txtSalaryinfo.setText(li.get(position).jobSalaryInfo)
//
//
//        Glide.with(con).load(Constants.profileimg + li.get(position).jobUser.user_image).apply(
//            RequestOptions.circleCropTransform()
//        ).into(holder.imgjobuser)
//
//
//            }

    }

    fun addtoFavourites(jobid:Int,position: Int)
    {
        var p: ProgressFragment = ProgressFragment()
        p.show((con  as MainActivity).supportFragmentManager,"dhjjd")
        val retrofithelper = Retrofithelper()

        var a: Call<JsonObject> = retrofithelper.getClient().addtoFavourites(
            PreferenceHelper(con).getData(Constants.tokenkey),jobid.toString())


        a.enqueue(object : Callback<JsonObject> {
            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {

                p.dismiss()

                if(response.body()!=null)
                {

                    var p=response.body();

                    try{



                        val json= JSONObject(p.toString())

                        if(json.getInt("status")==1)
                        {


                            li.get(position).isFavourite=true

                            Toast.makeText(con,json.getString("message"), Toast.LENGTH_SHORT).show()

                            notifyItemChanged(position)

                        }
                        else{




                            Toast.makeText(con,json.getString("message"), Toast.LENGTH_SHORT).show()
                        }







                    }catch (e: Exception)
                    {

                    }










                }






            }

            override fun onFailure(call: Call<JsonObject>, t: Throwable) {

                p.dismiss()

                if(!DownloadUtils().isNetworkConnected(con))
                {
                    DownloadUtils().showNoConnectiondialog(con);
                }

                t.printStackTrace()



                //getProfileImage.php


            }
        })
    }





    fun removeFromFavourites(jobid:Int,position: Int)
    {
        var p: ProgressFragment = ProgressFragment()
        p.show((con  as MainActivity).supportFragmentManager,"dhjjd")
        val retrofithelper = Retrofithelper()

        var a: Call<JsonObject> = retrofithelper.getClient().removeFromFavourites(
            PreferenceHelper(con).getData(Constants.tokenkey),jobid.toString())


        a.enqueue(object : Callback<JsonObject> {
            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {

                p.dismiss()

                if(response.body()!=null)
                {

                    var p=response.body();

                    try{



                        val json= JSONObject(p.toString())

                        if(json.getInt("status")==1)
                        {


                            li.get(position).isFavourite=false

                            Toast.makeText(con,json.getString("message"), Toast.LENGTH_SHORT).show()

                            notifyItemChanged(position)

                        }
                        else{




                            Toast.makeText(con,json.getString("message"), Toast.LENGTH_SHORT).show()
                        }







                    }catch (e: Exception)
                    {

                    }










                }






            }

            override fun onFailure(call: Call<JsonObject>, t: Throwable) {

                p.dismiss()

                if(!DownloadUtils().isNetworkConnected(con))
                {
                    DownloadUtils().showNoConnectiondialog(con);
                }

                t.printStackTrace()



                //getProfileImage.php


            }
        })
    }
}