package com.advertise.quickjob.app.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.advertise.quickjob.app.R
import com.advertise.quickjob.app.domain.Qualification

class Qualificationadapter(context:Context,q:ArrayList<Qualification>):BaseAdapter() {


    var con=context
    var qu=q;

    override fun getView(p0: Int, p1: View?, p2: ViewGroup?): View {


        var v = LayoutInflater.from(con).
            inflate(R.layout.layout_txt, p2, false);


var txt=v.findViewById<TextView>(R.id.txt);

        txt.setText(qu.get(p0).name)

        return v

    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var v = LayoutInflater.from(con).
            inflate(R.layout.layout_txt, parent, false);


        var txt=v.findViewById<TextView>(R.id.txt);

        txt.setText(qu.get(position).name)

        return v
    }

    override fun getItem(p0: Int): Any {

        return qu.get(p0)


           }

    override fun getItemId(p0: Int): Long {

        return p0.toLong()
          }

    override fun getCount(): Int {

       return qu.size
           }
}