package com.advertise.quickjob.app.domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ChatMessage implements Serializable {


    @SerializedName("chat_id")
    @Expose
    private String chatId;
    @SerializedName("chat_message_type")
    @Expose
    private String chatMessageType;
    @SerializedName("chat_message")
    @Expose
    private String chatMessage;
    @SerializedName("chat_senderid")
    @Expose
    private String chatSenderid;
    @SerializedName("chat_receiverid")
    @Expose
    private String chatReceiverid;
    @SerializedName("chat_roomid")
    @Expose
    private String chatRoomid;
    @SerializedName("chat_messages_noted")
    @Expose
    private String chatMessagesNoted;
    @SerializedName("chat_senddate")
    @Expose
    private String chat_senddate;
    @SerializedName("chat_sendtime")
    @Expose
    private String chat_sendtime;

    public ChatMessage() {
    }

    public String getChat_sendtime() {
        return chat_sendtime;
    }

    public void setChat_sendtime(String chat_sendtime) {
        this.chat_sendtime = chat_sendtime;
    }

    public String getChat_senddate() {
        return chat_senddate;
    }

    public void setChat_senddate(String chat_senddate) {
        this.chat_senddate = chat_senddate;
    }

    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    public String getChatMessageType() {
        return chatMessageType;
    }

    public void setChatMessageType(String chatMessageType) {
        this.chatMessageType = chatMessageType;
    }

    public String getChatMessage() {
        return chatMessage;
    }

    public void setChatMessage(String chatMessage) {
        this.chatMessage = chatMessage;
    }

    public String getChatSenderid() {
        return chatSenderid;
    }

    public void setChatSenderid(String chatSenderid) {
        this.chatSenderid = chatSenderid;
    }

    public String getChatReceiverid() {
        return chatReceiverid;
    }

    public void setChatReceiverid(String chatReceiverid) {
        this.chatReceiverid = chatReceiverid;
    }

    public String getChatRoomid() {
        return chatRoomid;
    }

    public void setChatRoomid(String chatRoomid) {
        this.chatRoomid = chatRoomid;
    }

    public String getChatMessagesNoted() {
        return chatMessagesNoted;
    }

    public void setChatMessagesNoted(String chatMessagesNoted) {
        this.chatMessagesNoted = chatMessagesNoted;
    }
}
