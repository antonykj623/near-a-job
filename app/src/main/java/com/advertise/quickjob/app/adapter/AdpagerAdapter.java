package com.advertise.quickjob.app.adapter;

import android.os.Bundle;


import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.advertise.quickjob.app.fragments.AdFragment;

import java.util.List;

public class AdpagerAdapter extends FragmentPagerAdapter {


    List<String>path;

    public AdpagerAdapter(FragmentManager fm) {
        super(fm);

    }

    @Override
    public Fragment getItem(int i) {
        Fragment fr=new AdFragment();
        Bundle bundle=new Bundle();
        bundle.putSerializable("pos",String.valueOf(i));
        fr.setArguments(bundle);

        return fr;
    }

    @Override
    public int getCount() {
        return 3;
    }
}
