package com.advertise.quickjob.app.backgroundservices

import android.app.IntentService
import android.content.Intent
import android.util.Log
import com.advertise.quickjob.app.Utils.Constants
import com.advertise.quickjob.app.domain.ChatHistoryBundle
import com.advertise.quickjob.app.domain.ChatMessage
import com.advertise.quickjob.app.preferencehelper.PreferenceHelper
import com.advertise.quickjob.app.webservicehelper.Retrofithelper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class IncomingChatService : IntentService("IncomingChatService") {


    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {




        return super.onStartCommand(intent, flags, startId)
    }




    override fun onHandleIntent(p0: Intent?) {


        var chatroom=p0!!.getIntExtra("chatroom",0);

        var position=p0!!.getIntExtra("position",0)
        Log.e("TAGSERVICE",chatroom.toString())

        val retrofithelper = Retrofithelper()

        val jsonArrayCall = retrofithelper.getClient().receiveChatMessages(
            PreferenceHelper(this).getData(
                Constants.tokenkey
            ), chatroom!!.toString()
        )



        jsonArrayCall.enqueue(object : Callback<List<ChatMessage>> {
            override fun onResponse(
                call: Call<List<ChatMessage>>,
                response: Response<List<ChatMessage>>
            ) {


                if (response.body() != null) {

                    var a=response.body();

                    if(a!!.size>0) {

                        var chatMessagebundle = ChatHistoryBundle()
                        chatMessagebundle.chatHistories = response.body()

                        val intent1 = Intent()
                        intent1.action = "com.quickjob.advertise.job.incomingchat"
                        intent1.putExtra("DATAPASSED", chatMessagebundle)
                        intent1.putExtra("position",position)
                        sendBroadcast(intent1)


                        ///  intentFilter.addAction("");

                    }




                } else {




                }


            }

            override fun onFailure(call: Call<List<ChatMessage>>, t: Throwable) {

                t.printStackTrace()


            }
        })

    }
}