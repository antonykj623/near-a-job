package com.advertise.quickjob.app.views

import android.content.Context
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.widget.AppCompatImageView
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import com.advertise.quickjob.app.R
import com.advertise.quickjob.app.Utils.Constants
import com.advertise.quickjob.app.adapter.HomepagerAdapter
import com.advertise.quickjob.app.domain.SearchSuggestion
import com.advertise.quickjob.app.fragments.AccountFragment
import com.advertise.quickjob.app.fragments.ChatFragment
import com.advertise.quickjob.app.fragments.HomeFragment
import com.advertise.quickjob.app.fragments.UploadsFragment
import com.advertise.quickjob.app.preferencehelper.PreferenceHelper
import com.advertise.quickjob.app.webservicehelper.Retrofithelper
import com.google.android.gms.tasks.OnSuccessListener
import com.google.android.material.tabs.TabLayout
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.InstanceIdResult
import com.google.gson.JsonObject
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.view.*;
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception

class MainActivity : AppCompatActivity() , TabLayout.OnTabSelectedListener,ViewPager.OnPageChangeListener{


    var fr:AccountFragment?=null;
var hf:HomeFragment?=null

    var uf:UploadsFragment?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportActionBar!!.hide()

        setupTabs()
        updateFirebaseID()
        getUserId()

    }




    fun setupTabs()
    {



             for(i in 0..3)
        {

            tabLayout.addTab(tabLayout.newTab())
//            var v=layoutInflater.inflate(R.layout.layout_customtab,null);

            var v=LayoutInflater.from(this).inflate(R.layout.layout_customtab,null)

            var img=v!!.findViewById<AppCompatImageView>(R.id.img)

            if(i==0)
            {
                img.setImageResource(R.drawable.ic_home)
               img.setColorFilter(Color.parseColor("#ffffff"))
            }

            if(i==1)
            {
                img.setImageResource(R.drawable.ic_chat)
            }

            if(i==2)
            {
                img.setImageResource(R.drawable.ic_file_upload)
            }

            if(i==3)
            {
                img.setImageResource(R.drawable.ic_person_black)
            }


                tabLayout.getTabAt(i)!!.customView=v

        }

       // tabLayout.setupWithViewPager(viewpager)

        fr=AccountFragment()

        uf=UploadsFragment()

        var frag_list=ArrayList<Fragment>();

        hf=HomeFragment()
        frag_list.add(hf!!)
        frag_list.add(ChatFragment())
        frag_list.add(uf!!)
        frag_list.add(fr!!)




        tabLayout.addOnTabSelectedListener(this)

        viewpager.adapter=HomepagerAdapter(supportFragmentManager,frag_list)
        viewpager.offscreenPageLimit=4
        viewpager.addOnPageChangeListener(this)



    }

    override fun onTabUnselected(p0: TabLayout.Tab?) {
                   var v:View?=p0!!.customView;

              var img=v!!.findViewById<AppCompatImageView>(R.id.img)

        img.setColorFilter(Color.parseColor("#000000"))

        //viewpager.setCurrentItem(p0.position)


          }

    override fun onTabSelected(p0: TabLayout.Tab?) {

        var v=p0!!.customView;

              var img=v!!.findViewById<AppCompatImageView>(R.id.img)

        img.setColorFilter(Color.parseColor("#ffffff"))

        viewpager.setCurrentItem(p0.position)

    }

    override fun onTabReselected(p0: TabLayout.Tab?) {

    }


    override fun onPageScrollStateChanged(state: Int) {

    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {



    }

    override fun onPageSelected(position: Int) {



                   var t= tabLayout.getTabAt(position);
        t!!.select()
           }


    override fun onRestart() {
        super.onRestart()

        if(fr!=null)
        {
            fr!!.showUserDetails()


        }

        if(hf!=null)
        {
            hf!!.refreshadapter()
        }

        if(uf!=null)
        {
            uf!!.showAllJobsOfMine()
        }
    }


    private fun updateFirebaseID() {


        FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener(this,
            OnSuccessListener<InstanceIdResult> { instanceIdResult ->
                val token = instanceIdResult.token

                val retrofithelper = Retrofithelper()





                val jsonArrayCall = retrofithelper.getClient().updateFirebasetoken(
                    PreferenceHelper(this).getData(
                        Constants.tokenkey),token
                )

                jsonArrayCall.enqueue(object : Callback<JsonObject> {
                    override fun onResponse(
                        call: Call<JsonObject>,
                        response: Response<JsonObject>
                    ) {

                    }

                    override fun onFailure(call: Call<JsonObject>, t: Throwable) {

                    }
                })
            })


    }


    fun getUserId()
    {
        val retrofithelper = Retrofithelper()





        val jsonArrayCall = retrofithelper.getClient().getUserId(
            PreferenceHelper(this).getData(
                Constants.tokenkey)
        )

        jsonArrayCall.enqueue(object : Callback<JsonObject> {
            override fun onResponse(
                call: Call<JsonObject>,
                response: Response<JsonObject>
            ) {

                try{


                    var js=JSONObject(response.body().toString())

                 //   {"status":1,"userid":"15"}
                    if(js.getInt("status")==1)
                    {


                        PreferenceHelper(this@MainActivity).putData(Constants.userid,js.getString("userid"))

                    }





                }catch (e:Exception)
                {


                }




            }

            override fun onFailure(call: Call<JsonObject>, t: Throwable) {

            }
        })

    }
}
