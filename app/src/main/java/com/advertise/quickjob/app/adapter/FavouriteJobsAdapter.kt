package com.advertise.quickjob.app.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.AppCompatImageView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.advertise.quickjob.app.R
import com.advertise.quickjob.app.Utils.Constants
import com.advertise.quickjob.app.Utils.DownloadUtils
import com.advertise.quickjob.app.domain.FavouriteJob
import com.advertise.quickjob.app.preferencehelper.PreferenceHelper
import com.advertise.quickjob.app.progress.ProgressFragment
import com.advertise.quickjob.app.views.FavouritesActivity
import com.advertise.quickjob.app.views.JobdetailsActivity
import com.advertise.quickjob.app.views.MainActivity
import com.advertise.quickjob.app.webservicehelper.Retrofithelper
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.gson.JsonObject
import kotlinx.android.synthetic.main.layout_favjoblist.view.*
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception

class FavouriteJobsAdapter(list: ArrayList<FavouriteJob>,context: Context) : RecyclerView.Adapter<FavouriteJobsAdapter.FavouritejobHolder>() {


    var li=list
    var con=context

    class FavouritejobHolder(v:View) :RecyclerView.ViewHolder(v)
    {
        var title=v.findViewById<TextView>(R.id.title)

        var txtwishlist=v.findViewById<TextView>(R.id.txtwishlist)

        var txtuploadDate=v.findViewById<TextView>(R.id.txtuploadDate)

        var txtSalaryinfo=v.findViewById<TextView>(R.id.txtSalaryinfo)

        var imgjobuser=v.findViewById<AppCompatImageView>(R.id.imgjobuser)

        var cardWishlist=v.findViewById<CardView>(R.id.cardWishlist)

        var viewpager=v.findViewById<ViewPager>(R.id.viewpager)


    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavouritejobHolder {


        var v= LayoutInflater.from(parent.context).inflate(R.layout.layout_favjoblist,parent,false)


        return FavouritejobHolder(v)
    }

    override fun getItemCount(): Int {

        return li.size
    }

    override fun onBindViewHolder(holder: FavouritejobHolder, position: Int) {


        holder.itemView.imgfavourite.visibility=View.GONE

        holder.title.setText(li.get(position).jobDetails.jobTitle)

        holder.txtwishlist.setText(li.get(position).jobDetails.jobDesccription)

        holder.txtuploadDate.setText(li.get(position).jobDetails.jobUploadedDate)

        holder.txtSalaryinfo.setText(li.get(position).jobDetails.jobSalaryInfo)


        Glide.with(con).load(Constants.profileimg + li.get(position).jobUser.user_image).apply(
            RequestOptions.circleCropTransform()
        ).into(holder.imgjobuser)

        holder.cardWishlist.setOnClickListener {


            var intent= Intent(con, JobdetailsActivity::class.java)


            var jb=li.get(position).jobDetails
            jb.jobUser=li.get(position).jobUser


            intent.putExtra("jobdetails",jb)

            con.startActivity(intent)




        }

        holder.itemView.fabremove.setOnClickListener {


            var p: ProgressFragment = ProgressFragment()
            p.show((con  as FavouritesActivity).supportFragmentManager,"dhjjd")
            val retrofithelper = Retrofithelper()

            var a: Call<JsonObject> = retrofithelper.getClient().removeFromFavourites(
                PreferenceHelper(con).getData(Constants.tokenkey),li.get(position).favouriteJobJobid)


            a.enqueue(object : Callback<JsonObject> {
                override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {

                    p.dismiss()

                    if(response.body()!=null)
                    {

                        var p=response.body();

                        try{



                            val json= JSONObject(p.toString())

                            if(json.getInt("status")==1)
                            {


                               li.removeAt(position)

                                Toast.makeText(con,json.getString("message"), Toast.LENGTH_SHORT).show()

                                notifyDataSetChanged()

                            }
                            else{




                                Toast.makeText(con,json.getString("message"), Toast.LENGTH_SHORT).show()
                            }







                        }catch (e: Exception)
                        {

                        }










                    }






                }

                override fun onFailure(call: Call<JsonObject>, t: Throwable) {

                    p.dismiss()

                    if(!DownloadUtils().isNetworkConnected(con))
                    {
                        DownloadUtils().showNoConnectiondialog(con);
                    }

                    t.printStackTrace()



                    //getProfileImage.php


                }
            })


        }

    }
}