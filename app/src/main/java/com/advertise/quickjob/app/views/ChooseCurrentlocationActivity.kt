package com.advertise.quickjob.app.views

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.location.Geocoder
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.annotation.NonNull
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.advertise.quickjob.app.R
import com.advertise.quickjob.app.progress.ProgressFragment
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import kotlinx.android.synthetic.main.activity_choose_currentlocation.*
import java.util.*

class ChooseCurrentlocationActivity : AppCompatActivity(), OnMapReadyCallback, LocationListener {

    private lateinit var mMap: GoogleMap



    var progressFragment: ProgressFragment?=null

    var locationManager: LocationManager?=null;

    var location: Location?=null

    var lat=0.0;
    var longi=0.0

    var qid="0"

    var result:String?="";

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_choose_currentlocation)
        supportActionBar!!.hide()

        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        imgback.setOnClickListener {

            onBackPressed()
        }

        fab_applied.setOnClickListener {


            val intent= Intent();
            intent.putExtra("locationName",result);
            intent.putExtra("lat",lat)
            intent.putExtra("longitude",longi)
            setResult(255,intent);
            finish();
        }

        Toast.makeText(this,"Click on the google map to pick location",Toast.LENGTH_SHORT).show()

        checkPermission()
    }









    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        // Add a marker in Sydney and move the camera

        mMap.setOnMapClickListener {


            lat = it.latitude
            longi = it.longitude



            val geocoder = Geocoder(this, Locale.getDefault())

            try {
                val addressList = geocoder.getFromLocation(
                    lat, longi, 1
                )
                if (addressList != null && addressList.size > 0) {
                    val address = addressList[0]


                    if (address != null) {
                        val sb = StringBuilder()
                        for (i in 0 until address.maxAddressLineIndex) {

                            if (address.getAddressLine(i) != null && address.getAddressLine(i) != "") {

                                sb.append(address.getAddressLine(i)).append(",")
                            }
                        }

                        if (address.locality != null) {
                            sb.append(address.locality).append(",")
                        }

                        if (address.countryName != null) {
                            sb.append(address.countryName).append("")
                        }
                        //sb.append(address.getCountryName());
                        result = sb.toString()


                        mMap.clear()

                        val sydney = LatLng(lat, longi)
                        mMap!!.addMarker(
                            MarkerOptions()
                                .position(sydney)
                                .title(result)


                        );


                        mMap!!.animateCamera( CameraUpdateFactory.newLatLngZoom(
                            LatLng(
                                lat, longi
                            ), 16.0f))


                    }
                } else {


                }
            } catch (e: Exception) {

            }


//            mMap.clear()
//
//            val sydney = LatLng(lat, longi)
//            mMap!!.addMarker(
//                MarkerOptions()
//                    .position(sydney)
//                    .title(result)
//
//
//            );


            mMap!!.animateCamera( CameraUpdateFactory.newLatLngZoom(
                LatLng(
                    lat, longi
                ), 16.0f))
        }

    }



    fun checkPermission() {

        //        if(CheckGpsStatus()) {


        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) !== PackageManager.PERMISSION_GRANTED
        ) {


            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                119
            )

        } else {


            if (!CheckGpsStatus()) {
                calllocation()
            } else {


                try {
                    progressFragment = ProgressFragment()
                    progressFragment!!.setCancelable(false)
                    progressFragment!!.show(supportFragmentManager, "dfgdf")
                    locationManager =
                        this@ChooseCurrentlocationActivity.getSystemService(Context.LOCATION_SERVICE) as LocationManager

                    location =
                        locationManager!!.getLastKnownLocation(LocationManager.GPS_PROVIDER)
                    locationManager!!.requestLocationUpdates(
                        LocationManager.NETWORK_PROVIDER,
                        12000,
                        7f,
                        this@ChooseCurrentlocationActivity
                    )
                } catch (e: SecurityException) {

                }


            }




        }



    }


    fun calllocation() {

        val mLocationRequest = LocationRequest.create()
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
            .setInterval(10 * 1000)
            .setFastestInterval(1 * 1000)


        val settingsBuilder = LocationSettingsRequest.Builder()
            .addLocationRequest(mLocationRequest)
        settingsBuilder.setAlwaysShow(true)


        val result = LocationServices.getSettingsClient(this)
            .checkLocationSettings(settingsBuilder.build())



        result.addOnCompleteListener(object : OnCompleteListener<LocationSettingsResponse> {

            override fun onComplete(@NonNull task: Task<LocationSettingsResponse>) {

                try {
                    val response = task.getResult(ApiException::class.java)
                } catch (ex: ApiException) {
                    when (ex.getStatusCode()) {
                        LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> try {
                            val resolvableApiException = ex as ResolvableApiException
                            resolvableApiException
                                .startResolutionForResult(
                                    this@ChooseCurrentlocationActivity,
                                    11
                                )
                        } catch (e: IntentSender.SendIntentException) {

                        }

                        LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                        }
                    }
                }

            }


        })


    }







    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 11 && resultCode == RESULT_OK) {

            try {
                progressFragment = ProgressFragment()
                progressFragment!!.setCancelable(false)
                progressFragment!!.show(supportFragmentManager, "dfgdf")
                locationManager =
                    this@ChooseCurrentlocationActivity.getSystemService(Context.LOCATION_SERVICE) as LocationManager

                location = locationManager!!.getLastKnownLocation(LocationManager.NETWORK_PROVIDER)
                locationManager!!.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER,
                    12000,
                    7f,
                    this@ChooseCurrentlocationActivity
                )
            } catch (e: SecurityException) {

            }

        }
    }

    override fun onLocationChanged(p0: Location?) {


        if (p0 != null) {

            lat = p0!!.getLatitude()
            longi = p0!!.getLongitude()


            val geocoder = Geocoder(this, Locale.getDefault())

            try {
                val addressList = geocoder.getFromLocation(
                    lat, longi, 1
                )
                if (addressList != null && addressList.size > 0) {
                    val address = addressList[0]


                    if (address != null) {
                        val sb = StringBuilder()
                        for (i in 0 until address.maxAddressLineIndex) {

                            if (address.getAddressLine(i) != null && address.getAddressLine(i) != "") {

                                sb.append(address.getAddressLine(i)).append(",")
                            }
                        }

                        if (address.locality != null) {
                            sb.append(address.locality).append(",")
                        }

                        if (address.countryName != null) {
                            sb.append(address.countryName).append("")
                        }
                        //sb.append(address.getCountryName());
                        result = sb.toString()


                        mMap.clear()

                        val sydney = LatLng(lat, longi)
                        mMap!!.addMarker(
                            MarkerOptions()
                                .position(sydney)
                                .title(result)


                        );


                        mMap!!.animateCamera( CameraUpdateFactory.newLatLngZoom(
                            LatLng(
                                lat, longi
                            ), 16.0f))


                    }
                } else {


                }
            } catch (e: Exception) {

            }

        } else {

            Toast.makeText(this, "Cannot fetch location please try again", Toast.LENGTH_LONG)
                .show()

        }

        if (progressFragment != null) {

            if (progressFragment!!.isVisible()) {
                progressFragment!!.dismiss()
            }

        }

    }

    override fun onStatusChanged(p0: String?, p1: Int, p2: Bundle?) {
    }

    override fun onProviderEnabled(p0: String?) {
    }

    override fun onProviderDisabled(p0: String?) {
    }



    fun CheckGpsStatus(): Boolean {

        locationManager =this.getSystemService(Context.LOCATION_SERVICE) as LocationManager

        return locationManager!!.isProviderEnabled(LocationManager.GPS_PROVIDER)

    }


    override fun onRequestPermissionsResult(requestCode: Int, @NonNull permissions: Array<String>, @NonNull grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

//        if (ContextCompat.checkSelfPermission(
//                this,
//                Manifest.permission.ACCESS_FINE_LOCATION
//            ) === PackageManager.PERMISSION_GRANTED
//        ) {


        checkPermission()
        // }

    }

}
