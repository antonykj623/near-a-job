package com.advertise.quickjob.app.views

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.advertise.quickjob.app.R
import com.advertise.quickjob.app.Utils.Constants
import com.advertise.quickjob.app.webservicehelper.RestApiInterface
import com.google.gson.JsonObject
import kotlinx.android.synthetic.main.activity_otp.*
import okhttp3.MediaType
import okhttp3.OkHttpClient
import okhttp3.RequestBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class OTPActivity : AppCompatActivity() {

    var pin=""
    var mob=""

    internal var baseurl = "https://2factor.in/API/V1/"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_otp)
        supportActionBar!!.hide()

         mob=intent.getStringExtra("mob");

        txtOtpcode.setText("Enter the OTP code that sent to your mobile number "+mob)

     //   mob="9747497967";

        val randomPIN = (Math.random() * 9000).toInt() + 1000
      pin   = "" + randomPIN


        btnSendOtp.setOnClickListener {


            if(!edtOTP!!.text.toString().equals(""))
            {
                if(edtOTP!!.text.toString().equals(pin))
                {



                    val intent=Intent()
                    intent.putExtra("message","OTP verified")
                    intent.putExtra("mobile",mob)
                    setResult(255,intent)
                    finish()



                }
                else{

                    Toast.makeText(this,"Enter correct otpcode",Toast.LENGTH_SHORT).show()
                }


            }
            else{

                Toast.makeText(this,"Enter otpcode",Toast.LENGTH_SHORT).show()
            }



        }


        txtResend.setOnClickListener {


            val randomPIN = (Math.random() * 9000).toInt() + 1000
            pin   = "" + randomPIN

            sendOTPcode()

        }
        sendOTPcode()
    }



    fun sendOTPcode()
    {

        val logging = HttpLoggingInterceptor()
        // set your desired log level
        logging.level = HttpLoggingInterceptor.Level.BODY

        val builder = OkHttpClient.Builder()
        builder.interceptors().add(logging)

        val httpClient = builder.build()

        val client = Retrofit.Builder()
            .baseUrl(baseurl)

            .addConverterFactory(GsonConverterFactory.create())
            .client(httpClient)
            .build()


        val restApiInterface =
            client.create<RestApiInterface>(RestApiInterface::class.java!!)

        val from = RequestBody.create(
            MediaType.parse("text/plain"),
            "NAJOBS"
        )

        val To = RequestBody.create(
            MediaType.parse("text/plain"),
            mob
        )

        val TemplateName = RequestBody.create(
            MediaType.parse("text/plain"),
            "Near a job"
        )

        val VAR1 = RequestBody.create(
            MediaType.parse("text/plain"),
            "Dear customer, We are from \'Near a job\'"
        )

        val VAR2 = RequestBody.create(
            MediaType.parse("text/plain"),
            pin
        )

        val jsonObjectCall =
            restApiInterface.sendOTP(Constants.msgapikey, from, To, TemplateName, VAR1, VAR2)

        jsonObjectCall.enqueue(object : Callback<JsonObject> {
            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {


                Toast.makeText(this@OTPActivity, "OTP code sent successfully", Toast.LENGTH_SHORT)
                    .show()

            }

            override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                //  Toast.makeText(otptestActivity.this,t.toString(),Toast.LENGTH_SHORT).show();

            }
        })
        
    }
}
