package com.advertise.quickjob.app.adapter

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.advertise.quickjob.app.R
import com.advertise.quickjob.app.Utils.Constants
import com.advertise.quickjob.app.Utils.DownloadUtils
import com.advertise.quickjob.app.domain.AppliedJob
import com.advertise.quickjob.app.domain.ChatRoom
import com.advertise.quickjob.app.preferencehelper.PreferenceHelper
import com.advertise.quickjob.app.progress.ProgressFragment
import com.advertise.quickjob.app.views.AppliedCandidatesActivity
import com.advertise.quickjob.app.views.AppliedJobsActivity
import com.advertise.quickjob.app.views.ChatroomActivity
import com.advertise.quickjob.app.views.LoginActivity
import com.advertise.quickjob.app.webservicehelper.Retrofithelper
import com.google.gson.JsonObject
import kotlinx.android.synthetic.main.layout_appliedjob.view.*
import kotlinx.android.synthetic.main.layout_joblist.view.*
import kotlinx.android.synthetic.main.layout_joblist.view.imgjobuser
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception

class AppliedJobsAdapter(context: Context,list: ArrayList<AppliedJob>):RecyclerView.Adapter<AppliedJobsAdapter.AppliedJobHolder>() {


    var con=context
    var li=list

    public class AppliedJobHolder(v:View):RecyclerView.ViewHolder(v)
    {
        var title=v.findViewById<TextView>(R.id.title)

        var txtwishlist=v.findViewById<TextView>(R.id.txtwishlist)

        var txtuploadDate=v.findViewById<TextView>(R.id.txtuploadDate)

        var txtSalaryinfo=v.findViewById<TextView>(R.id.txtSalaryinfo)

       // var view1=v.findViewById<View>(R.id.view1)


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AppliedJobHolder {

        var v=LayoutInflater.from(parent.context).inflate(R.layout.layout_appliedjob,parent,false)

        return AppliedJobHolder(v)

    }

    override fun getItemCount(): Int {

return li.size
    }

    override fun onBindViewHolder(holder: AppliedJobHolder, position: Int) {

//holder.itemView.imgjobuser.visibility=View.GONE

        holder.title.setText(li.get(position).jobdetail.jobTitle)

        holder.txtwishlist.setText(li.get(position).jobdetail.jobDesccription)

        holder.txtuploadDate.setText(li.get(position).jobdetail.jobUploadedDate)


        var c=li.get(position).jobdetail.jobSalaryInfo.split("to")

        var a1=c[0].trim()

        var a2=c[1].trim()

        var maximumsalary=a2.replace("Rs","")

        var a3=a1.replace("Between","")

        var minimumsalary=a3.replace("Rs","")




        holder.txtSalaryinfo.setText(con.getString(R.string.rs)+" "+minimumsalary+"-"+maximumsalary)

        if(li.get(position).job_approved_status.equals("0"))
        {

            holder.itemView.layout_approved.visibility=View.GONE
                //  holder.itemView.layout_call.visibility=View.GONE
            holder.itemView.layout_chat.visibility=View.GONE
                // holder.view1.visibility=View.GONE
        }
        else{

            holder.itemView.layout_approved.visibility=View.VISIBLE

            if(li.get(position).jobdetail.jobPhoneVisibleStatus.equals("1"))
            {

               // holder.itemView.layout_call.visibility=View.GONE
             //   holder.itemView.v.visibility=View.GONE
            }
            else{
               // holder.itemView.layout_call.visibility=View.VISIBLE
               // holder.itemView.v.visibility=View.VISIBLE

            }



            holder.itemView.layout_chat.visibility=View.VISIBLE

        }

        holder.itemView.txtCancel.setOnClickListener {

//            val builder = AlertDialog.Builder(con)
//            builder.setTitle("Near a job")
//            builder.setMessage("Do you want to cancel the applied job ?")
//            builder.setNegativeButton(
//                "yes"
//            ) {
//
//
//                    dialog, which -> dialog.dismiss()
//
//              cancelAppliedJob(li.get(position).jobId,position)
//            }
//
//
//            builder.setPositiveButton(
//                "no"
//            ) {
//
//
//                    dialog, which -> dialog.dismiss()
//
//
//            }
//            val alertDialog = builder.create()
//            alertDialog.show()

        }


        holder.itemView.imgcall.setOnClickListener {

//            val intent = Intent(Intent.ACTION_DIAL)
//            intent.data = Uri.parse("tel:"+li.get(position).jobdetail.jobuser.userPhone)
//            con.startActivity(intent)


            val builder = AlertDialog.Builder(con)
            builder.setTitle("Near a job")
            builder.setMessage("Do you want to cancel the applied job ?")
            builder.setNegativeButton(
                "yes"
            ) {


                    dialog, which -> dialog.dismiss()

                cancelAppliedJob(li.get(position).jobId,position)
            }


            builder.setPositiveButton(
                "no"
            ) {


                    dialog, which -> dialog.dismiss()


            }
            val alertDialog = builder.create()
            alertDialog.show()

        }

        holder.itemView.txtCall.setOnClickListener {

//            val intent = Intent(Intent.ACTION_DIAL)
//            intent.data = Uri.parse("tel:"+li.get(position).jobdetail.jobuser.userPhone)
//            con.startActivity(intent)

            val builder = AlertDialog.Builder(con)
            builder.setTitle("Near a job")
            builder.setMessage("Do you want to cancel the applied job ?")
            builder.setNegativeButton(
                "yes"
            ) {


                    dialog, which -> dialog.dismiss()

                cancelAppliedJob(li.get(position).jobId,position)
            }


            builder.setPositiveButton(
                "no"
            ) {


                    dialog, which -> dialog.dismiss()


            }
            val alertDialog = builder.create()
            alertDialog.show()

        }

        holder.itemView.imgchat.setOnClickListener {



            createChatroom(li.get(position).jobdetail.jobuser.userId)

        }


        holder.itemView.txtChat.setOnClickListener {

            createChatroom(li.get(position).jobdetail.jobuser.userId)

        }

    }



    fun createChatroom(jobuser:String)
    {



        var progressFragment= ProgressFragment()
        progressFragment.show((con as AppliedJobsActivity).supportFragmentManager,"")


        val retrofithelper = Retrofithelper()

        var a: Call<ChatRoom> = retrofithelper.getClient().createChatroom(
            PreferenceHelper(con).getData(
                Constants.tokenkey),jobuser)


        a.enqueue(object : Callback<ChatRoom> {
            override fun onResponse(call: Call<ChatRoom>, response: Response<ChatRoom>) {

                progressFragment.dismiss()

                if(response.body()!=null)
                {

                    var pa=response.body();

                    try{

                        if(pa!=null) {

                            try{


                                if(pa.status==1)
                                {

                                    val intent=Intent(con, ChatroomActivity::class.java)
                                    intent.putExtra("chatroom",pa)
                                    con.startActivity(intent)




                                }
                                else{


                                    Toast.makeText(con,"Error occured", Toast.LENGTH_SHORT).show()
                                }









                            }catch (ex: Exception)
                            {

                            }


                        }




                    }catch (e: Exception)
                    {

                    }










                }






            }

            override fun onFailure(call: Call<ChatRoom>, t: Throwable) {

                progressFragment.dismiss()

                if(!DownloadUtils().isNetworkConnected(con))
                {
                    DownloadUtils().showNoConnectiondialog(con);
                }

                t.printStackTrace()






            }
        })


    }


    fun cancelAppliedJob(jobid:String,pos:Int)
    {
        val retrofithelper = Retrofithelper()

        var a: Call<JsonObject> = retrofithelper.getClient().cancelAppliedJob(
            PreferenceHelper(con).getData(
                Constants.tokenkey),jobid)


        a.enqueue(object : Callback<JsonObject> {
            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {



                if(response.body()!=null)
                {

                    var p=response.body();

                    try{

                        if(p!=null) {


                            var j= JSONObject(p.toString())

                            if(j.getInt("status")==1)
                            {

                                Toast.makeText(con,"Cancelled your application successfully ",Toast.LENGTH_SHORT).show()


                                li.removeAt(pos)

                                notifyDataSetChanged()


                            }
                            else{


                                Toast.makeText(con,"Error",Toast.LENGTH_SHORT).show()

                            }






                        }




                    }catch (e: Exception)
                    {

                    }










                }






            }

            override fun onFailure(call: Call<JsonObject>, t: Throwable) {



                if(!DownloadUtils().isNetworkConnected(con))
                {
                    DownloadUtils().showNoConnectiondialog(con);
                }

                t.printStackTrace()






            }
        })

    }
}