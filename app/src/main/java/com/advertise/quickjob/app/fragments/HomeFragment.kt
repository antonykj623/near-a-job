package com.advertise.quickjob.app.fragments


import android.content.Intent
import android.database.MatrixCursor
import android.os.Bundle
import android.provider.BaseColumns
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AutoCompleteTextView
import android.widget.CursorAdapter
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.cursoradapter.widget.SimpleCursorAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.advertise.quickjob.app.R
import kotlinx.android.synthetic.main.fragment_home.view.*


import com.advertise.quickjob.app.Utils.Constants
import com.advertise.quickjob.app.Utils.DownloadUtils
import com.advertise.quickjob.app.adapter.HomeDataAdapter
import com.advertise.quickjob.app.adapter.JobListAdapter
import com.advertise.quickjob.app.adapter.SearchSuggestionsAdapter
import com.advertise.quickjob.app.adapter.UploadedJoblistAdapter
import com.advertise.quickjob.app.domain.Ads
import com.advertise.quickjob.app.domain.JobDetails
import com.advertise.quickjob.app.domain.SearchSuggestion
import com.advertise.quickjob.app.preferencehelper.PreferenceHelper
import com.advertise.quickjob.app.views.ChooseLocationActivity
import com.advertise.quickjob.app.webservicehelper.Retrofithelper
import kotlinx.android.synthetic.main.fragment_favourites.view.*
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_home.view.recycler_view
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception

import java.util.*
import kotlin.collections.ArrayList


/**
 * A simple [Fragment] subclass.
 */
class HomeFragment : Fragment() {

    var v:View?=null

    var page:Int=1

    val maprqcode=111

    var joblist=ArrayList<JobDetails>()
    var ads=ArrayList<Ads>()

    var isend:Boolean=false

    var isloading:Boolean=false

    var homeDataAdapter: HomeDataAdapter?=null

    internal var mAdapter: SimpleCursorAdapter?=null

    internal var pincodeFlag = false

    internal var pincodes: MutableList<SearchSuggestion> = java.util.ArrayList<SearchSuggestion>()

    internal val from = arrayOf("searchSuggestion")
    internal val to = intArrayOf(R.id.txt)

    var lat=0.0;
    var longi=0.0


    var querytxt=""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        v= inflater.inflate(R.layout.fragment_home, container, false)

        val linearLayout1 = v!!.searchView.getChildAt(0) as LinearLayout
        val linearLayout2 = linearLayout1.getChildAt(2) as LinearLayout
        val linearLayout3 = linearLayout2.getChildAt(1) as LinearLayout
        val autoComplete = linearLayout3.getChildAt(0) as AutoCompleteTextView

        val dimen_search = resources.getDimension(R.dimen.dimen_4dp)


        autoComplete.textSize = dimen_search.toFloat()

        var jb=JobDetails()
        jb.job_id="-1"
        joblist.add(jb)
        v!!.recycler_view.layoutManager=LinearLayoutManager(activity)
        homeDataAdapter= HomeDataAdapter(activity!!,joblist,ads)
        v!!.recycler_view.adapter=homeDataAdapter





        mAdapter = SimpleCursorAdapter(
            activity!!,
            R.layout.layout_listitem,
            null,
            from,
            to,
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER
        )
        v!!.searchView.setSuggestionsAdapter(mAdapter!!)



        showAllJobsOfMine()

        v!!.recycler_view.addOnScrollListener(object : RecyclerView.OnScrollListener() {
           override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)


                val offset = recyclerView.computeVerticalScrollOffset()
                val extent = recyclerView.computeVerticalScrollExtent()
                val range = recyclerView.computeVerticalScrollRange()

                val percentage = 100.0f * offset / (range - extent).toFloat()

                val a = percentage.toInt()





                    if (!isend) {

                        if (a == 100 && !isloading) {
                            var  abb=joblist.size-1

                            page = abb + 1
                            showAllJobsOfMine()

                        }
                    }



            }
        })




        v!!.imgloc.setOnClickListener {



            startActivityForResult(Intent(activity!!,ChooseLocationActivity::class.java),maprqcode)

        }

        v!!.imgpicklocation.setOnClickListener {


            if(lat!=0.0&&longi!=0.0)
            {


                v!!.txtLocation.setText("Choose location")
                v!!.imgpicklocation.setImageResource(R.drawable.ic_arrow_drop_down)

                lat=0.0
                longi=0.0

                joblist.clear()

                page=1


                var jb=JobDetails()
                jb.job_id="-1"
                joblist.add(jb)

                showAllJobsOfMine()

            }
            else {

                startActivityForResult(
                    Intent(activity!!, ChooseLocationActivity::class.java),
                    maprqcode
                )
            }

        }

        v!!.txtLocation.setOnClickListener {


            startActivityForResult(Intent(activity!!,ChooseLocationActivity::class.java),maprqcode)

        }



        v!!.searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
           override fun onQueryTextSubmit(s: String): Boolean {

                if (!s.equals("", ignoreCase = true)) {

                    if (s.length > 2) {

                        getSearchSuggestions(s)
                    } else {

                        pincodes.clear()

                    }
                } else {

                    pincodes.clear()


                    joblist.clear()

                    var jb=JobDetails()
                    jb.job_id="-1"
                    joblist.add(jb)
                    page=1
                    querytxt=""

                    showAllJobsOfMine()

                }

                return false
            }

            override  fun onQueryTextChange(s: String): Boolean {

                if (!s.equals("", ignoreCase = true)) {

                    if (s.length > 2) {

                        getSearchSuggestions(s)

                    } else {

                        pincodes.clear()


                    }
                } else {
                    pincodes.clear()

                    joblist.clear()

                    var jb=JobDetails()
                    jb.job_id="-1"
                    joblist.add(jb)
                    page=1

                    querytxt=""

                    showAllJobsOfMine()

                }

                return false
            }
        })




        v!!.searchView.setOnSuggestionListener(object : SearchView.OnSuggestionListener {
           override fun onSuggestionSelect(i: Int): Boolean {
                return false
            }

           override fun onSuggestionClick(i: Int): Boolean {

                var word=pincodes.get(i).jobTitle
               querytxt=word

               v!!.searchView.setQuery(word,false)

               pincodes.clear()

               joblist.clear()

               page=1


               var jb=JobDetails()
               jb.job_id="-1"
               joblist.add(jb)

              // showAllJobsOfMine()

               showAllJobsOfMine()


                return false
            }
        })










        getAllAds()
        return v!!

    }




    fun getSearchSuggestions(s:String)
    {
        val retrofithelper = Retrofithelper()

        var a: Call<List<SearchSuggestion>> = retrofithelper.getClient().getSearchSuggestions(
            PreferenceHelper(activity).getData(
                Constants.tokenkey),s)


        a.enqueue(object : Callback<List<SearchSuggestion>> {
            override fun onResponse(call: Call<List<SearchSuggestion>>, response: Response<List<SearchSuggestion>>) {



                if(response.body()!=null)
                {

                    var p=response.body();

                    try{

                        if(p!=null) {

                            if(p.size>0) {


                                pincodes.clear()

                                pincodes.addAll(response.body()!!)

                                val c = MatrixCursor(arrayOf(BaseColumns._ID, "searchSuggestion"))
                                for (i in pincodes.indices) {
                                    // if (SUGGESTIONS[i].toLowerCase().startsWith(query.toLowerCase()))
                                    c.addRow(
                                        arrayOf<Any>(
                                            i,
                                              pincodes[i].jobTitle
                                        )
                                    )
                                }
                                mAdapter!!.changeCursor(c)

                            }
                        }




                    }catch (e: Exception)
                    {

                    }










                }






            }

            override fun onFailure(call: Call<List<SearchSuggestion>>, t: Throwable) {



                if(!DownloadUtils().isNetworkConnected(activity))
                {
                    DownloadUtils().showNoConnectiondialog(activity);
                }

                t.printStackTrace()






            }
        })
    }











    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)


        if(requestCode==maprqcode&&resultCode==255&&data!=null)
        {

             lat=data.getDoubleExtra("lat",0.0)
             longi=data.getDoubleExtra("longitude",0.0)
            var location=data.getStringExtra("locationName")

            v!!.imgpicklocation.setImageResource(R.drawable.ic_close_)

 txtLocation.setText(location)

            joblist.clear()

            var jb=JobDetails()
            jb.job_id="-1"
            joblist.add(jb)

            page=1

            showAllJobsOfMine()


        }





    }





    fun refreshadapter()
    {
//        if(v!!.recycler_view.adapter!=null)
//        {
//            v!!.recycler_view.adapter!!.notifyDataSetChanged()
//        }

    }




    fun getAllAds()
    {
        val retrofithelper = Retrofithelper()

        var a: Call<List<Ads>> = retrofithelper.getClient().getAllads(
            PreferenceHelper(activity).getData(
                Constants.tokenkey))


        a.enqueue(object : Callback<List<Ads>> {
            override fun onResponse(call: Call<List<Ads>>, response: Response<List<Ads>>) {



                if(response.body()!=null)
                {

                    var p=response.body();

                    try{

                        if(p!=null) {

                            if(p.size>0) {


                                ads.addAll(p)

                                //checkadsPosition()

                                if(homeDataAdapter!=null) {

                                    homeDataAdapter!!.notifyDataSetChanged()
                                }


                            }
                        }




                    }catch (e: Exception)
                    {

                    }










                }






            }

            override fun onFailure(call: Call<List<Ads>>, t: Throwable) {



                if(!DownloadUtils().isNetworkConnected(activity))
                {
                    DownloadUtils().showNoConnectiondialog(activity);
                }

                t.printStackTrace()






            }
        })
    }







//    fun checkadsPosition()
//    {
//
//
//        var count=joblist.size/ads.size
//
//       // Toast.makeText(activity!!,count.toString(),Toast.LENGTH_SHORT).show()
//
//        var i=0;
//
//
//
//        for (j in joblist)
//        {
//
//            if(i==count-1)
//            {
//
//                //j.ads=
//
//
//
//                i=0
//            }
//
//
//        }
//
//
//    }





    fun showAllJobsOfMine()
    {

        isloading=true

        val retrofithelper = Retrofithelper()

        var a: Call<List<JobDetails>> = retrofithelper.getClient().getAllJobsFrontpage(
            PreferenceHelper(activity).getData(
                Constants.tokenkey),page,querytxt,lat.toString(),longi.toString())


        a.enqueue(object : Callback<List<JobDetails>> {
            override fun onResponse(call: Call<List<JobDetails>>, response: Response<List<JobDetails>>) {



                isloading=false

                if(response.body()!=null)
                {

                    var p=response.body();

                    try{

                        if(p!=null) {

                            if(p.size>0) {
                                if (p!!.get(0).status.equals("1")) {

                                    var a=p as ArrayList<JobDetails>

                                    joblist.addAll(a)


                                    val min = 1
                                    val max = joblist.size
                                    val random = Random().nextInt(max - min + 1) + min

                                    joblist.get(random).isContainAds=true


//                                    v!!.recycler_view.adapter= JobListAdapter(activity!!,joblist)

//                                    v!!.recycler_view.adapter= JobListAdapter(activity!!,joblist)

                                    homeDataAdapter!!.notifyDataSetChanged()

                                    v!!.recycler_view.scrollToPosition(page-1)

                                }

                            }
                            else{

                                if(page==1)
                                {
                                    joblist.clear()


                                    if(homeDataAdapter!=null) {

                                        homeDataAdapter!!.notifyDataSetChanged()
                                    }
                                }

                            }
                        }




                    }catch (e: Exception)
                    {

                    }










                }






            }

            override fun onFailure(call: Call<List<JobDetails>>, t: Throwable) {

                isloading=false

                if(!DownloadUtils().isNetworkConnected(activity))
                {
                    DownloadUtils().showNoConnectiondialog(activity);
                }

                t.printStackTrace()






            }
        })
    }


}
