package com.advertise.quickjob.app.domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ChatRoom implements Serializable {

    @SerializedName("status")
    @Expose
    private Integer status=0;
    @SerializedName("chatroomid")
    @Expose
    private Integer chatroomid=0;
    @SerializedName("user")
    @Expose
    private Jobuser user=new Jobuser();
    @SerializedName("message")
    @Expose
    private String message="";

    private String messagetype="0";

    private String senderid="0";

    private String receiverid="0";

    public ChatRoom() {
    }

    public String getMessagetype() {
        return messagetype;
    }

    public void setMessagetype(String messagetype) {
        this.messagetype = messagetype;
    }

    public String getSenderid() {
        return senderid;
    }

    public void setSenderid(String senderid) {
        this.senderid = senderid;
    }

    public String getReceiverid() {
        return receiverid;
    }

    public void setReceiverid(String receiverid) {
        this.receiverid = receiverid;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getChatroomid() {
        return chatroomid;
    }

    public void setChatroomid(Integer chatroomid) {
        this.chatroomid = chatroomid;
    }

    public Jobuser getUser() {
        return user;
    }

    public void setUser(Jobuser user) {
        this.user = user;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
