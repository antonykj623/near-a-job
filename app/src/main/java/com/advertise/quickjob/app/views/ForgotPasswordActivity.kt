package com.advertise.quickjob.app.views

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.advertise.quickjob.app.R
import com.advertise.quickjob.app.Utils.Constants
import com.advertise.quickjob.app.Utils.DownloadUtils
import com.advertise.quickjob.app.preferencehelper.PreferenceHelper
import com.advertise.quickjob.app.progress.ProgressFragment
import com.advertise.quickjob.app.webservicehelper.Retrofithelper
import com.google.gson.JsonObject
import kotlinx.android.synthetic.main.activity_forgot_password.*
import kotlinx.android.synthetic.main.activity_forgot_password.imgback
import kotlinx.android.synthetic.main.activity_user_profile_edit.*
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ForgotPasswordActivity : AppCompatActivity() {

    var mob=""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_password)
        supportActionBar!!.hide()

        mob=intent.getStringExtra("mob");


        imgback.setOnClickListener {

            onBackPressed()
        }

        btn_submit.setOnClickListener {


            if(!input_password.text.toString().equals(""))
            {
                if(input_password.text.toString().length>=6)
                {

                    if(input_password.text.toString().equals(input_reEnterPassword.text.toString()))
                    {




                        var p: ProgressFragment = ProgressFragment()
                        p.show(supportFragmentManager,"dhjjd")
                        val retrofithelper = Retrofithelper()

                        var a: Call<JsonObject> = retrofithelper.getClient().changePassword(mob,input_password.text.toString()
                            
                        )


                        a.enqueue(object : Callback<JsonObject> {
                            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {

                                p.dismiss()

                                if(response.body()!=null)
                                {

                                    var p=response.body();

                                    var js= JSONObject(p.toString())

                                    if(js.getInt("status")==1)
                                    {


                                        Toast.makeText(this@ForgotPasswordActivity,"Passwword changed successfully",Toast.LENGTH_SHORT).show()


                                        finish()

                                    }
                                    else{

                                        Toast.makeText(this@ForgotPasswordActivity,"Unknown error",Toast.LENGTH_SHORT).show()


                                    }








                                }






                            }

                            override fun onFailure(call: Call<JsonObject>, t: Throwable) {

                                p.dismiss()

                                if(!DownloadUtils().isNetworkConnected(this@ForgotPasswordActivity))
                                {
                                    DownloadUtils().showNoConnectiondialog(this@ForgotPasswordActivity);
                                }

                                t.printStackTrace()






                            }
                        })


























                    }
                    else
                    {
                        Toast.makeText(this,"Password confirmation failed", Toast.LENGTH_SHORT).show()
                    }





                }
                else
                {
                    Toast.makeText(this,"Enter password with atleast 6 characters", Toast.LENGTH_SHORT).show()
                }


            }
            else
            {

                Toast.makeText(this,"Enter password", Toast.LENGTH_SHORT).show()


            }


        }
    }
}
