package com.advertise.quickjob.app.views

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.advertise.quickjob.app.R
import com.advertise.quickjob.app.Utils.Constants
import com.advertise.quickjob.app.Utils.DownloadUtils
import com.advertise.quickjob.app.adapter.AdsAdapter
import com.advertise.quickjob.app.domain.Ads
import com.advertise.quickjob.app.preferencehelper.PreferenceHelper
import com.advertise.quickjob.app.webservicehelper.Retrofithelper
import kotlinx.android.synthetic.main.activity_full_ads.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception

class FullAdsActivity : AppCompatActivity() {

    var ads=ArrayList<Ads>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_full_ads)
        supportActionBar!!.hide()

        imgback.setOnClickListener {

            onBackPressed()
        }

        getAllAds()
    }

    fun getAllAds()
    {
        val retrofithelper = Retrofithelper()

        var a: Call<List<Ads>> = retrofithelper.getClient().getAllads(
            PreferenceHelper(this).getData(
                Constants.tokenkey))


        a.enqueue(object : Callback<List<Ads>> {
            override fun onResponse(call: Call<List<Ads>>, response: Response<List<Ads>>) {



                if(response.body()!=null)
                {

                    var p=response.body();

                    try{

                        if(p!=null) {

                            if(p.size>0) {


                                ads.addAll(p)

                                //checkadsPosition()

                              recycler_view.layoutManager=
                                    GridLayoutManager(this@FullAdsActivity,2)

                                recycler_view.adapter= AdsAdapter(this@FullAdsActivity,ads)


                            }
                        }




                    }catch (e: Exception)
                    {

                    }










                }






            }

            override fun onFailure(call: Call<List<Ads>>, t: Throwable) {



                if(!DownloadUtils().isNetworkConnected(this@FullAdsActivity))
                {
                    DownloadUtils().showNoConnectiondialog(this@FullAdsActivity);
                }

                t.printStackTrace()






            }
        })
    }

}
