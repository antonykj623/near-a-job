package com.advertise.quickjob.app.domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AppliedJob {

    @SerializedName("applied job_id")
    @Expose
    private String appliedJobId;
    @SerializedName("job_id")
    @Expose
    private String jobId;
    @SerializedName("job_employer_id")
    @Expose
    private String jobEmployerId;
    @SerializedName("job_applied_userid")
    @Expose
    private String jobAppliedUserid;
    @SerializedName("job_applied_date")
    @Expose
    private String jobAppliedDate;
    @SerializedName("jobdetail")
    @Expose
    private Jobdetail jobdetail;

    @SerializedName("job_approved_status")
    @Expose
    private String job_approved_status;

    public AppliedJob() {
    }


    public String getJob_approved_status() {
        return job_approved_status;
    }

    public void setJob_approved_status(String job_approved_status) {
        this.job_approved_status = job_approved_status;
    }

    public String getAppliedJobId() {
        return appliedJobId;
    }

    public void setAppliedJobId(String appliedJobId) {
        this.appliedJobId = appliedJobId;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public String getJobEmployerId() {
        return jobEmployerId;
    }

    public void setJobEmployerId(String jobEmployerId) {
        this.jobEmployerId = jobEmployerId;
    }

    public String getJobAppliedUserid() {
        return jobAppliedUserid;
    }

    public void setJobAppliedUserid(String jobAppliedUserid) {
        this.jobAppliedUserid = jobAppliedUserid;
    }

    public String getJobAppliedDate() {
        return jobAppliedDate;
    }

    public void setJobAppliedDate(String jobAppliedDate) {
        this.jobAppliedDate = jobAppliedDate;
    }

    public Jobdetail getJobdetail() {
        return jobdetail;
    }

    public void setJobdetail(Jobdetail jobdetail) {
        this.jobdetail = jobdetail;
    }
}
