package com.advertise.quickjob.app.views

import android.app.AlertDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.advertise.quickjob.app.R
import com.advertise.quickjob.app.Utils.Constants
import com.advertise.quickjob.app.Utils.DownloadUtils
import com.advertise.quickjob.app.adapter.JobListAdapter
import com.advertise.quickjob.app.domain.JobDetails
import com.advertise.quickjob.app.preferencehelper.PreferenceHelper
import com.advertise.quickjob.app.progress.ProgressFragment
import com.advertise.quickjob.app.webservicehelper.Retrofithelper
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.gson.JsonObject
import kotlinx.android.synthetic.main.activity_jobdetails.*
import kotlinx.android.synthetic.main.activity_jobdetails.card_location
import kotlinx.android.synthetic.main.activity_jobdetails.fabcall
import kotlinx.android.synthetic.main.activity_jobdetails.imgback
import kotlinx.android.synthetic.main.activity_jobdetails.profile
import kotlinx.android.synthetic.main.activity_jobdetails.txtAddress
import kotlinx.android.synthetic.main.activity_jobdetails.txtDescription
import kotlinx.android.synthetic.main.activity_jobdetails.txtJobValidity
import kotlinx.android.synthetic.main.activity_jobdetails.txtQualification
import kotlinx.android.synthetic.main.activity_jobdetails.txtSalaryinfo
import kotlinx.android.synthetic.main.activity_jobdetails.txtSalarytype
import kotlinx.android.synthetic.main.activity_jobdetails.txttitle
import kotlinx.android.synthetic.main.activity_jobdetails.username
import kotlinx.android.synthetic.main.activity_my_job_details.*
import kotlinx.android.synthetic.main.fragment_home.view.*
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception

class MyJobDetailsActivity : AppCompatActivity(), OnMapReadyCallback {

    var jb :JobDetails?=null

    var gmap:GoogleMap?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_job_details)
        supportActionBar!!.hide()

        jb=intent.getSerializableExtra("jobdetails") as JobDetails

        imgback.setOnClickListener {

            onBackPressed()
        }

        imgedit.setOnClickListener {


            val builder = AlertDialog.Builder(this)
            builder.setTitle("Near a job")
            builder.setMessage("Do you want to edit this job request ?")
            builder.setNegativeButton(
                "yes"
            ) {


                    dialog, which -> dialog.dismiss()


                var intent=Intent(this,EditJobActivity::class.java)

                intent.putExtra("jobdetails",jb)

                startActivity(intent)





            }


            builder.setPositiveButton(
                "no"
            ) {


                    dialog, which -> dialog.dismiss()


            }
            val alertDialog = builder.create()
            alertDialog.show()


        }

        imgdelete.setOnClickListener {

            val builder = AlertDialog.Builder(this)
            builder.setTitle("Near a job")
            builder.setMessage("Do you want to delete this job request ?")
            builder.setNegativeButton(
                "yes"
            ) {


                    dialog, which -> dialog.dismiss()



                var p: ProgressFragment = ProgressFragment()
                p.show(supportFragmentManager,"dhjjd")
                val retrofithelper = Retrofithelper()

                var a: Call<JsonObject> = retrofithelper.getClient().removeUploadedJob(
                    PreferenceHelper(this).getData(Constants.tokenkey),jb!!.job_id)


                a.enqueue(object : Callback<JsonObject> {
                    override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {

                        p.dismiss()

                        if(response.body()!=null)
                        {

                            var p=response.body();

                            try
                            {



                                val json= JSONObject(p.toString())

                                if(json.getInt("status")==1)
                                {


                                   finish()


                                    Toast.makeText(this@MyJobDetailsActivity,json.getString("message"), Toast.LENGTH_SHORT).show()



                                }
                                else{




                                    Toast.makeText(this@MyJobDetailsActivity,json.getString("message"), Toast.LENGTH_SHORT).show()
                                }







                            }catch (e: Exception)
                            {

                            }










                        }






                    }

                    override fun onFailure(call: Call<JsonObject>, t: Throwable) {

                        p.dismiss()

                        if(!DownloadUtils().isNetworkConnected(this@MyJobDetailsActivity))
                        {
                            DownloadUtils().showNoConnectiondialog(this@MyJobDetailsActivity);
                        }

                        t.printStackTrace()



                        //getProfileImage.php


                    }
                })




            }


            builder.setPositiveButton(
                "no"
            ) {


                    dialog, which -> dialog.dismiss()


            }
            val alertDialog = builder.create()
            alertDialog.show()

        }

//        imguserapplied.setOnClickListener {
//
////            var intent= Intent(this, AppliedCandidatesActivity::class.java)
////            intent.putExtra("jobid",jb!!.job_id)
////
////            startActivity(intent)
//
//        }



        fabapplied.setOnClickListener {
            var intent= Intent(this, AppliedCandidatesActivity::class.java)
            intent.putExtra("jobid",jb!!.job_id)

            startActivity(intent)

        }

        txttitle.setText(jb!!.jobTitle)
        txtDescription.setText(jb!!.jobDesccription)
        txtSalaryinfo.setText(jb!!.jobSalaryInfo)
        txtSalarytype.setText("Salary type : "+jb!!.jobSalaryType)
        txtJobValidity.setText(" Work validity :  "+jb!!.jobValidity)

        var pro= Constants.profileimg + jb!!.jobUser.user_image;

        Glide.with(this).load(pro).apply(
            RequestOptions.circleCropTransform()
        ).into(profile)

        username.setText(jb!!.jobUser.userName)

        txtQualification.setText("Qualification : "+jb!!.qualificationName)

        txtAddress.setText(jb!!.jobAddress)


        if(jb!!.jobLatitude.toDouble()!=0.0&&jb!!.jobLongitude.toDouble()!=0.0) {
            val mapFragment = supportFragmentManager
                .findFragmentById(com.advertise.quickjob.app.R.id.map) as SupportMapFragment?
            mapFragment!!.getMapAsync(this)

            card_location.visibility= View.VISIBLE
        }
        else{

            card_location.visibility= View.GONE
        }

        if(jb!!.jobPhoneVisibleStatus.toInt()==1)
        {
            fabcall.visibility= View.GONE
        }
        else{
            fabcall.visibility= View.VISIBLE
        }



        fab_applied.setOnClickListener {


            var intent= Intent(this, AppliedCandidatesActivity::class.java)
            intent.putExtra("jobid",jb!!.job_id)

            startActivity(intent)
        }



    }

    override fun onMapReady(p0: GoogleMap?) {

        gmap=p0

        val sydney = LatLng(jb!!.jobLatitude.toDouble(), jb!!.jobLongitude.toDouble())
        p0!!.addMarker(
            MarkerOptions()
                .position(sydney)
                .title(jb!!.jobAddress)


        );


        p0!!.animateCamera( CameraUpdateFactory.newLatLngZoom(
            LatLng(
                jb!!.jobLatitude.toDouble(), jb!!.jobLongitude.toDouble()
            ), 16.0f))


    }

    override fun onRestart() {
        super.onRestart()

        getJobDetails()
    }


    fun getJobDetails()
    {


        val retrofithelper = Retrofithelper()

        var a: Call<JobDetails> = retrofithelper.getClient().getJobDetail(
            PreferenceHelper(this).getData(
                Constants.tokenkey),jb!!.job_id.toInt())


        a.enqueue(object : Callback<JobDetails> {
            override fun onResponse(call: Call<JobDetails>, response: Response<JobDetails>) {





                if(response.body()!=null)
                {

                    var p=response.body();

                    try{

                        if(p!=null) {

                            jb=p;


                            txttitle.setText(jb!!.jobTitle)
                            txtDescription.setText(jb!!.jobDesccription)
                            txtSalaryinfo.setText(jb!!.jobSalaryInfo)
                            txtSalarytype.setText("Salary type : "+jb!!.jobSalaryType)
                            txtJobValidity.setText(" Work validity :  "+jb!!.jobValidity)

                            var pro= Constants.profileimg + jb!!.jobUser.user_image;

                            Glide.with(this@MyJobDetailsActivity).load(pro).apply(
                                RequestOptions.circleCropTransform()
                            ).into(profile)

                            username.setText(jb!!.jobUser.userName)

                            txtQualification.setText("Qualification : "+jb!!.qualificationName)

                            txtAddress.setText(jb!!.jobAddress)


                            if(jb!!.jobLatitude.toDouble()!=0.0&&jb!!.jobLongitude.toDouble()!=0.0) {
//                                val mapFragment = supportFragmentManager
//                                    .findFragmentById(com.advertise.quickjob.app.R.id.map) as SupportMapFragment?
//                                mapFragment!!.getMapAsync(this@MyJobDetailsActivity)


                                val sydney = LatLng(jb!!.jobLatitude.toDouble(), jb!!.jobLongitude.toDouble())
                                gmap!!.addMarker(
                                    MarkerOptions()
                                        .position(sydney)
                                        .title(jb!!.jobAddress)


                                );


                               gmap!!.animateCamera( CameraUpdateFactory.newLatLngZoom(
                                    LatLng(
                                        jb!!.jobLatitude.toDouble(), jb!!.jobLongitude.toDouble()
                                    ), 16.0f))


                                card_location.visibility= View.VISIBLE
                            }
                            else{

                                card_location.visibility= View.GONE
                            }

                            if(jb!!.jobPhoneVisibleStatus.toInt()==1)
                            {
                                fabcall.visibility= View.GONE
                            }
                            else{
                                fabcall.visibility= View.VISIBLE
                            }


                        }




                    }catch (e: Exception)
                    {

                    }










                }






            }

            override fun onFailure(call: Call<JobDetails>, t: Throwable) {



                if(!DownloadUtils().isNetworkConnected(this@MyJobDetailsActivity))
                {
                    DownloadUtils().showNoConnectiondialog(this@MyJobDetailsActivity);
                }

                t.printStackTrace()






            }
        })

    }
}
