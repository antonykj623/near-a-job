package com.advertise.quickjob.app.fragments


import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager

import com.advertise.quickjob.app.R
import com.advertise.quickjob.app.Utils.Constants
import com.advertise.quickjob.app.Utils.DownloadUtils
import com.advertise.quickjob.app.adapter.UploadedJoblistAdapter
import com.advertise.quickjob.app.domain.JobDetails
import com.advertise.quickjob.app.domain.UserDetails
import com.advertise.quickjob.app.preferencehelper.PreferenceHelper
import com.advertise.quickjob.app.progress.ProgressFragment
import com.advertise.quickjob.app.views.UploadJobActivity
import com.advertise.quickjob.app.webservicehelper.Retrofithelper
import kotlinx.android.synthetic.main.activity_user_profile_edit.*
import kotlinx.android.synthetic.main.fragment_favourites.view.*;
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception

/**
 * A simple [Fragment] subclass.
 */
class UploadsFragment : Fragment() {

    var v:View?=null;


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        v= inflater.inflate(R.layout.fragment_favourites, container, false)



        v!!.fabupload.setOnClickListener {

            startActivity(Intent(activity!!,UploadJobActivity::class.java))



        }



        return v!!
    }

    override fun onResume() {
        super.onResume()
        showAllJobsOfMine()
    }


    fun showAllJobsOfMine()
    {

        val retrofithelper = Retrofithelper()

        var a: Call<List<JobDetails>> = retrofithelper.getClient().getAllJobs(
            PreferenceHelper(activity).getData(
                Constants.tokenkey))


        a.enqueue(object : Callback<List<JobDetails>> {
            override fun onResponse(call: Call<List<JobDetails>>, response: Response<List<JobDetails>>) {



                if(response.body()!=null)
                {

                    var p=response.body();

                    try{

if(p!=null) {

    if(p.size>0) {
        if (p!!.get(0).status.equals("1")) {

            v!!.recycler_view.visibility=View.VISIBLE

            var a=p as ArrayList<JobDetails>



            v!!.recycler_view.layoutManager=LinearLayoutManager(activity)

            v!!.recycler_view.adapter= UploadedJoblistAdapter(activity!!,a!!)



        }

        else{
            v!!.recycler_view.visibility=View.GONE
        }

    }
    else{
        v!!.recycler_view.visibility=View.GONE
    }
}
else{
    v!!.recycler_view.visibility=View.GONE
}




                    }catch (e: Exception)
                    {

                    }










                }






            }

            override fun onFailure(call: Call<List<JobDetails>>, t: Throwable) {



                if(!DownloadUtils().isNetworkConnected(activity))
                {
                    DownloadUtils().showNoConnectiondialog(activity);
                }

                t.printStackTrace()






            }
        })
    }


}
