package com.advertise.quickjob.app.domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class JobDetails implements Serializable {

@SerializedName("job_id")
@Expose
private String job_id;
    @SerializedName("job_title")
    @Expose
    private String jobTitle;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("job_desccription")
    @Expose
    private String jobDesccription;
    @SerializedName("job_salary_info")
    @Expose
    private String jobSalaryInfo;
    @SerializedName("job_user_id")
    @Expose
    private String jobUserId;
    @SerializedName("job_address")
    @Expose
    private String jobAddress;
    @SerializedName("job_latitude")
    @Expose
    private String jobLatitude;
    @SerializedName("job_longitude")
    @Expose
    private String jobLongitude;
    @SerializedName("job_deal_status")
    @Expose
    private String jobDealStatus;
    @SerializedName("job_phone_visible_status")
    @Expose
    private String jobPhoneVisibleStatus;
    @SerializedName("job_uploaded_date")
    @Expose
    private String jobUploadedDate;
    @SerializedName("job_edited_date")
    @Expose
    private String jobEditedDate;
    @SerializedName("job_validity")
    @Expose
    private String jobValidity;
    @SerializedName("job_salary_type")
    @Expose
    private String jobSalaryType;
    @SerializedName("job_qualification")
    @Expose
    private String jobQualification;
    @SerializedName("Qualification_name")
    @Expose
    private String qualificationName;
    @SerializedName("favourite")
    @Expose
    private boolean favourite=false;
    @SerializedName("isend")
    @Expose
    private boolean isend=false;



    @SerializedName("job_user")
    @Expose
    private Jobuser jobUser;

    private boolean isContainAds=false;




    private Ads ads;

    public JobDetails() {
    }


    public boolean isContainAds() {
        return isContainAds;
    }

    public void setContainAds(boolean containAds) {
        isContainAds = containAds;
    }

    public boolean isIsend() {
        return isend;
    }

    public void setIsend(boolean isend) {
        this.isend = isend;
    }

    public Ads getAds() {
        return ads;
    }

    public void setAds(Ads ads) {
        this.ads = ads;
    }

    public boolean isFavourite() {
        return favourite;
    }

    public void setFavourite(boolean favourite) {
        this.favourite = favourite;
    }

    public String getJob_id() {
        return job_id;
    }

    public void setJob_id(String job_id) {
        this.job_id = job_id;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getJobDesccription() {
        return jobDesccription;
    }

    public void setJobDesccription(String jobDesccription) {
        this.jobDesccription = jobDesccription;
    }

    public String getJobSalaryInfo() {
        return jobSalaryInfo;
    }

    public void setJobSalaryInfo(String jobSalaryInfo) {
        this.jobSalaryInfo = jobSalaryInfo;
    }

    public String getJobUserId() {
        return jobUserId;
    }

    public void setJobUserId(String jobUserId) {
        this.jobUserId = jobUserId;
    }

    public String getJobAddress() {
        return jobAddress;
    }

    public void setJobAddress(String jobAddress) {
        this.jobAddress = jobAddress;
    }

    public String getJobLatitude() {
        return jobLatitude;
    }

    public void setJobLatitude(String jobLatitude) {
        this.jobLatitude = jobLatitude;
    }

    public String getJobLongitude() {
        return jobLongitude;
    }

    public void setJobLongitude(String jobLongitude) {
        this.jobLongitude = jobLongitude;
    }

    public String getJobDealStatus() {
        return jobDealStatus;
    }

    public void setJobDealStatus(String jobDealStatus) {
        this.jobDealStatus = jobDealStatus;
    }

    public String getJobPhoneVisibleStatus() {
        return jobPhoneVisibleStatus;
    }

    public void setJobPhoneVisibleStatus(String jobPhoneVisibleStatus) {
        this.jobPhoneVisibleStatus = jobPhoneVisibleStatus;
    }

    public String getJobUploadedDate() {
        return jobUploadedDate;
    }

    public void setJobUploadedDate(String jobUploadedDate) {
        this.jobUploadedDate = jobUploadedDate;
    }

    public String getJobEditedDate() {
        return jobEditedDate;
    }

    public void setJobEditedDate(String jobEditedDate) {
        this.jobEditedDate = jobEditedDate;
    }

    public String getJobValidity() {
        return jobValidity;
    }

    public void setJobValidity(String jobValidity) {
        this.jobValidity = jobValidity;
    }

    public String getJobSalaryType() {
        return jobSalaryType;
    }

    public void setJobSalaryType(String jobSalaryType) {
        this.jobSalaryType = jobSalaryType;
    }

    public String getJobQualification() {
        return jobQualification;
    }

    public void setJobQualification(String jobQualification) {
        this.jobQualification = jobQualification;
    }

    public String getQualificationName() {
        return qualificationName;
    }

    public void setQualificationName(String qualificationName) {
        this.qualificationName = qualificationName;
    }

    public Jobuser getJobUser() {
        return jobUser;
    }

    public void setJobUser(Jobuser jobUser) {
        this.jobUser = jobUser;
    }
}
