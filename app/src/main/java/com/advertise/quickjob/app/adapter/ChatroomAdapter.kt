package com.advertise.quickjob.app.adapter;

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.AsyncTask
import android.os.Build
import android.renderscript.Allocation
import android.renderscript.Element
import android.renderscript.RenderScript
import android.renderscript.ScriptIntrinsicBlur
import android.view.LayoutInflater
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat.startActivity
import androidx.core.content.FileProvider
import androidx.recyclerview.widget.RecyclerView;
import com.advertise.quickjob.app.R
import com.advertise.quickjob.app.Utils.Constants
import com.advertise.quickjob.app.domain.ChatMessage
import com.advertise.quickjob.app.preferencehelper.PreferenceHelper
import com.advertise.quickjob.app.progress.ProgressFragment
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.RequestOptions.bitmapTransform
import com.bumptech.glide.request.target.Target
import jp.wasabeef.glide.transformations.BlurTransformation
import kotlinx.android.synthetic.main.fragment_account.view.*
import kotlinx.android.synthetic.main.layout_chatroomholder.view.*
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.lang.Exception
import java.net.MalformedURLException
import java.net.URL

class ChatroomAdapter(list: ArrayList<ChatMessage>,cont:Context) : RecyclerView.Adapter<ChatroomAdapter.Chatroomholder> (){


    var li=list

    var con=cont
    public class Chatroomholder(itemView: View ) : RecyclerView.ViewHolder(itemView){


    }


     override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Chatroomholder {



         var v= LayoutInflater.from(parent.context).inflate(R.layout.layout_chatroomholder,parent,false)

         return     Chatroomholder(v)

     }

     override fun getItemCount(): Int {


   return li.size

     }

     override fun onBindViewHolder(holder: Chatroomholder, position: Int) {

try {

    holder.itemView.setOnClickListener {

        if (li.get(position).chatMessageType.equals("3")) {

            var r = li.get(position).chatMessage.split(",")

            var ur = "https://maps.google.com/?q=" + r[0] + "," + r[1]

            var intent = Intent(Intent.ACTION_VIEW)
            intent.setData(Uri.parse(ur))
            con.startActivity(intent)
        }
        else if(li.get(position).chatMessageType.equals("2"))
        {

            var file = File( con.getExternalCacheDir().toString() + "/" + li.get(position).chatRoomid + "/receivedfile/"+li.get(position).chatMessage)


            var file_send = File( con.getExternalCacheDir().toString() + "/" + li.get(position).chatRoomid + "/sentfiles/"+li.get(position).chatMessage)


            if(file.exists()) {

                if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.N)
                {

                    var photoURI = FileProvider.getUriForFile(con, con.getApplicationContext().getPackageName() + ".provider", file);

                    var intent = Intent();
                    intent.setAction(Intent.ACTION_VIEW);
                    intent.setDataAndType(photoURI, "image/*");
                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    con.startActivity(intent);

                }
                else {

                    var intent = Intent();
                    intent.setAction(Intent.ACTION_VIEW);
                    intent.setDataAndType(Uri.fromFile(file), "image/*");
                    con.startActivity(intent);
                }
            }

            else if(file_send.exists())
            {

                if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.N)
                {

                    var photoURI = FileProvider.getUriForFile(con, con.getApplicationContext().getPackageName() + ".provider", file_send);

                    var intent = Intent();
                    intent.setAction(Intent.ACTION_VIEW);
                    intent.setDataAndType(photoURI, "image/*");
                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    con.startActivity(intent);

                }
                else {

                    var intent = Intent();
                    intent.setAction(Intent.ACTION_VIEW);
                    intent.setDataAndType(Uri.fromFile(file_send), "image/*");
                    con.startActivity(intent);
                }


            }

        }


    }


    when (li.get(position).chatMessageType) {


        "1" -> {

            holder.itemView.cardtxlocationReceive.visibility = View.GONE
            holder.itemView.cardtxlocationSend.visibility = View.GONE
            holder.itemView.cardimgreceived.visibility = View.GONE
            holder.itemView.cardimgrSend.visibility = View.GONE

            if (li.get(position).chatSenderid.equals(PreferenceHelper(con).getData(Constants.userid))) {

                holder.itemView.cardtxtmsgSend.visibility = View.VISIBLE

                holder.itemView.cardtxtmsgReceive.visibility = View.GONE

                holder.itemView.txtSenderMessage.setText(li.get(position).chatMessage)
                holder.itemView.txtsendtime.setText(li.get(position).chat_senddate)


            } else {

                holder.itemView.cardtxtmsgSend.visibility = View.GONE

                holder.itemView.cardtxtmsgReceive.visibility = View.VISIBLE

                holder.itemView.txtRecivemsg.setText(li.get(position).chatMessage)

                holder.itemView.txtreceivedtime.setText(li.get(position).chat_senddate)
            }

        }

        "3" -> {

            holder.itemView.txtRecivemsg.visibility = View.GONE
            holder.itemView.txtSenderMessage.visibility = View.GONE

            holder.itemView.cardimgreceived.visibility = View.GONE
            holder.itemView.cardimgrSend.visibility = View.GONE




            if (li.get(position).chatSenderid.equals(PreferenceHelper(con).getData(Constants.userid))) {


//                holder.itemView.layout_locationreceive.visibility = View.GONE
//                holder.itemView.layout_locationsend.visibility = View.VISIBLE

                holder.itemView.cardtxlocationReceive.visibility = View.GONE
                holder.itemView.cardtxlocationSend.visibility = View.VISIBLE
                holder.itemView.txtlocationsenddtime.setText(li.get(position).chat_senddate)

                // var r = li.get(position).chatMessage.split(",")

                // holder.itemView.locationsend.setText(r[2]+"\n"+"Click here see location")


            } else {


                holder.itemView.cardtxlocationReceive.visibility = View.VISIBLE
                holder.itemView.cardtxlocationSend.visibility = View.GONE

                holder.itemView.txtlocationreceivedtime.setText(li.get(position).chat_senddate)

                //  var r = li.get(position).chatMessage.split(",")

                // holder.itemView.location_receive.setText(r[2]+"\n"+"Click here see location")
            }


        }

        "2" -> {

            holder.itemView.cardtxlocationReceive.visibility = View.GONE
            holder.itemView.cardtxlocationSend.visibility = View.GONE

            holder.itemView.cardtxtmsgReceive.visibility = View.GONE
            holder.itemView.cardtxtmsgSend.visibility = View.GONE

            if (li.get(position).chatSenderid.equals(PreferenceHelper(con).getData(Constants.userid))) {



                holder.itemView.cardimgreceived.visibility = View.GONE
                holder.itemView.cardimgrSend.visibility = View.VISIBLE
                holder.itemView.txtimgsendtime.setText(li.get(position).chat_senddate)



                var r =
                    con.getExternalCacheDir().toString() + "/" + li.get(position).chatRoomid + "/sentfiles" + "/" + li.get(
                        position
                    ).chatMessage;

                Glide.with(con).load(File(r)).into(holder.itemView.imgsend)


            } else {


                holder.itemView.cardimgreceived.visibility = View.VISIBLE
                holder.itemView.cardimgrSend.visibility = View.GONE
                holder.itemView.txtimgreceivetime.setText(li.get(position).chat_senddate)



                var r =
                    con.getExternalCacheDir().toString() + "/" + li.get(position).chatRoomid + "/receivedfile" + "/" + li.get(
                        position
                    ).chatMessage;

                var file=File(r)


                if(file.exists())
                {

                    holder.itemView.imgbtnDownload.visibility=View.GONE

                    Glide.with(con).load(file).into(holder.itemView.imgreceived)


                }
                else {


                    Glide.with(con).load(Constants.chatfilepath + li.get(position).chatMessage)
                        .apply(
                            bitmapTransform(
                                BlurTransformation(25, 3)
                            )
                        ).into(holder.itemView.imgreceived)


                holder.itemView.imgbtnDownload.visibility=View.VISIBLE


                }


            }

        }
    }
}catch (ex:Exception)
{
}


         holder.itemView.imgbtnDownload.setOnClickListener {


             var pf:ProgressFragment?= ProgressFragment()
             pf!!.show((con as AppCompatActivity).supportFragmentManager,"")


             Glide.with(con!!).asBitmap().load(Constants.chatfilepath + li.get(position).chatMessage).listener(object :RequestListener<Bitmap>{


                 override fun onLoadFailed(
                     e: GlideException?,
                     model: Any?,
                     target: Target<Bitmap>?,
                     isFirstResource: Boolean
                 ): Boolean {


                     pf!!.dismiss()

                     return true
                 }

                 override fun onResourceReady(
                     resource: Bitmap?,
                     model: Any?,
                     target: Target<Bitmap>?,
                     dataSource: DataSource?,
                     isFirstResource: Boolean
                 ): Boolean {


                     val t = con.getExternalCacheDir().toString() + "/" + li.get(position).chatRoomid + "/receivedfile"

                     val fol = File(t)

                     if (!fol.exists()) {
                         fol.mkdirs()
                     }

                     var fl=File(fol.absolutePath+"/"+li.get(position).chatMessage.trim())


                     if(!fl.exists())
                     {

                         fl.createNewFile()
                     }
                     var bytes =  ByteArrayOutputStream();
                     resource!!.compress(Bitmap.CompressFormat.PNG, 60, bytes);

                     var fo =  FileOutputStream(fl);
                     fo.write(bytes.toByteArray());
                     fo.close();

                     pf!!.dismiss()
                     notifyDataSetChanged()

                     return true

                 }
             }).into(holder.itemView.imgreceived)

         }
     }







 }
