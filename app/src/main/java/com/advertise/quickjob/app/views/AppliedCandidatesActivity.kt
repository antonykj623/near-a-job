package com.advertise.quickjob.app.views

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.advertise.quickjob.app.R
import com.advertise.quickjob.app.Utils.Constants
import com.advertise.quickjob.app.Utils.DownloadUtils
import com.advertise.quickjob.app.adapter.AppliedUserAdapter
import com.advertise.quickjob.app.adapter.UploadedJoblistAdapter
import com.advertise.quickjob.app.domain.JobApplieds
import com.advertise.quickjob.app.domain.JobDetails
import com.advertise.quickjob.app.preferencehelper.PreferenceHelper
import com.advertise.quickjob.app.webservicehelper.Retrofithelper
import kotlinx.android.synthetic.main.activity_applied_candidates.*
import kotlinx.android.synthetic.main.fragment_favourites.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception

class AppliedCandidatesActivity : AppCompatActivity() {

    var jobid=""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_applied_candidates)
        supportActionBar!!.hide()


        jobid=intent.getStringExtra("jobid")

        showAppliedCandidates()
    }


    fun showAppliedCandidates()
    {
        val retrofithelper = Retrofithelper()

        var a: Call<List<JobApplieds>> = retrofithelper.getClient().getAllAppliedCandidates(
            PreferenceHelper(this).getData(
                Constants.tokenkey),jobid.toString())


        a.enqueue(object : Callback<List<JobApplieds>> {
            override fun onResponse(call: Call<List<JobApplieds>>, response: Response<List<JobApplieds>>) {



                if(response.body()!=null)
                {

                    var p=response.body();

                    try{

                        if(p!=null) {

                            if(p.size>0) {


                                    var a=p as ArrayList<JobApplieds>



                                    recyclerview.layoutManager=LinearLayoutManager(this@AppliedCandidatesActivity)
                                    recyclerview.adapter=AppliedUserAdapter(this@AppliedCandidatesActivity,p)



                            }
                        }




                    }catch (e: Exception)
                    {

                    }










                }






            }

            override fun onFailure(call: Call<List<JobApplieds>>, t: Throwable) {



                if(!DownloadUtils().isNetworkConnected(this@AppliedCandidatesActivity))
                {
                    DownloadUtils().showNoConnectiondialog(this@AppliedCandidatesActivity);
                }

                t.printStackTrace()






            }
        })
    }
}
