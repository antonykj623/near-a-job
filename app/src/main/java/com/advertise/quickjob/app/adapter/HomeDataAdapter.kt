package com.advertise.quickjob.app.adapter

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.location.Geocoder
import android.os.Handler
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatImageView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.advertise.quickjob.app.R
import com.advertise.quickjob.app.Utils.Constants
import com.advertise.quickjob.app.Utils.DownloadUtils
import com.advertise.quickjob.app.domain.Ads
import com.advertise.quickjob.app.domain.JobDetails
import com.advertise.quickjob.app.preferencehelper.PreferenceHelper
import com.advertise.quickjob.app.progress.ProgressFragment
import com.advertise.quickjob.app.views.FullAdsActivity
import com.advertise.quickjob.app.views.JobdetailsActivity
import com.advertise.quickjob.app.views.MainActivity
import com.advertise.quickjob.app.webservicehelper.Retrofithelper
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.tabs.TabLayout
import com.google.gson.JsonObject
import kotlinx.android.synthetic.main.activity_edit_job.*
import kotlinx.android.synthetic.main.layout_joblist.view.*
import kotlinx.android.synthetic.main.layout_sliderholder.view.*

import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception
import java.util.*
import kotlin.collections.ArrayList

class HomeDataAdapter (con:Context,li:ArrayList<JobDetails>,ads:List<Ads>):RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var cont=con;
    var list=li

    var ad=ads

    var currentPage:Int=0

    class SliderHolder(v:View) : RecyclerView.ViewHolder(v)
    {
        var viewpager=v.findViewById<ViewPager>(R.id.viewpager)

        var tab_layout=v.findViewById<TabLayout>(R.id.tab_layout)
    }


    class JoblistHolder(v:View) : RecyclerView.ViewHolder(v)
    {
        var title=v.findViewById<TextView>(R.id.title)

        var txtwishlist=v.findViewById<TextView>(R.id.txtwishlist)

       // var txtuploadDate=v.findViewById<TextView>(R.id.txtuploadDate)

        var txtSalaryinfo=v.findViewById<TextView>(R.id.txtSalaryinfo)

        var imgjobuser=v.findViewById<AppCompatImageView>(R.id.imgjobuser)

        var cardWishlist=v.findViewById<CardView>(R.id.cardWishlist)

        var recyclerads=v.findViewById<RecyclerView>(R.id.recyclerads)


    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        var viewHolder:RecyclerView.ViewHolder?=null

        if(viewType==1)
        {

            var v= LayoutInflater.from(parent.context).inflate(R.layout.layout_sliderholder,parent,false)

            viewHolder=SliderHolder(v)


        }
        else{

            var v=LayoutInflater.from(parent.context).inflate(R.layout.layout_joblist,parent,false)

            viewHolder=JoblistHolder(v)

        }


        return viewHolder!!



    }

    override fun getItemCount(): Int {


        return list.size

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {


        if(holder.itemViewType==1)
        {

            var sholder=(holder as SliderHolder)



            for (j in 1..3) {



                sholder.itemView.tab_layout.addTab( holder.itemView.tab_layout.newTab())
                sholder.viewpager.id=j
            }

            sholder.viewpager.offscreenPageLimit=3

            val displayMetrics = DisplayMetrics()
            (cont as Activity).windowManager.defaultDisplay.getMetrics(displayMetrics)
            val height = displayMetrics.heightPixels
            val width = displayMetrics.widthPixels
            val w = width / 1.7

            sholder.viewpager.setLayoutParams(FrameLayout.LayoutParams(width, w.toInt()))

            sholder.viewpager.setAdapter(
                AdpagerAdapter(
                    (cont as AppCompatActivity).getSupportFragmentManager()

                )
            )
            sholder.itemView.tab_layout.setupWithViewPager(sholder.viewpager)

            //  holder.itemView.viewpager.setPageTransformer(true, ZoomOutPageTransformer())

            sholder.itemView.tab_layout.addOnTabSelectedListener(object :
                TabLayout.OnTabSelectedListener {
                override  fun onTabSelected(tab: TabLayout.Tab) {
                    sholder.viewpager.setCurrentItem(tab.getPosition())
                }

                override  fun onTabUnselected(tab: TabLayout.Tab) {

                }

                override  fun onTabReselected(tab: TabLayout.Tab) {

                }
            })


            var handler =  Handler();
            var Update =  Runnable() {

                if (currentPage == 3) {
                    currentPage = 0;
                }
                sholder.viewpager.setCurrentItem(currentPage++, true);

            };

            val timer = Timer()
            val hourlyTask = object : TimerTask() {
                override  fun run() {


                    //showLatestOrders()

                    handler.post(Update)


                }
            }


            timer.schedule (hourlyTask, 5000, 6000);
        }

        else if(holder.itemViewType==2)
        {

            var jbholder = holder as JoblistHolder


            holder.cardWishlist.setOnClickListener {


                var intent= Intent(cont, JobdetailsActivity::class.java)
                intent.putExtra("jobdetails",list.get(position))

                cont.startActivity(intent)




            }




            if(list.get(position).isFavourite)
            {
                holder.itemView.imgfavourite.setImageResource(R.drawable.ic_favorite)
            }
            else{

                holder.itemView.imgfavourite.setImageResource(R.drawable.ic_favorite_border_black)
            }




            holder.itemView.imgfavourite.setOnClickListener {


                if(list.get(position).isFavourite)
                {
                    removeFromFavourites(list.get(position).job_id.toInt(), position)
                }
                else {

                    addtoFavourites(list.get(position).job_id.toInt(), position)
                }

            }





            holder.title.setText(list.get(position).jobTitle)

            holder.txtwishlist.setText(list.get(position).jobDesccription)

           // holder.txtuploadDate.setText(list.get(position).jobUploadedDate)

            if(!list.get(position).jobLongitude.equals("0.0")&&!list.get(position).jobLatitude.equals("0.0"))
            {


                holder.itemView.layouttxtlocation.visibility=View.VISIBLE

                val geocoder = Geocoder(cont, Locale.getDefault())

                try {
                    val addressList = geocoder.getFromLocation(
                        list.get(position).jobLatitude.toDouble(), list.get(position).jobLongitude.toDouble(), 1
                    )
                    if (addressList != null && addressList.size > 0) {
                        val address = addressList[0]


                        if (address != null) {
                            val sb = StringBuilder()
                            for (i in 0 until address.maxAddressLineIndex) {

                                if (address.getAddressLine(i) != null && address.getAddressLine(i) != "") {

                                    sb.append(address.getAddressLine(i)).append(",")
                                }
                            }

                            if (address.locality != null) {
                                sb.append(address.locality).append(",")
                            }

                            if (address.countryName != null) {
                                sb.append(address.countryName).append("")
                            }
                            //sb.append(address.getCountryName());


                            holder.itemView.txtLocation.setText(sb.toString())







                        }
                    } else {


                    }
                } catch (e: Exception) {

                }
            }
            else{


                holder.itemView.layouttxtlocation.visibility=View.GONE
            }


            var c=list.get(position).jobSalaryInfo.split("to")

            var a1=c[0].trim()

            var a2=c[1].trim()

            var maximumsalary=a2.replace("Rs","")

            var a3=a1.replace("Between","")

            var minimumsalary=a3.replace("Rs","")

//            input_minimumsalary.setText(maximumsalary)
//
//            input_maximum.setText(maximumsalary)


            holder.txtSalaryinfo.setText(cont.getString(R.string.rs)+minimumsalary+"-"+maximumsalary+" "+list.get(position).jobSalaryType)

            holder.imgjobuser.visibility=View.GONE


//            Glide.with(cont).load(Constants.profileimg + list.get(position).jobUser.user_image).apply(
//                RequestOptions.circleCropTransform()
//            ).into(holder.imgjobuser)


            if(list.get(position).isContainAds)
            {
                holder.itemView.layout_ads.visibility=View.VISIBLE


                holder.recyclerads.layoutManager=LinearLayoutManager(cont,LinearLayoutManager.HORIZONTAL,false)

                holder.recyclerads.adapter=AdsAdapter(cont,ad)




            }
            else{


                holder.itemView.layout_ads.visibility=View.GONE
            }



            holder.itemView.txtViewmore.setOnClickListener {




                var intent= Intent(cont, FullAdsActivity::class.java)


                cont.startActivity(intent)




            }









        }






    }

    override fun getItemViewType(position: Int): Int {

        var i=0

        if(list.get(position).job_id.equals("-1"))
        {
            i= 1
        }
        else {

            i= 2
        }

return i

    }



    fun addtoFavourites(jobid:Int,position: Int)
    {
        var p: ProgressFragment = ProgressFragment()
        p.show((cont  as MainActivity).supportFragmentManager,"dhjjd")
        val retrofithelper = Retrofithelper()

        var a: Call<JsonObject> = retrofithelper.getClient().addtoFavourites(
            PreferenceHelper(cont).getData(Constants.tokenkey),jobid.toString())


        a.enqueue(object : Callback<JsonObject> {
            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {

                p.dismiss()

                if(response.body()!=null)
                {

                    var p=response.body();

                    try{



                        val json= JSONObject(p.toString())

                        if(json.getInt("status")==1)
                        {


                            list.get(position).isFavourite=true

                            Toast.makeText(cont,json.getString("message"), Toast.LENGTH_SHORT).show()

                            notifyItemChanged(position)

                        }
                        else{




                            Toast.makeText(cont,json.getString("message"), Toast.LENGTH_SHORT).show()
                        }







                    }catch (e: Exception)
                    {

                    }










                }






            }

            override fun onFailure(call: Call<JsonObject>, t: Throwable) {

                p.dismiss()

                if(!DownloadUtils().isNetworkConnected(cont))
                {
                    DownloadUtils().showNoConnectiondialog(cont);
                }

                t.printStackTrace()



                //getProfileImage.php


            }
        })
    }





    fun removeFromFavourites(jobid:Int,position: Int)
    {
        var p: ProgressFragment = ProgressFragment()
        p.show((cont  as MainActivity).supportFragmentManager,"dhjjd")
        val retrofithelper = Retrofithelper()

        var a: Call<JsonObject> = retrofithelper.getClient().removeFromFavourites(
            PreferenceHelper(cont).getData(Constants.tokenkey),jobid.toString())


        a.enqueue(object : Callback<JsonObject> {
            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {

                p.dismiss()

                if(response.body()!=null)
                {

                    var p=response.body();

                    try{



                        val json= JSONObject(p.toString())

                        if(json.getInt("status")==1)
                        {


                            list.get(position).isFavourite=false

                            Toast.makeText(cont,json.getString("message"), Toast.LENGTH_SHORT).show()

                            notifyItemChanged(position)

                        }
                        else{




                            Toast.makeText(cont,json.getString("message"), Toast.LENGTH_SHORT).show()
                        }







                    }catch (e: Exception)
                    {

                    }










                }






            }

            override fun onFailure(call: Call<JsonObject>, t: Throwable) {

                p.dismiss()

                if(!DownloadUtils().isNetworkConnected(cont))
                {
                    DownloadUtils().showNoConnectiondialog(cont);
                }

                t.printStackTrace()



                //getProfileImage.php


            }
        })
    }
}